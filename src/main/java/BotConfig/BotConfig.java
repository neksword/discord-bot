package BotConfig;

import org.aeonbits.owner.Config;

    @Config.Sources({"classpath:BotConfig.properties"})
public interface BotConfig extends Config {
    @Key("bot.token")
    String token();
    @Key("bot.prefix")
    String prefix();
    @Key("bot.name")
        String name();
    @Key("bot.avatar")
        String avatar();
    @Key("bot.auditchanneladd")
        String auditchanneladd();
    @Key("bot.auditchannelleave")
        String auditchannelleave();
    @Key("bot.audit")
        String botaudit();
    @Key("bot.auditnickname")
        String auditnickname();
    @Key("bot.vktoken")
        String vktoken();
    @Key("bot.steamtoken")
        String steamtoken();
    @Key("bot.version")
        String version();
    @Key("bot.updatedate")
        String updatedate();
    @Key("bot.suggestionchannel")
        String suggestionchannel();
    @Key("bot.musicadminchannel")
        String musicchannel();
    @Key("bot.adminchannel")
        String adminchannel();
    @Key("bot.channelused")
        String channelused();
    @Key("bot.twinklewineid")
        String twinklewineid();
    @Key("bot.channelban")
        String channelban();
    @Key("bot.freezonecategoryid")
        String freezonecategoryid();
    @Key("bot.freezonechannelcreate")
        String freezonechannelcreate();
    @Key("bot.freezonechatid")
        String freezonechatid();
    @Key("bot.userrole")
        String userrole();
    @Key("bot.homedirectory")
        String homedirectory();
    @Key("bot.adminrole")
        String adminrole();
    @Key("bot.id")
        String botid();
    }

