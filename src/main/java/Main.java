
import Audit.*;
import Audit.MemberInviteInfo.InviteTracking;
import Audit.MessageDelete.MessageDelete;
import Music.GuildMusicManager;
import UserRankSystem.Economy.AddMoney;
import UserRankSystem.Economy.Case.AddCase;
import UserRankSystem.Economy.Case.Case;
import UserRankSystem.Economy.Case.CaseShop;
import UserRankSystem.Economy.Case.RemoveCase;
import UserRankSystem.Economy.Game.Bj.Bj;
import UserRankSystem.Economy.RemoveMoney;
import UserRankSystem.LevelCommand;
import UserRankSystem.MessageRank;
import UserRankSystem.VoiceRank;
import com.jagrosh.jdautilities.command.CommandClient;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import commands.ModerationCommand.moderation.*;
import commands.ModerationCommand.moderation.JavaTest.TestJava;
import commands.ModerationCommand.moderation.TempRole.FileUpdate;
import commands.ModerationCommand.moderation.TempRole.TempRole;
import commands.ModerationCommand.moderation.TempRole.Unmute;
import commands.NoModeration.entertainments.*;
import commands.NoModeration.entertainments.lawlietBot.*;
import commands.NoModeration.information.*;
import commands.NoModeration.information.Help.Help;
import commands.NoModeration.information.Test.test;
import commands.NoModeration.utilities.*;
import commands.NoModeration.utilities.SuggestionOptions.AcceptSuggestion;
import commands.NoModeration.utilities.SuggestionOptions.DenySuggestion;
import commands.NoModeration.utilities.SuggestionOptions.Suggestion;
import commands.NoModeration.utilities.Tickets.TicketsNew;
import commands.audiochannel.ChannelSettings.*;
import commands.audiochannel.Createchannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.guild.GuildReadyEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.managers.AudioManager;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import BotConfig.BotConfig;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import noCommands.ChangeRole;
import noCommands.ServerStats;
import noCommands.Verifyreact;
import noCommands.slworld;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;
import triggers.LD;
import triggers.TriggerHelp;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


public class Main extends ListenerAdapter {
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(4);
    public static void main(String[] arguments) throws Exception {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb+srv://TwinkleWine:fedoro2505@cluster0.py8c8.mongodb.net/Economy?retryWrites=true&w=majority"));
        MongoDatabase database = mongoClient.getDatabase("Economy");
        MongoCollection<Document> collection = database.getCollection("test");
        MongoCollection<Document> collectionShop = database.getCollection("shop");
        JDA api = JDABuilder.create(cfg.token(),GatewayIntent.GUILD_MEMBERS,GatewayIntent.GUILD_MESSAGES,GatewayIntent.GUILD_MESSAGE_REACTIONS,GatewayIntent.GUILD_BANS,GatewayIntent.GUILD_PRESENCES,GatewayIntent.GUILD_EMOJIS,GatewayIntent.GUILD_VOICE_STATES,GatewayIntent.GUILD_INVITES).enableCache(CacheFlag.MEMBER_OVERRIDES,CacheFlag.ACTIVITY,CacheFlag.CLIENT_STATUS).setMemberCachePolicy(MemberCachePolicy.ALL).build();
        EventWaiter waiter = new EventWaiter();
        CommandClient client = new CommandClientBuilder()
                .addCommand(new Ping())
                .addCommand(new Ban())
                .addCommand(new Kick())
                .addCommand(new Clear())
                .addCommand(new TempRole())
                .addCommand(new GiveRole())
                .addCommand(new NicknameModify())
                .addCommand(new RemoveRole())
                .addCommand(new Cat())
                .addCommand(new Coin())
                .addCommand(new Dog())
                .addCommand(new Fox())
                .addCommand(new Meme())
                .addCommand(new Say())
                .addCommand(new Say2())
                .addCommand(new Botinfo())
                .addCommand(new UserInfo(collection))
                .addCommand(new Version())
                .addCommand(new Avatar())
                .addCommand(new Suggestion())
                .addCommand(new DenySuggestion())
                .addCommand(new AcceptSuggestion())
                .addCommand(new ChannelName())
                .addCommand(new ChannelUsersMax())
                .addCommand(new MemberVoiceKick())
                .addCommand(new MemberVoiceOpen())
                .addCommand(new VoiceCloseCommand())
                .addCommand(new VoiceOpenCommand())
                .addCommand(new Random())
                .addCommand(new Unmute())
                .addCommand(new LevelCommand(mongoClient,database,collection))
                .addCommand(new Serverinfo())
                .addCommand(new Cookies(collection))
                .addCommand(new Case(collection))
                .addCommand(new CaseShop(collection))
                .addCommand(new AddCase(collection))
                .addCommand(new RemoveCase(collection))
                .addCommand(new Kiss())
                .addCommand(new Hug())
                .addCommand(new Cuddle())
                .addCommand(new Slap())
                .addCommand(new Poke())
                .addCommand(new Pat())
                .addCommand(new Feed())
                .addCommand(new WWWQCommand(waiter))
                .addCommand(new AddMoney(collection))
                .addCommand(new RemoveMoney(collection))
                .addCommand(new RoleInfo(collection))
                .addCommand(new test(waiter,mongoClient))
                .addCommand(new EmbedBComm(waiter))
                .useHelpBuilder(false)
                .setEmojis("✅","⚠️","❌")
                .setActivity(Activity.watching(cfg.prefix() + "help | By TwinkleWine"))
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .setAlternativePrefix("D!")
                .setPrefix(cfg.prefix())
                .setOwnerId(cfg.twinklewineid())
                .build();
        api.addEventListener(new Main());
        api.addEventListener(new Createchannel());
        api.addEventListener(new LD());
        api.addEventListener(new ChangeRole());
        api.addEventListener(new MessageDelete());
        api.addEventListener(new Help());
        api.addEventListener(new InviteTracking());
        api.addEventListener(new ChangeNickname());
        api.addEventListener(new ChannelRoleUpdate());
        api.addEventListener(new InviteLink());
        api.addEventListener(new MemberJoinNicknameChange());
        api.addEventListener(new MemberLeave());
        api.addEventListener(new MemberRoleUpdate());
        api.addEventListener(new MemberVoiceAudit());
        api.addEventListener(new MessageRank(collection));
        api.addEventListener(new Verifyreact());
        api.addEventListener(new ServerStats());
        api.addEventListener(new slworld());
        api.addEventListener(new TicketsNew());
        api.addEventListener(new VoiceRank(collection));
        api.addEventListener(new FileUpdate());
        //api.addEventListener(new PhasmaGame());
        api.addEventListener(new TriggerHelp());
        api.addEventListener(new TestJava());
        //api.addEventListener(new Kvazar());
        api.addEventListener(new Bj(collection, waiter));
        //api.addEventListener(new ShopCommand(collectionShop));
        api.addEventListener(waiter);
        api.addEventListener(client);
        api.upsertCommand("ping", "Calculate ping of the bot").queue();
        System.out.println(api);
        //new BotsLongPoll(new VkBoard(api)).run();
    }

    public void onGuildReady(GuildReadyEvent event) {
        if (event.getGuild().getId().equals("807690589322608690")) {
            TextChannel channel = event.getGuild().getTextChannelById("821571264710836244");
            Member member = event.getGuild().getMemberById("387521150281973760");
            channel.sendMessage(member.getAsMention() + " я запущен!").queue();
        }
        if (event.getGuild().getId().equals("807690589322608690")){
            TextChannel channel = event.getGuild().getTextChannelById("821571264710836244");
            loadAndPlay(channel,"https://www.youtube.com/watch?v=b3FORGBsmNA");
            Runnable runnable = () -> {
                loadAndPlay(channel,"https://www.youtube.com/watch?v=b3FORGBsmNA");
            };
            ScheduledFuture<?> scheduledFuture = scheduler.scheduleAtFixedRate(runnable,6,6, TimeUnit.HOURS);
        }
    }

    private final AudioPlayerManager playerManager;
    private final Map<Long, GuildMusicManager> musicManagers;

    private Main() {
        this.musicManagers = new HashMap<>();

        this.playerManager = new DefaultAudioPlayerManager();
        AudioSourceManagers.registerRemoteSources(playerManager);
        AudioSourceManagers.registerLocalSource(playerManager);
    }

    private synchronized GuildMusicManager getGuildAudioPlayer(Guild guild) {
        long guildId = Long.parseLong(guild.getId());
        GuildMusicManager musicManager = musicManagers.get(guildId);

        if (musicManager == null) {
            musicManager = new GuildMusicManager(playerManager);
            musicManagers.put(guildId, musicManager);
        }

        guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

        return musicManager;
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        String channelid = event.getChannel().getId();

            if ((cfg.prefix() + "play").equals(command[0]) && command.length == 2) {
                if (channelid.equals(cfg.musicchannel())) {
                    loadAndPlay(event.getChannel(), command[1]);

                    Date date = new Date();

                    TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Была использована команда :");
                    builder.addField("Информация: ", "Название команды: `Play` " + "\n" + "Использована в: " + "`" + event.getChannel().getName() + "`" + "( " + event.getChannel().getAsMention() + " )" + "\n" + "Время: " + "`" + date.toString() + "`", false);
                    builder.setThumbnail(event.getAuthor().getAvatarUrl());
                    builder.setColor(56);
                    builder.addField("Пользователь: ", "Имя: " + "`" + event.getAuthor().getName() + "`" + "( " + event.getMember().getAsMention() + " )" + "\n" + "ID: " + "`" + event.getAuthor().getId() + "`", false);
                    builder.setFooter(event.getGuild().getName());

                    channel.sendMessage(builder.build()).queue();
                    builder.clear();
                }
                else {
                    event.getMessage().reply("**Управлять музыкой могут только Модераторы!**").queue();
                }
            }
            else if ((cfg.prefix() + "skip").equals(command[0])) {
                if (channelid.equals(cfg.musicchannel())) {
                    skipTrack(event.getChannel());

                    Date date = new Date();

                    TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Была использована команда :");
                    builder.addField("Информация: ", "Название команды: `Skip` " + "\n" + "Использована в: " + "`" + event.getChannel().getName() + "`" + "( " + event.getChannel().getAsMention() + " )" + "\n" + "Время: " + "`" + date.toString() + "`", false);
                    builder.setThumbnail(event.getAuthor().getAvatarUrl());
                    builder.setColor(56);
                    builder.addField("Пользователь: ", "Имя: " + "`" + event.getAuthor().getName() + "`" + "( " + event.getMember().getAsMention() + " )" + "\n" + "ID: " + "`" + event.getAuthor().getId() + "`", false);
                    builder.setFooter(event.getGuild().getName());

                    channel.sendMessage(builder.build()).queue();
                    builder.clear();
                }
                else {
                    event.getMessage().reply("**Управлять музыкой могут только Модераторы!**").queue();
                }
            }
            else if ((cfg.prefix() + "stop").equalsIgnoreCase(command[0])){
                if (channelid.equals(cfg.musicchannel())) {
                    stop(event.getChannel());

                    Date date = new Date();

                    TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Была использована команда :");
                    builder.addField("Информация: ", "Название команды: `Stop` " + "\n" + "Использована в: " + "`" + event.getChannel().getName() + "`" + "( " + event.getChannel().getAsMention() + " )" + "\n" + "Время: " + "`" + date.toString() + "`", false);
                    builder.setThumbnail(event.getAuthor().getAvatarUrl());
                    builder.setColor(56);
                    builder.addField("Пользователь: ", "Имя: " + "`" + event.getAuthor().getName() + "`" + "( " + event.getMember().getAsMention() + " )" + "\n" + "ID: " + "`" + event.getAuthor().getId() + "`", false);
                    builder.setFooter(event.getGuild().getName());

                    channel.sendMessage(builder.build()).queue();
                    builder.clear();
                }
                else {
                    event.getMessage().reply("**Управлять музыкой могут только Модераторы!**").queue();
                }
            }
            else if ((cfg.prefix() + "queue").equalsIgnoreCase(command[0])){
                if (channelid.equals(cfg.musicchannel())) {
                    queue(event.getChannel());

                    Date date = new Date();

                    TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Была использована команда :");
                    builder.addField("Информация: ", "Название команды: `Queue` " + "\n" + "Использована в: " + "`" + event.getChannel().getName() + "`" + "( " + event.getChannel().getAsMention() + " )" + "\n" + "Время: " + "`" + date.toString() + "`", false);
                    builder.setThumbnail(event.getAuthor().getAvatarUrl());
                    builder.setColor(56);
                    builder.addField("Пользователь: ", "Имя: " + "`" + event.getAuthor().getName() + "`" + "( " + event.getMember().getAsMention() + " )" + "\n" + "ID: " + "`" + event.getAuthor().getId() + "`", false);
                    builder.setFooter(event.getGuild().getName());

                    channel.sendMessage(builder.build()).queue();
                    builder.clear();
                }
                else {
                    event.getMessage().reply("**Управлять музыкой могут только Модераторы!**").queue();
                }
            }
            else if ((cfg.prefix() + "pause").equalsIgnoreCase(command[0])){
                if (channelid.equals(cfg.musicchannel())) {
                    pause(event.getChannel());

                    Date date = new Date();

                    TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Была использована команда :");
                    builder.addField("Информация: ", "Название команды: `Pause` " + "\n" + "Использована в: " + "`" + event.getChannel().getName() + "`" + "( " + event.getChannel().getAsMention() + " )" + "\n" + "Время: " + "`" + date.toString() + "`", false);
                    builder.setThumbnail(event.getAuthor().getAvatarUrl());
                    builder.setColor(56);
                    builder.addField("Пользователь: ", "Имя: " + "`" + event.getAuthor().getName() + "`" + "( " + event.getMember().getAsMention() + " )" + "\n" + "ID: " + "`" + event.getAuthor().getId() + "`", false);
                    builder.setFooter(event.getGuild().getName());

                    channel.sendMessage(builder.build()).queue();
                    builder.clear();
                }
                else {
                    event.getMessage().reply("**Управлять музыкой могут только Модераторы!**").queue();
                }
            }
            else if ((cfg.prefix() + "resume").equalsIgnoreCase(command[0])){
                if (channelid.equals(cfg.musicchannel())) {
                    resume(event.getChannel());

                    Date date = new Date();

                    TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Была использована команда :");
                    builder.addField("Информация: ", "Название команды: `Resume` " + "\n" + "Использована в: " + "`" + event.getChannel().getName() + "`" + "( " + event.getChannel().getAsMention() + " )" + "\n" + "Время: " + "`" + date.toString() + "`", false);
                    builder.setThumbnail(event.getAuthor().getAvatarUrl());
                    builder.setColor(56);
                    builder.addField("Пользователь: ", "Имя: " + "`" + event.getAuthor().getName() + "`" + "( " + event.getMember().getAsMention() + " )" + "\n" + "ID: " + "`" + event.getAuthor().getId() + "`", false);
                    builder.setFooter(event.getGuild().getName());

                    channel.sendMessage(builder.build()).queue();
                    builder.clear();
                }
                else {
                    event.getMessage().reply("**Управлять музыкой могут только Модераторы!**").queue();
                }
            }
            else if ((cfg.prefix() + "volume").equalsIgnoreCase(command[0])) {
                if (channelid.equals(cfg.musicchannel())) {
                    TextChannel channel = event.getChannel();
                    GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
                    try {
                        if (command.length == 1) {
                            channel.sendMessage("Текущая громкость: " + musicManager.player.getVolume()).queue();
                        } else {
                            if ((Integer.parseInt(command[1].trim()) <= 0) || (Integer.parseInt(command[1].trim()) >= 101)) {
                                channel.sendMessage("Укажите значение от 1 до 100").queue();
                            } else {
                                musicManager.player.setVolume(Integer.parseInt(command[1].trim()));
                                channel.sendMessage("Громкость изменена на: " + Integer.parseInt(command[1].trim())).queue();
                            }
                        }
                    } catch (NumberFormatException numberFormatException) {
                        channel.sendMessage("Укажите цифровое значение от 1 до 100").queue();
                    }

                    Date date = new Date();

                    TextChannel textChannel = event.getGuild().getTextChannelById(cfg.botaudit());

                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Была использована команда :");
                    builder.addField("Информация: ", "Название команды: `Volume` " + "\n" + "Использована в: " + "`" + event.getChannel().getName() + "`" + "( " + event.getChannel().getAsMention() + " )" + "\n" + "Время: " + "`" + date.toString() + "`", false);
                    builder.setThumbnail(event.getAuthor().getAvatarUrl());
                    builder.setColor(56);
                    builder.addField("Пользователь: ", "Имя: " + "`" + event.getAuthor().getName() + "`" + "( " + event.getMember().getAsMention() + " )" + "\n" + "ID: " + "`" + event.getAuthor().getId() + "`", false);
                    builder.setFooter(event.getGuild().getName());

                    textChannel.sendMessage(builder.build()).queue();
                    builder.clear();
                }
                else {
                    event.getMessage().reply("**Управлять музыкой могут только Модераторы!**").queue();
                }
            }

        super.onGuildMessageReceived(event);
    }
    private void pause(TextChannel channel){
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
        musicManager.player.setPaused(true);

        channel.sendMessage("Воспроизведение на паузе").queue();
    }
    private void resume(TextChannel channel){
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
        musicManager.player.setPaused(false)    ;

        channel.sendMessage("Воспроизведение продолжено").queue();
    }
    private  void stop(TextChannel channel){
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
        musicManager.player.stopTrack();

        channel.sendMessage("Музыка остановлена").queue();
    }
    private void queue(TextChannel channel){
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
        musicManager.player.getPlayingTrack();
        if (musicManager.player.getPlayingTrack() == null){
            channel.sendMessage("Песня не включена").queue();
        }
        else {
            channel.sendMessage("Играет: " + musicManager.player.getPlayingTrack().getInfo().title).queue();
        }
    }

    private void loadAndPlay(final TextChannel channel, final String trackUrl) {
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

        playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler() {
            @Override
            public void trackLoaded(AudioTrack track) {
                channel.sendMessage("Добавлено в очередь: " + track.getInfo().title).queue();

                play(channel.getGuild(), musicManager, track);
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist) {
                AudioTrack firstTrack = playlist.getSelectedTrack();

                if (firstTrack == null) {
                    firstTrack = playlist.getTracks().get(0);
                }

                channel.sendMessage("Добавлено в очередь: " + firstTrack.getInfo().title + " (Первый трек в плейлисте:  " + playlist.getName() + ")").queue();

                play(channel.getGuild(), musicManager, firstTrack);
            }

            @Override
            public void noMatches() {
                channel.sendMessage("Ничего не найдено: " + trackUrl).queue();
            }

            @Override
            public void loadFailed(FriendlyException exception) {
                channel.sendMessage("Ошибка загрузки: " + exception.getMessage()).queue();
            }
        });
    }

    private void play(Guild guild, GuildMusicManager musicManager, AudioTrack track) {
        connectToFirstVoiceChannel(guild.getAudioManager(),guild);

        musicManager.scheduler.queue(track);
    }

    private void skipTrack(TextChannel channel) {
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
        musicManager.scheduler.nextTrack();

        channel.sendMessage("Пропущен трек.").queue();
    }

    private static void connectToFirstVoiceChannel(AudioManager audioManager,Guild guild) {
        if (!audioManager.isConnected() && !audioManager.isAttemptingToConnect()) {
            VoiceChannel voiceChannel = guild.getVoiceChannelById("807808550143393834");
                audioManager.openAudioConnection(voiceChannel);
        }
    }
//🎶Музыкальная Зона

}
