package commands.audiochannel;

import net.dv8tion.jda.api.entities.Member;

public class ChannelInfo {
    private String memberID;
    public ChannelInfo(){

    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memverID) {
        this.memberID = memverID;
    }

    public ChannelInfo(final Member member){
        this.memberID = member.getId();
    }
}
