package commands.audiochannel.ChannelSettings;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.audiochannel.PermissionCheck.Permission;
import commands.audiochannel.PermissionCheck.VoiceCommandPermissionCheck;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

public class VoiceCloseCommand extends Command {

    public VoiceCloseCommand() {
        this.name = "vclose";
        this.guildOnly = true;
        this.cooldown = 20;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        VoiceCommandPermissionCheck voiceCommandPermissionCheck = new VoiceCommandPermissionCheck();
        if (voiceCommandPermissionCheck.VoiceReturn(event.getMember(), event.getTextChannel(), event.getMessage()) == true) {
            Permission permission = new Permission();
            if (permission.permissioncheck(event.getMember().getVoiceState().getChannel(), event.getMember()) == true) {
                Role role = event.getGuild().getRoleById(cfg.userrole());
                System.out.println(role.getName());
                event.getMember().getVoiceState().getChannel().upsertPermissionOverride(role).deny(net.dv8tion.jda.api.Permission.VOICE_CONNECT).queue();
                event.getMessage().reply("**Канал закрыт!**").queue();
            } else {
                event.getMessage().reply("**У вас нет прав на использование этой команды!**").queue();

            }
        }
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        String commandname = "VClose";
        LogCommand logCommand = new LogCommand();
        channel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
    }
}

