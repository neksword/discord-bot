package commands.audiochannel.ChannelSettings;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.audiochannel.PermissionCheck.Permission;
import commands.audiochannel.PermissionCheck.VoiceCommandPermissionCheck;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

public class ChannelUsersMax extends Command {

    public ChannelUsersMax() {
        this.name = "vmax";
        this.guildOnly = true;
        this.cooldown = 20;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        VoiceCommandPermissionCheck voiceCommandPermissionCheck = new VoiceCommandPermissionCheck();
        if (voiceCommandPermissionCheck.VoiceReturn(event.getMember(), event.getTextChannel(), event.getMessage()) == true) {
            Permission permission = new Permission();
            if (permission.permissioncheck(event.getMember().getVoiceState().getChannel(), event.getMember()) == true) {
                VoiceChannel voiceChannel = event.getMember().getVoiceState().getChannel();
                if (command.length == 1) {
                    if (voiceChannel.getUserLimit() == 0) {
                        event.getMessage().reply("**Текущее ограничение - **" + "`" + "Не ограничено" + "`").queue();
                    } else {
                        event.getMessage().reply("**Текущее ограничение - **" + "`" + voiceChannel.getUserLimit() + "`").queue();
                    }
                } else {
                    try {
                        if ((Integer.parseInt(command[1].trim()) >= 0) && (Integer.parseInt(command[1].trim()) <= 99)) {

                            event.getMember().getVoiceState().getChannel().getManager().setUserLimit(Integer.parseInt(command[1])).queue();
                            event.getMessage().reply("**Максимальное количество участников канала успешно изменено на - `" + Integer.parseInt(command[1].trim()) + "`!**").queue();

                        } else {
                            event.getMessage().reply("**Укажите число от 0 до 99!**").queue();
                        }
                    } catch (NumberFormatException nfe) {
                        event.getMessage().reply("**Укажите число от 0 до 99!**").queue();
                    }
                }
            } else {
                event.getMessage().reply("**У вас нет прав на использование этой команды!**").queue();
            }
        }
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        String commandname = "VMax";
        LogCommand logCommand = new LogCommand();
        channel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
    }
}
