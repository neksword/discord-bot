package commands.audiochannel.ChannelSettings;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.audiochannel.PermissionCheck.Permission;
import commands.audiochannel.PermissionCheck.VoiceCommandPermissionCheck;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.util.Arrays;
import java.util.List;

public class MemberVoiceOpen extends Command {

    public MemberVoiceOpen() {
        this.name = "vmopen";
        this.guildOnly = true;
        this.cooldown = 20;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        VoiceCommandPermissionCheck voiceCommandPermissionCheck = new VoiceCommandPermissionCheck();
        if (voiceCommandPermissionCheck.VoiceReturn(event.getMember(), event.getTextChannel(), event.getMessage()) == true) {
            Permission permission = new Permission();
            if (permission.permissioncheck(event.getMember().getVoiceState().getChannel(), event.getMember()) == true) {
                List<Member> memberList = event.getMessage().getMentionedMembers();
                if (memberList.size() == 0) {
                    event.getMessage().reply("Использование команды: " + cfg.prefix() + "VMOpen (Тег пользователя)").queue();
                } else {
                    VoiceChannel voiceChannel = event.getMember().getVoiceState().getChannel();
                    voiceChannel.putPermissionOverride(memberList.get(0)).setAllow(net.dv8tion.jda.api.Permission.VOICE_CONNECT).queue();
                    event.getMessage().reply("**Канал открыт для: **" + memberList.get(0).getAsMention()).queue();
                }
            } else {
                event.getMessage().reply("**У вас нет прав на использование этой команды!**").queue();

            }
        }
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        String commandname = "VMOpen";
        LogCommand logCommand = new LogCommand();
        channel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
    }
}
