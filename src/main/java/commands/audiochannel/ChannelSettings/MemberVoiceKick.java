package commands.audiochannel.ChannelSettings;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.audiochannel.PermissionCheck.Permission;
import commands.audiochannel.PermissionCheck.VoiceCommandPermissionCheck;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.util.Arrays;
import java.util.List;

public class MemberVoiceKick extends Command {

    public MemberVoiceKick() {
        this.name = "vkick";
        this.guildOnly = true;
        this.cooldown = 20;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        System.out.println(command[0]);
        VoiceCommandPermissionCheck voiceCommandPermissionCheck = new VoiceCommandPermissionCheck();
        if (voiceCommandPermissionCheck.VoiceReturn(event.getMember(), event.getTextChannel(), event.getMessage()) == true) {
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
            Permission permission = new Permission();
            if (permission.permissioncheck(event.getMember().getVoiceState().getChannel(), event.getMember()) == true) {
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                List<Member> memberList = event.getMessage().getMentionedMembers();
                VoiceChannel voiceChannel = event.getMember().getVoiceState().getChannel();
                System.out.println(memberList);
                if (!(memberList.size() == 0)) {
                    if ((memberList.get(0).getVoiceState().getChannel().getId().equals(event.getMember().getVoiceState().getChannel().getId())) || !(memberList.get(0).getVoiceState() == null)) {
                        event.getGuild().kickVoiceMember(memberList.get(0)).queue();
                        List<net.dv8tion.jda.api.Permission> permissions = Arrays.asList(net.dv8tion.jda.api.Permission.VOICE_CONNECT);
                        voiceChannel.upsertPermissionOverride(memberList.get(0)).deny(permissions).queue();
                        event.getMessage().reply("**Участник " + memberList.get(0).getAsMention() + " выгнан!**").queue();
                    } else {
                        event.getMessage().reply("**Вы не можете управлять этим пользователем!**").queue();
                    }
                } else {
                    event.getMessage().reply("**Укажите пользователя!**").queue();
                }
            } else {
                event.getMessage().reply("**У вас нет прав на использование этой команды!**").queue();

            }
        }
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        String commandname = "VKick";
        LogCommand logCommand = new LogCommand();
        channel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
    }
}
