package commands.audiochannel.ChannelSettings;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.audiochannel.PermissionCheck.Permission;
import commands.audiochannel.PermissionCheck.VoiceCommandPermissionCheck;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

public class ChannelName extends Command {

    public ChannelName() {
        this.name = "vname";
        this.guildOnly = true;
        this.cooldown = 20;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        VoiceCommandPermissionCheck voiceCommandPermissionCheck = new VoiceCommandPermissionCheck();
        if (voiceCommandPermissionCheck.VoiceReturn(event.getMember(), event.getTextChannel(), event.getMessage()) == true) {
            Permission permission = new Permission();
            if (permission.permissioncheck(event.getMember().getVoiceState().getChannel(), event.getMember()) == true) {
                if (command.length == 1) {
                    event.getMessage().reply("Использование команды: " + cfg.prefix() + "VName (ИМЯ)").queue();
                } else {
                    final StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 1; i < command.length; i++) {
                        stringBuilder.append(command[i]).append(" ");
                    }
                    VoiceChannel voiceChannel = event.getMember().getVoiceState().getChannel();
                    voiceChannel.getManager().setName(stringBuilder.toString()).queue();
                    event.getMessage().reply("**Имя канала успешно изменено на - `" + stringBuilder.toString().trim() + "`!**").queue();
                }
            } else {
                event.getMessage().reply("**У вас нет прав на использование этой команды!**").queue();

            }
        }
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        String commandname = "VName";
        LogCommand logCommand = new LogCommand();
        channel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
    }
}

