package commands.audiochannel;

import BotConfig.BotConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Createchannel extends ListenerAdapter {

    public void onGuildVoiceJoin(GuildVoiceJoinEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        String voiceid = event.getChannelJoined().getId();
        if (voiceid.equals(cfg.freezonechannelcreate())){
            Member member = event.getMember();
            Category category = event.getGuild().getCategoryById(cfg.freezonecategoryid());
            VoiceChannel channel = event.getGuild().createVoiceChannel(member.getEffectiveName(),category).complete();
            event.getGuild().moveVoiceMember(member,channel).queue();
            File file = new File("/home/" + cfg.homedirectory() + "/VoiceChannel/" + channel.getId() + ".json");
            try {
                file.createNewFile();
                ObjectMapper mapper = new ObjectMapper();
                final ChannelInfo channelInfo = new ChannelInfo(event.getMember());
                mapper.writeValue(file,channelInfo);
                List<Permission> permissions = Arrays.asList(Permission.VOICE_CONNECT,Permission.VIEW_CHANNEL);
                channel.putPermissionOverride(member).setAllow(permissions).queue();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Date date = new Date();

            EmbedBuilder builder = new EmbedBuilder();
            builder.setTitle("Создан голосовой канал во FreeZone!");
            builder.setDescription("Пользователь: " + member.getUser().getAsTag());
            builder.setColor(Color.BLUE);
            builder.addField("Канал пользователя: " + member.getEffectiveName() ,"Дата и время: " + date.toString(),false);

            TextChannel channel1 = event.getGuild().getTextChannelById(cfg.botaudit());

            channel1.sendMessage(builder.build()).queue();
            builder.clear();
        }
    }

    public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        String categ = cfg.freezonecategoryid();
        VoiceChannel voiceChannel = event.getChannelLeft();
        String channel1 = event.getChannelLeft().getId();
        String voice = cfg.freezonechannelcreate();
        List<Member> member = event.getChannelLeft().getMembers();
        String category1 = event.getChannelLeft().getParent().getId();
        if (categ.equals(category1)){
            if (!(channel1.equals(voice))){
                if (member.size() == 0){
                    voiceChannel.delete().queue();
                    File file = new File("/home/" + cfg.homedirectory() + "/VoiceChannel/" + channel1 + ".json");
                    file.delete();

                    Date date = new Date();

                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Удален голосовой канал во FreeZone!");
                    builder.setDescription("Пользователь: " + event.getMember().getUser().getAsTag());
                    builder.setColor(Color.MAGENTA);
                    builder.addField("Последний вышедший пользователь: " + event.getMember().getEffectiveName() , "Имя канала: " + event.getChannelLeft().getName() + "\n" + "Дата и время: " + date.toString(),false);

                    TextChannel channel2 = event.getGuild().getTextChannelById(cfg.botaudit());

                    channel2.sendMessage(builder.build()).queue();
                    builder.clear();
                }
            }
        }
    }

    public void onGuildVoiceMove(GuildVoiceMoveEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        String voiceid = event.getChannelJoined().getId();
        if (voiceid.equals(cfg.freezonechannelcreate())){
            Member member = event.getMember();
            Category category = event.getGuild().getCategoryById(cfg.freezonecategoryid());
            VoiceChannel channel = event.getGuild().createVoiceChannel(member.getEffectiveName(),category).complete();
            event.getGuild().moveVoiceMember(member,channel).queue();
            File file = new File("/home/" + cfg.homedirectory() + "/VoiceChannel/" + channel.getId() + ".json");
            try {
                file.createNewFile();
                ObjectMapper mapper = new ObjectMapper();
                final ChannelInfo channelInfo = new ChannelInfo(event.getMember());
                mapper.writeValue(file,channelInfo);
                List<Permission> permissions = Arrays.asList(Permission.VOICE_CONNECT,Permission.VIEW_CHANNEL);
                channel.putPermissionOverride(member).setAllow(permissions).queue();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Date date = new Date();

            EmbedBuilder builder = new EmbedBuilder();
            builder.setTitle("Создан голосовой канал во FreeZone!");
            builder.setDescription("Пользователь: " + member.getUser().getAsTag());
            builder.setColor(Color.BLUE);
            builder.addField("Канал пользователя: " + member.getEffectiveName() ,"Дата и время: " + date.toString(),false);

            TextChannel channel1 = event.getGuild().getTextChannelById(cfg.botaudit());

            channel1.sendMessage(builder.build()).queue();
            builder.clear();
        }
        String categ = cfg.freezonecategoryid();
        VoiceChannel voiceChannel = event.getChannelLeft();
        String channel1 = event.getChannelLeft().getId();
        String voice = cfg.freezonechannelcreate();
        List<Member> member = event.getChannelLeft().getMembers();
        String category1 = event.getChannelLeft().getParent().getId();
        if (categ.equals(category1)){
            if (!(channel1.equals(voice))){
                if (member.size() == 0){
                    voiceChannel.delete().queue();
                    File file = new File("/home/" + cfg.homedirectory() + "/VoiceChannel/" + channel1 + ".json");
                    file.delete();

                    Date date = new Date();

                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Удален голосовой канал во FreeZone!");
                    builder.setDescription("Пользователь: " + event.getMember().getUser().getAsTag());
                    builder.setColor(Color.MAGENTA);
                    builder.addField("Последний вышедший пользователь: " + event.getMember().getEffectiveName() , "Имя канала: " + event.getChannelLeft().getName() + "\n" + "Дата и время: " + date.toString(),false);

                    TextChannel channel2 = event.getGuild().getTextChannelById(cfg.botaudit());

                    channel2.sendMessage(builder.build()).queue();
                    builder.clear();
                }
            }
        }
    }
}
