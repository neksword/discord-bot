package commands.audiochannel.PermissionCheck;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

public class VoiceCommandPermissionCheck {
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public boolean VoiceBot(final Member member){
        if (member.getUser().isBot()) {
            return true;
        } else {
            return false;
        }
    }
    public boolean VoiceChannel(final Member member,final TextChannel textChannel){
        if (textChannel.getId().equals(cfg.channelused()) || textChannel.getId().equals(cfg.musicchannel()) || textChannel.getId().equals(cfg.freezonechatid()) || member.getId().equals(cfg.twinklewineid())){
            return true;
        }
        else {
            return false;
        }
    }
    public boolean VoiceReturn(final Member member, final TextChannel textChannel, final Message message){
        if (VoiceBot(member)){
            return false;
        }
        else if (VoiceChannel(member, textChannel)){
            return true;
        }
        else if (!VoiceChannel(member, textChannel)){
            VoiceInfo(textChannel, message);
            return false;
        }
        return false;
    }
    public void VoiceInfo(final TextChannel textChannel, final Message message){
        TextChannel Channel = textChannel.getGuild().getTextChannelById(cfg.channelused());
        TextChannel textChannel1= textChannel.getGuild().getTextChannelById(cfg.freezonechatid());
        message.reply("**Команды настройки комнат только в ** " + Channel.getAsMention() + " **и** " + textChannel1.getAsMention()).queue();

    }
}
