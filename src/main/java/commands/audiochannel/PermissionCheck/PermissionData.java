package commands.audiochannel.PermissionCheck;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;

public class PermissionData {
    private String channelid;
    private String memberid;
    public PermissionData(final Member member, final VoiceChannel channel) {
        this.channelid = channel.getId();
        this.memberid = member.getId();
    }
    public String getChannelid()
    {
        return channelid;
    }
    public String getMemberid()
    {
        return memberid;
    }
}
