package commands.audiochannel.PermissionCheck;

import BotConfig.BotConfig;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import commands.audiochannel.ChannelInfo;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.VoiceChannel;
import org.aeonbits.owner.ConfigFactory;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Permission {

    public boolean permissioncheck(final VoiceChannel voiceChannel, final Member member) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        if (voiceChannel != null) {
            File file = new File("/home/" + cfg.homedirectory() + "/VoiceChannel/" + voiceChannel.getId() + ".json");
            ObjectMapper mapper = new ObjectMapper();
            try {
                ChannelInfo channelInfo = mapper.readValue(file, ChannelInfo.class);
                if (channelInfo.getMemberID().equals(member.getId())) {
                    return true;
                } else {
                    return false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
