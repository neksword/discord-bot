package commands.NoModeration.utilities;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

public class Random extends Command {
    public Random()
    {
        this.name = "random";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            String[] args = event.getMessage().getContentRaw().split("\\s");
            if (event.getAuthor().isBot()) {
                return;
            } else {
                if (event.getChannel().getId().equals(cfg.channelused()) || event.getChannel().getId().equals("813114771808976916") || event.getChannel().getId().equals(cfg.musicchannel()) || event.getMember().getId().equals(cfg.twinklewineid())) {
                    if (args.length == 1) {
                        event.getChannel().sendMessage("Использование команды: " + cfg.prefix() + "random (значение) (значение)").queue();
                    } else {
                        try {
                            int a = Integer.parseInt(args[1].trim());
                            int b = Integer.parseInt(args[2].trim());
                            java.util.Random random = new java.util.Random();
                            int diff1 = b - a;
                            int dice1 = random.nextInt(diff1 + 1) + a;
                            event.getChannel().sendMessage(String.valueOf(dice1)).queue();
                        } catch (NumberFormatException nfe) {
                            event.getChannel().sendMessage("Использование команды: " + cfg.prefix() + "random (значение) (значение)").queue();

                        }
                    }
                } else {
                    TextChannel textChannel = event.getGuild().getTextChannelById(cfg.channelused());
                    event.getMessage().reply("**Команды только в ** " + textChannel.getAsMention()).queue();
                }
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Random";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
