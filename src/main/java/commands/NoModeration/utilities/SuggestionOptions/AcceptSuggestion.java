package commands.NoModeration.utilities.SuggestionOptions;

import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;

public class AcceptSuggestion extends Command {

    public AcceptSuggestion()
    {
        this.name = "accept";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        String[] args = event.getMessage().getContentRaw().split("\\s");
        String channel = event.getChannel().getId();
        if (channel.equals(cfg.adminchannel())) {
            if (args.length < 3) {
                event.getChannel().sendMessage("Использование команды: " + cfg.prefix() + "accept (ID сообщения) (Ответ)").queue();
            } else {
                String message = args[1];
                TextChannel textChannel = event.getGuild().getTextChannelById(cfg.suggestionchannel());
                Message message2 = textChannel.retrieveMessageById(args[1]).complete();
                EmbedBuilder builder = new EmbedBuilder(message2.getEmbeds().get(0));
                final StringBuilder stringBuilder = new StringBuilder();
                for (int i = 2; i < args.length; i++) {
                    stringBuilder.append(args[i]).append(" ");
                }
                builder.addField("(Принято!) Ответ от: " + event.getAuthor().getAsTag(), stringBuilder.toString(), false);
                builder.setColor(Color.GREEN);
                textChannel.editMessageById(message, builder.build()).queue();
                builder.clear();
                event.getChannel().sendMessage("Ответ отправлен!").queue();
            }
        } else {
            event.getMessage().reply("**Принимать идеи могут только Админы!**").queue();
        }
    }
}

