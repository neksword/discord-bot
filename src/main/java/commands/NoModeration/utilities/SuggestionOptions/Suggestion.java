package commands.NoModeration.utilities.SuggestionOptions;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;

public class Suggestion extends Command {

    public Suggestion() {
        this.name = "idea";
        this.guildOnly = true;
        this.cooldown = 60;
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        String[] args = event.getMessage().getContentRaw().split("\\s");
        EmbedBuilder embedBuilder = new EmbedBuilder();
        if (args.length == 1) {
            event.getChannel().sendMessage("Использование команды: " + cfg.prefix() + "idea (предложение)").queue();
        } else {
            TextChannel channel = event.getGuild().getTextChannelById(cfg.suggestionchannel());
            embedBuilder.setColor(Color.YELLOW);
            embedBuilder.setAuthor(event.getAuthor().getAsTag(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
            embedBuilder.setFooter(event.getGuild().getName(), event.getGuild().getIconUrl());
            embedBuilder.setTitle("Предложение: ");
            if (!(event.getMessage().getAttachments().size() == 0)) {
                embedBuilder.setImage(event.getMessage().getAttachments().get(0).getUrl());
            }
            for (int i = 1; i < args.length; i++) {
                embedBuilder.appendDescription(args[i]).appendDescription(" ");
            }

            channel.sendMessage(embedBuilder.build()).queue(message -> {
                message.addReaction("\uD83D\uDC4D").queue();
                message.addReaction("\uD83D\uDC4E").queue();
            });
            event.getMessage().reply("Идея успешно отправлена!").queue();
            embedBuilder.clear();

            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Idea";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
