package commands.NoModeration.utilities;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.awt.*;
import java.util.concurrent.TimeUnit;

public class WWWQCommand extends Command {
    public final EventWaiter waiter;
    public WWWQCommand(EventWaiter waiter){
        this.waiter = waiter;
        this.name = "wques";
        this.guildOnly = true;
        this.cooldown = 60;
    }

    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            EmbedBuilder builder = new EmbedBuilder();
            Color purple = new Color(76, 0, 230);
            builder.setColor(purple);
            builder.setAuthor(event.getAuthor().getName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
            builder.setThumbnail(event.getAuthor().getEffectiveAvatarUrl());
            builder.setFooter("ID: " + event.getMember().getId());
            Message message = event.getMessage().reply("Напишите свой вопрос в чат!").complete();
            waiter.waitForEvent(MessageReceivedEvent.class,
                    event1 -> event1.getAuthor().equals(event.getAuthor())
                            && event1.getChannel().equals(event.getChannel())
                            && !event1.getMessage().equals(event.getMessage()),
                    e1 -> {
                        builder.addField("Вопрос", e1.getMessage().getContentRaw(), false);
                        e1.getMessage().delete().queue();
                        message.delete().queue();
                        Message message1 = event.getMessage().reply("Напишите свой ответ в чат!").complete();
                        waiter.waitForEvent(MessageReceivedEvent.class,
                                e2 -> e2.getAuthor().equals(event.getAuthor())
                                        && e2.getChannel().equals(event.getChannel())
                                        && !e2.getMessage().equals(event.getMessage()),
                                e3 -> {
                                    builder.addField("Ответ", e3.getMessage().getContentRaw(), false);
                                    e3.getMessage().delete().queue();
                                    message1.delete().queue();
                                    Message message2 = event.getMessage().reply("Напишите источник информации в чат!").complete();
                                    waiter.waitForEvent(MessageReceivedEvent.class,
                                            receivedEvent -> receivedEvent.getAuthor().equals(event.getAuthor())
                                                    && receivedEvent.getChannel().equals(event.getChannel())
                                                    && !receivedEvent.getMessage().equals(event.getMessage()),
                                            e4 -> {
                                                builder.addField("Источник информации", e4.getMessage().getContentRaw(), false);
                                                e4.getMessage().delete().queue();
                                                message2.delete().queue();
                                                event.getGuild().getTextChannelById("854347360092291112").sendMessage(builder.build()).queue();
                                                event.reply("Ваш вопрос записан!");
                                            },
                                            1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
                                },
                                1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
                    },
                    1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
        }
    }
}
