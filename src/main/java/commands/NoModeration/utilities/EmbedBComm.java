package commands.NoModeration.utilities;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import commands.NoModeration.information.Test.MainTest;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class EmbedBComm extends Command {
    private final EventWaiter waiter;
    public EmbedBComm(EventWaiter waiter){
        this.name = "embed";
        this.guildOnly = true;
        this.ownerCommand = true;
        this.waiter = waiter;
    }
    @Override
    protected void execute(CommandEvent event) {
        Message message = event.getMessage().reply("Введите заголовок (EXIT что бы пропустить)").complete();
        EmbedBuilder builder = new EmbedBuilder();
        waiter.waitForEvent(MessageReceivedEvent.class,
                e -> e.getAuthor().equals(event.getAuthor())
                        && e.getChannel().equals(event.getChannel())
                        && !e.getMessage().equals(event.getMessage()),


                e -> {
                    if (e.getMessage().getContentRaw().toUpperCase(Locale.ROOT).equals("exit")){

                    }
                    else {
                        builder.setTitle(e.getMessage().getContentRaw());
                    }
                    e.getMessage().delete().queue();
                    message.delete().queue();
                    Message message1 = event.getMessage().reply("Введите описание (EXIT что бы пропустить)").complete();
                    waiter.waitForEvent(MessageReceivedEvent.class,
                            e1 -> e1.getAuthor().equals(event.getAuthor())
                                    && e1.getChannel().equals(event.getChannel())
                                    && !e1.getMessage().equals(event.getMessage()),


                            e1 -> {
                                if (e1.getMessage().getContentRaw().toUpperCase(Locale.ROOT).equals("exit")){

                                }
                                else {
                                    builder.setDescription(e1.getMessage().getContentRaw());
                                }
                                Message message2 = event.getMessage().reply("Введите ссылку на изображение (использование) (EXIT что бы пропустить)").complete();
                                message1.delete().queue();
                                e1.getMessage().delete().queue();
                                waiter.waitForEvent(MessageReceivedEvent.class,
                                        e2 -> e2.getAuthor().equals(event.getAuthor())
                                                && e2.getChannel().equals(event.getChannel())
                                                && !e2.getMessage().equals(event.getMessage()),


                                        e2 -> {
                                            if (e2.getMessage().getContentRaw().toUpperCase(Locale.ROOT).equals("exit")){

                                            }
                                            else {
                                                try {
                                                    builder.setImage(e2.getMessage().getContentRaw());
                                                }
                                                catch (Exception exception){
                                                    event.replyError("Укажите изображение)");
                                                }
                                            }
                                            message2.delete().queue();
                                            event.getChannel().sendMessage(builder.build()).queue();
                                        },


                                        1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
                            },


                            1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
                },


                1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
    }
}
