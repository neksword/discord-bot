package commands.NoModeration.utilities.Tickets;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class TicketsNew extends ListenerAdapter {
    private final Map<String, TicketsInfo> ticketCache = new ConcurrentHashMap<>();
    BotConfig cfg = ConfigFactory.create(BotConfig.class);

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        if (command[0].equalsIgnoreCase(cfg.prefix() + "tclose")) {
            if (event.getAuthor().isBot()) {
                return;
            } else {
                System.out.println(ticketCache);
                if (ticketCache.containsKey(event.getChannel().getId())) {
                    List<Role> removedRoles = event.getMember().getRoles();
                    Optional<Role> roleContainer = removedRoles.stream().filter(role -> role.getId().equals(cfg.adminrole())).findAny();
                    if (roleContainer.isPresent()) {
                        event.getMessage().reply("**Канал будет удалён через 10 секунд.**").queue();
                        event.getChannel().delete().queueAfter(10, TimeUnit.SECONDS);
                        ticketCache.remove(event.getChannel().getId());
                    } else {
                        event.getMessage().reply("**У вас нет прав на использование этой команды!**").queue();
                    }
                } else {
                    event.getMessage().reply("**Здесь нельзя использовать эту команду!**").queue();
                }
            }
        }
    }

    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        if (event.getReaction().getChannel().getId().equals("809861489312661565")) {
            if (event.getMessageId().equals("844600529174265877")) {
                if (event.getReaction().getReactionEmote().getName().equals("⚠️")) {
                    if (event.getMember().getUser().isBot()) {
                        return;
                    } else {
                        event.getReaction().removeReaction(event.getMember().getUser()).queue();
                        Category category = event.getGuild().getCategoryById("809858909786734633");
                        TextChannel channel = event.getGuild().createTextChannel(event.getMember().getUser().getName() + "-ticket", category).complete();
                        List<Permission> permissions = Arrays.asList(Permission.VIEW_CHANNEL, Permission.MESSAGE_WRITE, Permission.MESSAGE_HISTORY);
                        channel.putPermissionOverride(event.getMember()).setAllow(permissions).queue();
                        Role userrole = event.getGuild().getRoleById(cfg.userrole());
                        channel.upsertPermissionOverride(userrole).deny(Permission.VIEW_CHANNEL).queue();
                        channel.upsertPermissionOverride(event.getGuild().getRoleById("807879328704364554")).deny(Permission.VIEW_CHANNEL).queue();
                        Role adminrole = event.getGuild().getRoleById(cfg.adminrole());
                        channel.putPermissionOverride(adminrole).setAllow(permissions).queue();
                        TicketsInfo ticketsInfo = new TicketsInfo(event.getMember(), channel);
                        ticketCache.put(channel.getId(), ticketsInfo);
                        System.out.println(ticketCache.toString() + " | ");
                        channel.sendMessage(adminrole.getAsMention()).queue();
                        channel.sendMessage(new EmbedBuilder()
                                .setTitle("Инструкция:")
                                .setDescription("Для закрытия тикета используете - " + cfg.prefix() + "tclose")
                                .setColor(Color.RED)
                                .setFooter(event.getGuild().getName(), event.getGuild().getIconUrl())
                                .build()
                        ).queue();
                    }
                }
            }
        }
    }
}
