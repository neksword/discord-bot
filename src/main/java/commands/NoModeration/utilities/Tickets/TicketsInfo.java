package commands.NoModeration.utilities.Tickets;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;

public class TicketsInfo {
    private final String channelid;
    private final String memberid;

    public TicketsInfo(final Member member, final TextChannel channel) {
        this.channelid = channel.getId();
        this.memberid = member.getId();
    }

    public String getChannelid() {
        return channelid;
    }

    public String getMemberid() {
        return memberid;
    }
}
