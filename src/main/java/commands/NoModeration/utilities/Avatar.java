package commands.NoModeration.utilities;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.Member;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.util.List;

public class Avatar extends Command {

    public Avatar() {
        this.name = "avatar";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            List<Member> mentionedmember = event.getMessage().getMentionedMembers();
            TextChannel channel = event.getTextChannel();
            if (mentionedmember.isEmpty()) {
                EmbedBuilder member = new EmbedBuilder();
                Member target = event.getMember();
                member.setAuthor(target.getEffectiveName(), target.getUser().getAvatarUrl(), target.getUser().getEffectiveAvatarUrl());
                member.setImage(target.getUser().getAvatarUrl() + "?size=512");
                Color purple = new Color(76, 0, 230);
                member.setColor(purple);
                channel.sendMessage(member.build()).queue();
                member.clear();
            } else {
                Member target1 = mentionedmember.get(0);
                EmbedBuilder member1 = new EmbedBuilder();
                member1.setAuthor(target1.getEffectiveName(), target1.getUser().getAvatarUrl(), target1.getUser().getEffectiveAvatarUrl());
                member1.setImage(target1.getUser().getAvatarUrl() + "?size=512");
                Color purple = new Color(76, 0, 230);
                member1.setColor(purple);

                channel.sendMessage(member1.build()).queue();
                member1.clear();

            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Avatar";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}