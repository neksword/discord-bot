package commands.NoModeration.information;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.Economy.DBNewUser;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mongodb.client.MongoCollection;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

public class UserInfo extends Command {
    private final MongoCollection<Document> collection;
    public UserInfo(MongoCollection<Document> collection) {
        this.name = "userinfo";
        this.guildOnly = true;
        this.aliases = new String[]{"user"};
        this.collection = collection;
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            Message message = event.getMessage();
            List<Member> mentionedmember = message.getMentionedMembers();
            if (mentionedmember.isEmpty()) {
                long id = Long.parseLong(event.getMember().getId());
                Document document = collection.find(new Document("id",id)).first();
                if (document == null){
                    DBNewUser dbNewUser = new DBNewUser();
                    dbNewUser.NewUser(collection,event.getMember());
                }
                    Member target = event.getMember();
                    EmbedBuilder user = new EmbedBuilder();
                    if (target.getNickname() == null) {
                        String nick = "Нет серверного ника";
                        user.setTitle(target.getUser().getName() + " (" + nick + ")");
                    } else {
                        user.setTitle(target.getUser().getName() + " (" + target.getNickname() + ")");
                    }
                    user.setAuthor(target.getUser().getName(), target.getUser().getAvatarUrl(), target.getUser().getEffectiveAvatarUrl());
                    user.setThumbnail(target.getUser().getEffectiveAvatarUrl());
                    Color purple = new Color(76, 0, 230);
                    user.setColor(purple);
                    if (!target.getActivities().isEmpty()) {
                        user.addField("Информация", "**Имя:** " + target.getUser().getAsTag() + "\n" + "**Статус:** " + emoji(target.getOnlineStatus()) + "\n" + "**Активность:** " + getActivities(target).toString() + "\n" + "**Пользовательский статус:** " + getStatus(target) + "\n" + "**Высшая роль:** " + role(target) + "\n" + "**\uD83C\uDF6AПеченек:** " + document.get("cookies") + "\n" + "**Присоединился:** " + target.getTimeJoined().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "\n" + "**Дата создания аккаунта:** " + target.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), false);
                    } else {
                        user.addField("Информация", "**Имя:** " + target.getUser().getAsTag() + "\n" + "**Статус:** " + emoji(target.getOnlineStatus()) + "\n" + "**Активность:** " + "_нет активности_" + "\n" + "**Пользовательский статус:** " + getStatus(target) + "\n" + "**Высшая роль:** " + role(target) + "\n" + "**\uD83C\uDF6AПеченек:** " + document.get("cookies") + "\n" + "**Присоединился:** " + target.getTimeJoined().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "\n" + "**Дата создания аккаунта:** " + target.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), false);
                    }
                    user.setFooter("ID: " + target.getId());
                    event.getChannel().sendMessage(user.build()).queue();
                    user.clear();
            } else if (!mentionedmember.isEmpty()) {
                if (mentionedmember.get(0).getUser().isBot()) {
                    return;
                }
                long id = Long.parseLong(mentionedmember.get(0).getId());
                Document document = collection.find(new Document("id", id)).first();
                if (document == null) {
                    DBNewUser dbNewUser = new DBNewUser();
                    dbNewUser.NewMentionUser(collection, mentionedmember);
                }
                Member target1 = mentionedmember.get(0);
                EmbedBuilder user1 = new EmbedBuilder();
                if (target1.getNickname() == null) {
                    String nick = "Нет серверного ника";
                    user1.setTitle(target1.getUser().getName() + " (" + nick + ")");
                } else {
                    user1.setTitle(target1.getUser().getName() + " (" + target1.getNickname() + ")");
                }
                user1.setAuthor(target1.getUser().getName(), target1.getUser().getAvatarUrl(), target1.getUser().getEffectiveAvatarUrl());
                user1.setThumbnail(target1.getUser().getEffectiveAvatarUrl());
                Color purple = new Color(76, 0, 230);
                user1.setColor(purple);
                if (!target1.getActivities().isEmpty()) {
                    user1.addField("Информация", "**Имя:** " + target1.getUser().getAsTag() + "\n" + "**Статус:** " + emoji(target1.getOnlineStatus()) + "\n" + "**Активность:** " + getActivities(target1).toString() + "\n" + "**Пользовательский статус:** " + getStatus(target1) + "\n" + "**Высшая роль:** " + role(target1) + "\n" + "**\uD83C\uDF6AПеченек:** " + document.get("cookies") + "\n" + "**Присоединился:** " + target1.getTimeJoined().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "\n" + "**Дата создания аккаунта:** " + target1.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), false);
                } else {
                    user1.addField("Информация", "**Имя:** " + target1.getUser().getAsTag() + "\n" + "**Статус:** " + emoji(target1.getOnlineStatus()) + "\n" + "**Активность:** " + "_нет активности_" + "\n" + "**Пользовательский статус:** " + getStatus(target1) + "\n" + "**Высшая роль:** " + role(target1) + "\n" + "**\uD83C\uDF6AПеченек:** " + document.get("cookies") + "\n" + "**Присоединился:** " + target1.getTimeJoined().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "\n" + "**Дата создания аккаунта:** " + target1.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), false);
                }
                user1.setFooter("ID: " + target1.getId());
                event.getChannel().sendMessage(user1.build()).queue();
                user1.clear();
            }
                TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
                LogCommand logCommand = new LogCommand();
                String commandname = "Userinfo";
                logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
    public String emoji(OnlineStatus status) {
        switch (status) {
            case ONLINE:
                return "\uD83D\uDFE2 Онлайн";
            case IDLE:
                return "\uD83C\uDF19 Не активен";
            case DO_NOT_DISTURB:
                return "⛔ Не беспокоить";
            case INVISIBLE:
                return "\uD83D\uDD32 Не видимый";
            case OFFLINE:
                return "\uD83D\uDD32 Не в сети";
        }
        return "АШИБКА";
    }
    public String role(Member member){
        if (member.getRoles().isEmpty()){
            return "_нет ролей_";
        }
        else {
            return member.getRoles().get(0).getAsMention();
        }
    }
    private StringBuilder getActivities(Member member) {
        StringBuilder commonBuilder = new StringBuilder();
        Iterator<Activity> iterable = member.getActivities().iterator();
        while (iterable.hasNext()) {
            Activity activity = iterable.next();
            String activityText = activity.getName();
            if (activity.getType().equals(Activity.ActivityType.CUSTOM_STATUS)){

            }
            else {
                appendEntry(commonBuilder, activityText);
            }
        }
        return commonBuilder;
    }
    private String getStatus(Member member){
        Iterator<Activity> iterator = member.getActivities().iterator();
        while (iterator.hasNext()){
            Activity activity = iterator.next();
            if (activity != null){
                if (activity.getType().equals(Activity.ActivityType.CUSTOM_STATUS)){
                    return activity.getName();
                }
                else {
                    return "_нет статуса_";
                }
            }
        }
        return "_нет статуса_";
    }
    private StringBuilder appendEntry(StringBuilder commonBuilder, String value) {
        return commonBuilder
                .append(value)
                .append(" ");
    }
}

