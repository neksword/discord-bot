package commands.NoModeration.information;

import java.time.format.DateTimeFormatter;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.utils.FinderUtil;
import vortex.FormatUtil;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import vortex.commands.CommandExceptionListener;

public class RoleInfo extends Command {

    private final static String LINESTART = "\u25AB"; // ▫
    private final static String ROLE_EMOJI = "\uD83C\uDFAD"; // 🎭

    private MongoCollection<Document> collection;

    public RoleInfo(MongoCollection<Document> collection) {
        this.collection = collection;
        this.guildOnly = true;
        this.name = "roleinfo";
        this.aliases = new String[]{"role"};
        this.arguments = "<role>";
    }

    @Override
    protected void execute(CommandEvent event) {
        if (event.getMessage().getMentionedRoles().isEmpty()) {

        } else {
            Role role;
            if (event.getArgs().isEmpty())
                throw new CommandExceptionListener.CommandErrorException("Please provide the name of a role!");
            else {
                List<Role> found = FinderUtil.findRoles(event.getArgs(), event.getGuild());
                if (found.isEmpty()) {
                    event.replyError("I couldn't find the role you were looking for!");
                    return;
                } else if (found.size() > 1) {
                    event.replyWarning(FormatUtil.filterEveryone(FormatUtil.listOfRoles(found, event.getArgs())));
                    return;
                } else {
                    role = found.get(0);
                }
            }

            String title = ROLE_EMOJI + " Информация о **" + role.getName() + "**:";
            List<Member> list = role.isPublicRole() ? event.getGuild().getMembers() : event.getGuild().getMembersWithRoles(role);
            StringBuilder desr = new StringBuilder(LINESTART + "ID: **" + role.getId() + "**\n"
                    + LINESTART + "Дата создания: **" + role.getTimeCreated().format(DateTimeFormatter.RFC_1123_DATE_TIME) + "**\n"
                    + LINESTART + "Позиция: **" + role.getPosition() + "**\n"
                    + LINESTART + "Цвет: **#" + (role.getColor() == null ? "000000" : Integer.toHexString(role.getColor().getRGB()).toUpperCase().substring(2)) + "**\n"
                    + LINESTART + "Упоминание: **" + role.isMentionable() + "**\n"
                    + LINESTART + "Отображение: **" + role.isHoisted() + "**\n"
                    + LINESTART + "Управляемая: **" + role.isManaged() + "**\n"
                    + LINESTART + "Права: ");
            if (role.getPermissions().isEmpty())
                desr.append("None");
            else
                desr.append(role.getPermissions().stream().map(p -> "`, `" + p.getName()).reduce("", String::concat).substring(3)).append("`");
            desr.append("\n").append(LINESTART).append("Пользователи: **").append(list.size()).append("**\n");
            if (list.size() * 24 <= 2048 - desr.length())
                list.forEach(m -> desr.append("<@").append(m.getUser().getId()).append("> "));

            event.reply(new MessageBuilder()
                    .append(FormatUtil.filterEveryone(title))
                    .setEmbed(new EmbedBuilder()
                            .setDescription(desr.toString().trim())
                            .setColor(role.getColor()).build())
                    .build());
        }
    }
}
