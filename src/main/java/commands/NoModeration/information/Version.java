package commands.NoModeration.information;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;

public class Version extends Command {

    public Version() {
        this.name = "version";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            TextChannel textChannel = event.getTextChannel();
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setFooter("TwinkleWine and Neksword", "https://cdn.discordapp.com/avatars/387521150281973760/52e066b7a70817aa8e571a4f9e8f82d1.webp?size=2048");
            embedBuilder.setTitle(cfg.name());
            embedBuilder.setDescription("Текущая версия: " + "`" + cfg.version() + "`");
            embedBuilder.setThumbnail(cfg.avatar());
            Color purple = new Color(76, 0, 230);
            embedBuilder.setColor(purple);
            embedBuilder.addField("Добавлено: ", "1. Изменён способ отправки логов." + "\n" + "2. Добавлена команда: version" + "\n" + "3. Добавлена команда: fox" + "\n" + "4. Добавлена проверка каналов", false);
            textChannel.sendMessage(embedBuilder.build()).queue();
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Version";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}

