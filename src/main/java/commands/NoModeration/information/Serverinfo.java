package commands.NoModeration.information;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class Serverinfo extends Command {

    public Serverinfo() {
        this.name = "serverinfo";
        this.guildOnly = true;
        this.aliases = new String[]{"server"};
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            Guild guild = event.getGuild();
            Member member = event.getMember();
            TextChannel channel = event.getTextChannel();
            List<User> users = guild.getMembers().stream().map(Member::getUser)
                    .filter(user -> !user.isBot()).collect(Collectors.toList());
            EmbedBuilder serverinfo = new EmbedBuilder();
            serverinfo.setAuthor(member.getUser().getName(), member.getUser().getAvatarUrl(), member.getUser().getEffectiveAvatarUrl());
            serverinfo.setTitle(guild.getName());
            int userss = guild.getMemberCount() - users.size();
            serverinfo.addField("Пользователи: ", "Участников: " + "`" + users.size() + "`" + "\n" + "Ботов: " + "`" + userss + "`", false);
            //serverinfo.addField("Статусы: ","Онлайн: " + guild.getSelfMember().getUser().,true);
            serverinfo.addField("Каналы и категории: ", "Категорий: " + "`" + guild.getCategoryCache().stream().count() + "`" + "\n" + "Текстовые: " + "`" + guild.getTextChannelCache().stream().count() + "`" + "\n" + " Голосовые: " + "`" + guild.getVoiceChannelCache().stream().count() + "`", true);
            serverinfo.addField("Регион: ", "`" + guild.getRegion() + "`", true);
            serverinfo.addField("Дата создания: ", "`" + guild.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "`", true);
            serverinfo.addField("Уровень верификации: ", "`" + guild.getVerificationLevel() + "`", true);
            serverinfo.addField("Владелец: ", guild.getOwner().getAsMention(), false);
            Color purple = new Color(76, 0, 230);
            serverinfo.setColor(purple);
            serverinfo.setFooter("ID: " + guild.getId());
            serverinfo.setThumbnail(guild.getIconUrl());

            channel.sendMessage(serverinfo.build()).queue();
            serverinfo.clear();
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Serverinfo";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
