package commands.NoModeration.information.Help;

import Audit.LogCommand;
import Audit.MessageDelete.MessageData;
import BotConfig.BotConfig;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;

public class Help extends ListenerAdapter {
    private final Map<String, HelpData> HelpCache = new ConcurrentHashMap<>();
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] args = event.getMessage().getContentRaw().split("\\s");
        if (args[0].equalsIgnoreCase(cfg.prefix() + "help") || args[0].equalsIgnoreCase("D!help")) {
            final UsedChannel usedChannel = new UsedChannel();
            if (usedChannel.Return(event.getMember(), event.getChannel(), event.getMessage())) {
                if (args.length == 1) {
                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                    builder.setTitle("Дворецкий");
                    builder.addField("By TwinkleWine", "Версия: " + "`" + cfg.version() + "`" + "\n" + "Дата последнего обновления: " + "`" + cfg.updatedate() + "`", false);
                    builder.addField("\uD83E\uDD16Информация о боте", "`Help`" + " - покажет список всех команд." + "\n" + "`Botinfo`" + " - информация о боте." + "\n" + "`Version`" + " - последняя версия.", false);
                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                    Color purple = new Color(76, 0, 230);
                    builder.setColor(purple);
                    builder.setThumbnail(cfg.avatar());
                    builder.setFooter("Page 0 of 10");
                    Message message = event.getMessage().reply(builder.build()).complete();
                    message.addReaction("0️⃣").queue();
                    message.addReaction("1️⃣").queue();
                    message.addReaction("2️⃣").queue();
                    message.addReaction("3️⃣").queue();
                    message.addReaction("4️⃣").queue();
                    message.addReaction("5️⃣").queue();
                    message.addReaction("6️⃣").queue();
                    message.addReaction("7️⃣").queue();
                    message.addReaction("8️⃣").queue();
                    message.addReaction("9️⃣").queue();
                    message.addReaction("\uD83D\uDD1F").queue();
                        final HelpData helpData = new HelpData(event.getMember(), message);
                        HelpCache.put(message.getId(), helpData);
                    TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
                    LogCommand logCommand = new LogCommand();
                    String commandname = "Help";
                    channel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getChannel()).build()).queue();
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                    Runnable runnable = () -> {
                        System.out.println(">>>>>>>>>>>>>>>>>>>>>>");//запустился
                        final List<MessageReaction> messageReactions = message.getReactions();
                        message.removeReaction("0️⃣").queue();
                        message.removeReaction("1️⃣").queue();
                        message.removeReaction("2️⃣").queue();
                        message.removeReaction("3️⃣").queue();
                        message.removeReaction("4️⃣").queue();
                        message.removeReaction("5️⃣").queue();
                        message.removeReaction("6️⃣").queue();
                        message.removeReaction("7️⃣").queue();
                        message.removeReaction("8️⃣").queue();
                        message.removeReaction("9️⃣").queue();
                        message.removeReaction("\uD83D\uDD1F").queue();
                        HelpCache.remove(message.getId());
                        System.out.println(message.getId());
                        System.out.println(HelpCache.toString());
                        System.out.println(messageReactions);
                    };
                    ScheduledFuture<?> reactionDelete = scheduler.schedule(runnable,1, TimeUnit.MINUTES);
                } else {
                    TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
                    LogCommand logCommand = new LogCommand();
                    String commandname = "Help";
                    channel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getChannel()).build()).queue();
                    EmbedBuilder embedBuilder = new EmbedBuilder();
                    embedBuilder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());//то же самое здесь, да и вообще много где ты ниже полистай. switch лучше делать через перечисление, а не строчками. поэтому я  и говорю тебе рефакторить и смотреть код ревью. все, я ушел в магаз, давай ок

                    embedBuilder.setDescription("ℹ️Информация о команде");
                    Color purple = new Color(76, 0, 230);
                    embedBuilder.setColor(purple);
                    switch (args[1]) {
                        case "help":
                            embedBuilder.addField("Help", "Покажет список всех команд.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "botinfo":
                            embedBuilder.addField("Botinfo", "Покажет информацию о боте.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "version":
                            embedBuilder.addField("Version", "Информация о версии.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "userinfo":
                            embedBuilder.addField("Userinfo", "Информация о участнике.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "serverinfo":
                            embedBuilder.addField("Serverinfo", "Информация о сервере.", false);
                            embedBuilder.addField("‼️стались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "ban":
                            embedBuilder.addField("Ban", "Забанит участника.\n Использование - ban @упоминание", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "kick":
                            embedBuilder.addField("Kick", "Кикнет участника.\n Использование - kick @упоминание", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "nick":
                            embedBuilder.addField("Nick", "Сменит ник учаснику.\n Использование - nick @упоминание (новый ник)", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "giverole":
                            embedBuilder.addField("Giverole", "Выдаст роль участнику.\n Использование - giverole @упоминание @роль", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "removerole":
                            embedBuilder.addField("Removerole", "Снимет роль у участника.\n Использование - removerole @упоминание @роль", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "idea":
                            embedBuilder.addField("Idea", "Отправит идею.\n Использование - idea (текст)", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "avatar":
                            embedBuilder.addField("Avatar", "Покажет аватар.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "random":
                            embedBuilder.addField("Random", "Отправит рандомное число.\n Использование - random (число) (число)", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "ping":
                            embedBuilder.addField("Ping", "Покажет пинг бота.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "cat":
                            embedBuilder.addField("Cat", "Покажет кота.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "dog":
                            embedBuilder.addField("Dog", "Покажет собаку.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "fox":
                            embedBuilder.addField("Fox", "Покажет лису.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "coin":
                            embedBuilder.addField("Coin", "Подбросит монетку.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "meme":
                            embedBuilder.addField("Meme", "Покажет мем.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "say":
                            embedBuilder.addField("Say", "Повторит слова.\n Использование - say (текст)", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "vname":
                            embedBuilder.addField("VName", "Сменит имя комнаты.\n Использование - vname (текст)", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "vmax":
                            embedBuilder.addField("VMax", "Изменит ограничение участников в комнате.\n Использование - vmax (число)", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "vclose":
                            embedBuilder.addField("VClose", "Закроет комнату.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "vopen":
                            embedBuilder.addField("VOpen", "Откроет комнату.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "vkick":
                            embedBuilder.addField("VKick", "Кикнет учасника из комнаты и зкакроет доступ.\n Использование - vkick @упоминание", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "vmopen":
                            embedBuilder.addField("VMOpen", "Откроет участнику доступ в комнату.\n Использование - vmopen @упоминание", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "L&D":
                            embedBuilder.addField("L&D", "Поставит реакции лайки и дизлайка.", false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "level":
                            embedBuilder.addField("Level","Покажет рейтинг учасника.",false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                        case "911":
                            embedBuilder.addField("911","Позовёт администрацию к вам на помощь.",false);
                            embedBuilder.addField("‼️Остались вопросы?", "Написать создателю - [TwinkleWine](https://discord.com/users/387521150281973760)", false);
                            event.getMessage().reply(embedBuilder.build()).queue();
                            break;
                    }
                }
            }
        }
    }

    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        Message message = event.retrieveMessage().complete();
        if (!(event.getMember().getId().equals(cfg.botid()))) {
            if (message.getAuthor().getId().equals(cfg.botid())) {
                String messageID = event.getMessageId();
                if (HelpCache.get(messageID) != null) {
                    String MemberID = HelpCache.get(messageID).getMemberid();
                    if (event.getMember().getId().equals(MemberID)) {
                        MessageEmbed.Footer footer = message.getEmbeds().get(0).getFooter();
                        System.out.println(footer.getText());
                        BotConfig cfg = ConfigFactory.create(BotConfig.class);
                        System.out.println(HelpCache.get(event.getMessageId()).getMemberid());
                        switch (Objects.requireNonNull(footer.getText())) {
                            case "Page 0 of 10":
                            case "Page 1 of 10":
                            case "Page 2 of 10":
                            case "Page 3 of 10":
                            case "Page 4 of 10":
                            case "Page 5 of 10":
                            case "Page 6 of 10":
                            case "Page 7 of 10":
                            case "Page 8 of 10":
                            case "Page 9 of 10":
                            case "Page 10 of 10":
                                if (event.getReaction().getReactionEmote().getAsReactionCode().equals("0️⃣")){
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    builder.setTitle("Дворецкий");
                                    builder.addField("By TwinkleWine", "Версия: " + "`" + cfg.version() + "`" + "\n" + "Дата последнего обновления: " + "`" + cfg.updatedate() + "`", false);
                                    builder.addField("\uD83E\uDD16Информация о боте", "`Help`" + " - покажет список всех команд." + "\n" + "`Botinfo`" + " - информация о боте." + "\n" + "`Version`" + " - последняя версия.", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setThumbnail(cfg.avatar());
                                    builder.setFooter("Page 0 of 10");
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                }
                                else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("1️⃣")) {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("\uD83D\uDCB7Экономика","`Level`" + " - просмотреть свой рейтинг.", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 1 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                } else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("2️⃣")) {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("\uD83D\uDD75️\u200D♂️Информация", "`Userinfo`" + " - информация о пользователе." + "\n" + "`Serverinfo`" + " - информация о сервере." + "\n" + "`Level`" + " - рейтинг участника.", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 2 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                } else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("3️⃣")) {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    builder.setColor(Color.RED);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("❌Модерирование", "`Ban`" + " - забанить участника." + "\n" + "`Kick`" + " - выгнать учасника." + "\n" + "`Clear`" + " - очистить сообщения." + "\n" + "`Nick`" + " - изменить никнейм." + "\n" + "`Giverole`" + " - выдать роль участнику." + "\n" + "`Removerole`" + " - снять роль у участника.", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 3 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                } else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("4️⃣")) {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("\uD83D\uDCD9Идеи", "`Idea`" + " - отправить идею." + "\n" + "`Accept`" + " - принять идею." + "\n" + "`Deny`" + " - отклонить идею.", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 4 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                } else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("5️⃣")) {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("\uD83D\uDD0AГолосовые комнаты", "`VName` - изменить имя комнаты. " + "\n" + "`VMax` - указать число пользователей. " + "\n" + "`Vclose` - закрыть комнату. " + "\n" + "`VOpen` - открыть комнату. " + "\n" + "`VKick` - выгнать человека из комнаты и закрыть доступ. " + "\n" + "`VMOpen` - открыть человеку доступ в комнату. ", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 5 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                } else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("6️⃣")) {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("\uD83D\uDCCCПолезности", "`Avatar`" + " - покажет аватар." + "\n" + "`Random`" + " - выкинет рандомное число." + "\n" + "`Ping`" + " - покажет пинг бота.", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 6 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                } else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("7️⃣")) {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("\uD83C\uDF89Развлечения", "`Cat`" + " - покажет кота." + "\n" + "`Dog`" + " - покажет собаку." + "\n" + "`Fox`" + " - покажет лису." + "\n" + "`Coin`" + " - подкинет монетку." + "\n" + "`Meme`" + " - выкинет мем." + "\n" + "`Say`" + " - повторит слово." + "\n" + "`Cookies`" + " - дать печеньку участнику." + "\n" + "`Kiss`" + " - поцеловаться." + "\n" + "`Hug`" + " - обняться." + "\n" + "`Cuddle`" + " - прижаться." + "\n" + "`Feed`" + " - покормиться.(баги)" + "\n" + "`Pat`" + " - погладить." + "\n" + "`Slap`" + " - ударить.(баги)" + "\n" + "`Poke`" + " - тыкнуть.(баги)", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 7 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                } else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("8️⃣")) {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("\uD83C\uDFB5Музыка", "`Play`" + " - включить трек." + "\n" + "`Skip`" + " - пропустить трек." + "\n" + "`Queue`" + " - посмотреть трек." + "\n" + "`Stop`" + " - остановить музыку." + "\n" + "`Pause`" + " - поставить на паузу." + "\n" + "`Resume`" + " - снять с паузы." + "\n" + "`Volume`" + " - изменит громкость.", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 8 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                } else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("9️⃣")) {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("\uD83D\uDECEТриггеры", "`L&D`" + " - поставит реакции лайка и дизлайка на сообщение." + "\n" + "`911`" + " - позовёт администрацию к вам на помощь.", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 9 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                } else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("\uD83D\uDD1F")){
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor("Дворецкий", cfg.avatar(), cfg.avatar());
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    builder.setTitle("Дворецкий");
                                    builder.addField("\uD83C\uDF81Кейсы","`Shopcase`" + " - купить кейс." + "\n" + "`Case (номер кейса)`" + " - открыть кейс.",false);
//                                    builder.addField("\uD83C\uDFAEИгры","`Phasma`" + " - начнет игру phasmaphobia." + "\n" + "`Dice`" + " - подбросит кубики." + "\n" + "`Kvazar`" + " - начнет игру квазар.", false);
                                    builder.addField("⁉️Детали", "Для получения дополнительной информации по командам используйте `" + cfg.prefix() + "Help [команда]`", false);
                                    builder.addField("\uD83D\uDD17Ссылки", "[TwinkleWine](https://discord.com/users/387521150281973760)" + " | " + "[Дворецкий](https://discord.com/users/776486694911344640)", false);
                                    builder.setFooter("Page 10 of 10");
                                    builder.setThumbnail(cfg.avatar());
                                    message.editMessage(builder.build()).queue();
                                    event.getReaction().removeReaction(event.getUser()).queue();
                                    return;
                                }
                        }
                    } else {
                        event.getReaction().removeReaction(event.getUser()).queue();
                    }
                }
            }
        }
    }
}


