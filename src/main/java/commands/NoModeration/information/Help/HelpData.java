package commands.NoModeration.information.Help;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;

public class HelpData {
    private String memberid;
    private String messageID;
    public HelpData(final Member member,final Message message) {
        this.memberid = member.getId();
        this.messageID = message.getId();
    }
    public String getMemberid()
    {
        return memberid;
    }
    public String getMessageID(){
        return  messageID;
    }
}
