package commands.NoModeration.information;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class Botinfo extends Command {

    public Botinfo() {
        this.name = "botinfo";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            Member target = event.getMember();
            event.getMessage().reply("загрузка").queue(message -> {
                long ping = event.getMessage().getTimeCreated().until(message.getTimeCreated(), ChronoUnit.MILLIS);
                EmbedBuilder info = new EmbedBuilder();
                info.setAuthor(target.getUser().getName(), target.getUser().getAvatarUrl(), target.getUser().getEffectiveAvatarUrl());
                info.setTitle(cfg.name());
                info.setDescription("Discordbot for Java" + "\n" + "Версия: " + "`" + cfg.version() + "`" + "\n" + "Дата последнего обновления: " + "`" + cfg.updatedate() + "`");
                info.addField("Команды:", "Введите: " + cfg.prefix() + "help", true);
                info.addField("Ping","Ping: " + ping + "ms | Websocket: " + event.getJDA().getGatewayPing() + "ms",false);
                info.addField("Дата создания", String.valueOf(event.getSelfMember().getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))),true);
                info.addField("Команд", String.valueOf(event.getClient().getCommands().size()),true);
                info.addField("Создатели:", "[TwinkleWine](https://discord.com/users/387521150281973760)" + "\n" + "Neksword", false);
                //: [Ссылка](" + "https://discord.com/users/387521150281973760" + ")"
                //: [Ссылка](" + "https://discord.com/users/329246031055421441" + ")"
                Color purple = new Color(76, 0, 230);
                info.setColor(purple);
                info.setThumbnail(cfg.avatar());
                info.setFooter("TwinkleWine and Neksword", "https://cdn.discordapp.com/avatars/387521150281973760/52e066b7a70817aa8e571a4f9e8f82d1.webp?size=2048");
                message.delete().queue();
                event.getMessage().reply(info.build()).queue();
            });

            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Botinfo";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}



