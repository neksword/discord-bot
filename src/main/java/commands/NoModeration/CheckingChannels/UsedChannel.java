package commands.NoModeration.CheckingChannels;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

public class UsedChannel {
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public boolean Bot(final Member member){
            if (member.getUser().isBot()) {
                return true;
            } else {
                return false;
            }
    }
    public boolean Channel(final Member member,final TextChannel textChannel){
        if (textChannel.getId().equals(cfg.channelused()) || textChannel.getId().equals(cfg.musicchannel()) || member.getId().equals(cfg.twinklewineid())){
            return true;
        }
        else {
            return false;
        }
    }
    public boolean Return(final Member member, final TextChannel textChannel, final Message message){
        if (Bot(member)){
            return false;
        }
        else if (Channel(member, textChannel)){
            return true;
        }
        else if (!Channel(member, textChannel)){
            Info(textChannel, message);
            return false;
        }
        return false;
    }
    public void Info(final TextChannel textChannel, final Message message){
                TextChannel Channel = textChannel.getGuild().getTextChannelById(cfg.channelused());
                message.reply("**Команды только в ** " + Channel.getAsMention()).queue();

    }
}
