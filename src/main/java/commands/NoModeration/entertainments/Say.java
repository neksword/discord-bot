package commands.NoModeration.entertainments;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;

public class Say extends Command {

    public Say()
    {
        this.name = "say";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            TextChannel channel = event.getTextChannel();
            String[] args = event.getMessage().getContentRaw().split("\\s");
            if (args.length == 1) {
                channel.sendMessage("Укажите слово для повтора!").queue();
            } else {
                final EmbedBuilder builderembed = new EmbedBuilder();
                Color purple = new Color(76, 0, 230);
                builderembed.setColor(purple);
                for (int i = 1; i < args.length; i++) {
                    builderembed.appendDescription(args[i]).appendDescription(" ");
                }
                channel.sendMessage(builderembed.build()).queue();
                builderembed.clear();
            }
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Say";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
