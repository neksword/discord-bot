package commands.NoModeration.entertainments;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.entities.TextChannel;

public class Say2 extends Command {

    public Say2() {
        this.name = "say2";
        this.guildOnly = true;
        this.ownerCommand = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getMessage().getContentRaw().split("\\s");
        if (event.isOwner()){
            TextChannel channel = event.getTextChannel();
            if (args.length == 1) {
                channel.sendMessage("Укажите слово для повтора!").queue();
            } else {
                final StringBuilder stringBuilder = new StringBuilder();
                for (int i = 1; i < args.length; i++) {
                    stringBuilder.append(args[i]).append(" ");
                }
                event.getChannel().sendMessage(stringBuilder.toString()).queue();
            }
        }
    }
}
