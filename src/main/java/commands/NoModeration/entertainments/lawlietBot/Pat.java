package commands.NoModeration.entertainments.lawlietBot;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;

import java.awt.*;
import java.util.Random;

public class Pat extends Command {

    public Pat() {
        this.name = "pat";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setAuthor("\uD83E\uDD1A Гладить");
            if (event.getMessage().getMentionedMembers().isEmpty()) {
                builder.setDescription("**" + event.getAuthor().getName() + "** погладил кого-то!");
            } else {
                if (event.getMessage().getMentionedMembers().size() > 1) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < event.getMessage().getMentionedMembers().size(); i++) {
                        stringBuilder.append("**" + event.getMessage().getMentionedMembers().get(i).getEffectiveName() + "**, ").append(" ");
                    }
                    builder.setDescription(stringBuilder.toString() + " вас погладил **" + event.getMember().getEffectiveName() + "**!");
                } else {
                    builder.setDescription("**" + event.getMessage().getMentionedMembers().get(0).getEffectiveName() + "**, вас погладил **" + event.getMember().getEffectiveName() + "**!");
                }
            }
            if (event.getMessage().getMentionedMembers().size() <= event.getMessage().getContentRaw().split("\\s").length){
            }
            else {
                StringBuilder stringBuilder = new StringBuilder();
                String[] args = event.getMessage().getContentRaw().split("\\s");
                for (int i = event.getMessage().getMentionedMembers().size() + 1; i < args.length; i++){
                    stringBuilder.append(args[i]).append(" ");
                }
                builder.addField("",">>> " + stringBuilder.toString(),false);
            }
            Color purple = new Color(76, 0, 230);
            builder.setColor(purple);
            builder.setImage(pat());
            builder.setFooter(event.getAuthor().getAsTag());
            event.getMessage().reply(builder.build()).queue();
        }
    }

    private String pat() {
        int a = 1;
        int b = 50;
        int diff = b - a;
        Random random = new Random();
        int r = random.nextInt(diff + 1) + a;
        if (r < 10) {
            return "https://cdn.nekos.life/pat/pat_00" + r + ".gif";
        } else {
            return "https://cdn.nekos.life/pat/pat_0" + r + ".gif";
        }
    }
}
