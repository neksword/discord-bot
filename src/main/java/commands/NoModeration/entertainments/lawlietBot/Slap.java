package commands.NoModeration.entertainments.lawlietBot;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;

import java.awt.*;
import java.util.Random;

public class Slap extends Command {

    public Slap()
    {
        this.name = "slap";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setAuthor("\uD83D\uDC4F Ударить");
            if (event.getMessage().getMentionedMembers().isEmpty()) {
                builder.setDescription("**" + event.getAuthor().getName() + "** ударил кого-то!");
            } else {
                if (event.getMessage().getMentionedMembers().size() > 1) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < event.getMessage().getMentionedMembers().size(); i++) {
                        stringBuilder.append("**" + event.getMessage().getMentionedMembers().get(i).getEffectiveName() + "**, ").append(" ");
                    }
                    builder.setDescription(stringBuilder.toString() + "вас ударил **" + event.getMember().getEffectiveName() + "**!");
                } else {
                    builder.setDescription("**" + event.getMessage().getMentionedMembers().get(0).getEffectiveName() + "**, вас ударил **" + event.getMember().getEffectiveName() + "**!");
                }
            }
            Color purple = new Color(76, 0, 230);
            builder.setColor(purple);
            builder.setImage(slap());
            builder.setFooter(event.getAuthor().getAsTag());
            event.getMessage().reply(builder.build()).queue();
        }
    }
    private String slap(){
        int a = 1;
        int b = 50;
        int diff = b - a;
        Random random = new Random();
        int r = random.nextInt(diff + 1) + a;
        if (r < 10) {
            return "https://cdn.nekos.life/slap/slap_00" + r + ".gif";
        } else {
            return "https://cdn.nekos.life/slap/slap_0" + r + ".gif";
        }
    }
}
