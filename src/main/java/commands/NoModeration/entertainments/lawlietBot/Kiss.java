package commands.NoModeration.entertainments.lawlietBot;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;

import java.awt.*;
import java.io.File;
import java.net.MalformedURLException;
import java.util.Random;

public class Kiss extends Command {

    public Kiss()
    {
        this.name = "kiss";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setAuthor("\uD83D\uDC8B Поцеловать");
            if (event.getMessage().getMentionedMembers().isEmpty()){
                builder.setDescription("**" + event.getAuthor().getName() + "** целуется!");
            }
            else {
                if (event.getMessage().getMentionedMembers().size() > 1){
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < event.getMessage().getMentionedMembers().size(); i++){
                        stringBuilder.append("**" + event.getMessage().getMentionedMembers().get(i).getEffectiveName() + "**, ").append(" ");
                    }
                    builder.setDescription(stringBuilder.toString() + "вас поцеловал **" + event.getMember().getEffectiveName() + "**!");
                }
                else {
                    builder.setDescription("**" + event.getMessage().getMentionedMembers().get(0).getEffectiveName() + "**, вас поцеловал **" + event.getMember().getEffectiveName() + "**!");
                }
            }
            Color purple = new Color(76, 0, 230);
            builder.setColor(purple);
            builder.setImage(kiss());
            builder.setFooter(event.getAuthor().getAsTag());
            event.getMessage().reply(builder.build()).queue();
        }
    }
    private String kiss(){
        int a = 1;
        int b = 50;
        int diff = b - a;
        Random random = new Random();
        int r = random.nextInt(diff + 1) + a;
        switch (r){
            case 1:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280406634266659/kiss.gif";
            case 2:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280379862024443/kiss.gif";
            case 3:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280490931257497/kiss.gif";
            case 4:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511898649165824/kiss.gif";
            case 5:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280554512711680/kiss.gif";
            case 6:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511742083268659/kiss.gif";
            case 7:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280338548129841/kiss.gif";
            case 8:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511663347531776/kiss.gif";
            case 9:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511608750669825/kiss.gif";
            case 10:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511608750669825/kiss.gif";
            case 11:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280520312225873/kiss.gif";
            case 12:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511729211080814/kiss.gif";
            case 13:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511535048097812/kiss.gif";
            case 14:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511562890543175/kiss.gif";
            case 15:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511717265309746/kiss.gif";
            case 16:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280534602481714/kiss.gif";
            case 17:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511797158936576/kiss.gif";
            case 18:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511620428005377/kiss.gif";
            case 19:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511396309434368/kiss.gif";
            case 20:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280415978913822/kiss.gif";
            case 21:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511439065645056/kiss.gif";
            case 22:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511768863768596/kiss.gif";
            case 23:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511841174093844/kiss.gif";
            case 24:
                return "https://cdn.discordapp.com/attachments/736280297171058759/768080405668167680/kiss.gif";
            case 25:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511453821468692/kiss.gif";
            case 26:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280426380918955/kiss.gif";
            case 27:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511481617383454/kiss.gif";
            case 28:
                return "https://cdn.discordapp.com/attachments/736280297171058759/736280349406920844/kiss.gif";
            case 29:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511704565481522/kiss.gif";
            case 30:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511693001523228/kiss.gif";
            case 31:
                return "https://cdn.discordapp.com/attachments/736280297171058759/834511579105853560/kiss.gif";
            case 32:
                return "https://cdn.nekos.life/kiss/kiss_001.gif";
            case 33:
                return "https://cdn.nekos.life/kiss/kiss_002.gif";
            case 34:
                return "https://cdn.nekos.life/kiss/kiss_003.gif";
            case 35:
                return "https://cdn.nekos.life/kiss/kiss_004.gif";
            case 36:
                return "https://cdn.nekos.life/kiss/kiss_005.gif";
            case 37:
                return "https://cdn.nekos.life/kiss/kiss_006.gif";
            case 38:
                return "https://cdn.nekos.life/kiss/kiss_007.gif";
            case 39:
                return "https://cdn.nekos.life/kiss/kiss_008.gif";
            case 40:
                return "https://cdn.nekos.life/kiss/kiss_009.gif";
            case 41:
                return "https://cdn.nekos.life/kiss/kiss_010.gif";
            case 42:
                return "https://cdn.nekos.life/kiss/kiss_011.gif";
            case 43:
                return "https://cdn.nekos.life/kiss/kiss_012.gif";
            case 44:
                return "https://cdn.nekos.life/kiss/kiss_013.gif";
            case 45:
                return "https://cdn.nekos.life/kiss/kiss_014.gif";
            case 46:
                return "https://cdn.nekos.life/kiss/kiss_015.gif";
            case 47:
                return "https://cdn.nekos.life/kiss/kiss_016.gif";
            case 48:
                return "https://cdn.nekos.life/kiss/kiss_017.gif";
            case 49:
                return "https://cdn.nekos.life/kiss/kiss_018.gif";
            case 50:
                return "https://cdn.nekos.life/kiss/kiss_019.gif";

        }
        return "АШИБКА";
    }
}
