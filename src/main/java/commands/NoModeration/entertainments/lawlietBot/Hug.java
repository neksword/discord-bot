package commands.NoModeration.entertainments.lawlietBot;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;

import java.awt.*;
import java.util.Random;

public class Hug extends Command {

    public Hug()
    {
        this.name = "hug";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setAuthor("\uD83D\uDC50 Обнять");
            if (event.getMessage().getMentionedMembers().isEmpty()){
                builder.setDescription("**" + event.getAuthor().getName() + "** обнимается!");
            }
            else {
                if (event.getMessage().getMentionedMembers().size() > 1){
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < event.getMessage().getMentionedMembers().size(); i++){
                        stringBuilder.append("**" + event.getMessage().getMentionedMembers().get(i).getEffectiveName() + "**, ").append(" ");
                    }
                    builder.setDescription(stringBuilder.toString() + "вас обнял **" + event.getMember().getEffectiveName() + "**!");
                }
                else {
                    builder.setDescription("**" + event.getMessage().getMentionedMembers().get(0).getEffectiveName() + "**, вас обнял **" + event.getMember().getEffectiveName() + "**!");
                }
            }
            Color purple = new Color(76, 0, 230);
            builder.setColor(purple);
            builder.setImage(hug());
            builder.setFooter(event.getAuthor().getAsTag());
            event.getMessage().reply(builder.build()).queue();
        }
    }
    private String hug(){
        int a = 1;
        int b = 50;
        int diff = b - a;
        Random random = new Random();
        int r = random.nextInt(diff + 1) + a;
        switch (r){
            case 1:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_001.gif";
            case 2:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_002.gif";
            case 3:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_003.gif";
            case 4:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_004.gif";
            case 5:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_005.gif";
            case 6:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_006.gif";
            case 7:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_007.gif";
            case 8:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_008.gif";
            case 9:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_009.gif";
            case 10:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_010.gif";
            case 11:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_011.gif";
            case 12:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_012.gif";
            case 13:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_013.gif";
            case 14:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_014.gif";
            case 15:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_015.gif";
            case 16:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_016.gif";
            case 17:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_017.gif";
            case 18:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_018.gif";
            case 19:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_019.gif";
            case 20:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_020.gif";
            case 21:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_021.gif";
            case 22:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_022.gif";
            case 23:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_023.gif";
            case 24:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_024.gif";
            case 25:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_025.gif";
            case 26:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_026.gif";
            case 27:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_027.gif";
            case 28:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_028.gif";
            case 29:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_029.gif";
            case 30:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_030.gif";
            case 31:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_031.gif";
            case 32:
                System.out.println(r);
                return "https://cdn.nekos.life/hug/hug_032.gif";
        }
        return "АШИБКА";
    }
}
