package commands.NoModeration.entertainments;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

public class Coin extends Command {

    public Coin()
    {
        this.name = "coin";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            TextChannel channel = event.getTextChannel();
            int a = 1;
            int b = 10;
            int x = a + (int) (Math.random() * b);

            if (x == 1 || x == 2 || x == 3 || x == 4) {
                channel.sendMessage("\uD83C\uDF15 Орёл!").queue();
            } else if (x == 5 || x == 6 || x == 7 || x == 8) {
                channel.sendMessage("\uD83C\uDF11 Решка!").queue();
            } else if (x == 9 || x == 10) {
                channel.sendMessage("\uD83C\uDF13 Монета упала ребром!").queue();
            }
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Coin";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
