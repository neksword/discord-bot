package commands.NoModeration.entertainments;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.Economy.DBNewUser;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Cookies extends Command {
    public final MongoCollection<Document> collection;
    public Cookies(MongoCollection<Document> collection) {
        this.name = "cookies";
        this.guildOnly = true;
        this.cooldown = 1000;
        this.collection = collection;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (new UsedChannel().Return(event.getMember(), event.getTextChannel(), event.getMessage())){
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            String[] command = event.getMessage().getContentRaw().split("\\s");
            if (command.length  >= 2) {
                List<Member> members = event.getMessage().getMentionedMembers();
                if (members == null){
                    event.replyError("**Укажите пользователя!**");
                }
                else {
                    if (event.getMember().getId().equals(members.get(0).getId())) {
                        event.replyError("**Нельзя выдать печеньку себе!**");
                    } else {
                        if (members.get(0).getUser().isBot()) {
                            return;
                        }
                        long id1 = Long.parseLong(event.getMember().getId());
                        Document document1 = collection.find(new Document("id",id1)).first();
                        if (document1 == null){
                            DBNewUser dbNewUser = new DBNewUser();
                            dbNewUser.NewUser(collection,event.getMember());
                        }
                        long id = Long.parseLong(members.get(0).getId());
                        Document document = collection.find(new Document("id",id)).first();
                        if (document == null){
                            DBNewUser dbNewUser = new DBNewUser();
                            dbNewUser.NewMentionUser(collection,members);
                        }
                        collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("cookies", Integer.parseInt(String.valueOf(document.get("cookies"))) + 1)));
                        event.replySuccess("**Вы выписали печеньку\uD83C\uDF6A - **" + members.get(0).getAsMention());

                    }
                }
            }
            else {
                event.replyError("**Укажите пользователя!**");
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Cookies";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
