package commands.ModerationCommand.moderation;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.ModerationCommand.CheckingChannelsMod.UsedChannelMod;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import org.aeonbits.owner.ConfigFactory;

import java.util.Arrays;
import java.util.List;

public class GiveRole extends Command {

    public GiveRole()
    {
        this.name = "giverole";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannelMod usedChannel = new UsedChannelMod();
        if (usedChannel.ReturnMod(event.getMember(), event.getTextChannel(), event.getMessage())) {
            Message message = event.getMessage();
            Member member = event.getMember();
            List<String> args = Arrays.asList(event.getArgs().split("\\s"));
            List<Member> mentionedmember = message.getMentionedMembers();
            if (member.hasPermission(Permission.MANAGE_ROLES)) {
                List<Member> members = message.getMentionedMembers();
                List<Role> roles = message.getMentionedRoles();
                if (members.isEmpty() || roles.isEmpty()) {
                    event.getChannel().sendMessage("Укажите пользователя и роль!").queue();
                }

                Member target = members.get(0);
                Role targetRole = roles.get(0);
                event.getGuild().addRoleToMember(target, targetRole).queue();
                event.getChannel().sendMessage(targetRole.getAsMention() + " выдана: " + target.getAsMention()).queue();
            } else {
                event.getChannel().sendMessage(member.getAsMention() + " у вас нет прав!").queue();
            }
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "GiveRole";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
