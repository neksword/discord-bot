package commands.ModerationCommand.moderation;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Clear extends Command {

    public Clear()
    {
        this.name = "clear";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        Message message1 = event.getMessage();
        TextChannel channel = event.getTextChannel();
        Member member = event.getMember();
        String[] args = event.getMessage().getContentRaw().split("\\s");
        if (member.getUser().isBot()) {
            return;
        } else {
            if (member.hasPermission(Permission.MESSAGE_MANAGE)) {
                if (args.length >= 2) {
                    EmbedBuilder message = new EmbedBuilder();
                    message.setDescription("Сообщения удалены!");
                    message.setColor(Color.cyan);
                    OffsetDateTime earlist = message1.getTimeCreated().minusHours(335);
                    try {
                        if (Integer.parseInt(args[1]) < 100) {
                            List<Message> messages = channel.getHistory().retrievePast(Integer.parseInt(args[1])).complete();
                            if (messages.get(messages.size() - 1).getTimeCreated().isBefore(earlist)) {
                                EmbedBuilder builder = new EmbedBuilder();
                                builder.setColor(Color.RED);
                                builder.setDescription("Я не могу удалить сообщение давностью больше двух недель!");
                                channel.sendMessage(builder.build()).queue();
                                builder.clear();
                            } else {
                                channel.deleteMessages(messages).queue();
                                channel.sendMessage(message.build()).queue(m -> m.delete().queueAfter(5, TimeUnit.SECONDS));
                                message.clear();
                            }
                        } else {
                            EmbedBuilder builder = new EmbedBuilder();
                            builder.setColor(Color.RED);
                            builder.setDescription("Я не могу удалить больше 100 сообщений!");
                            channel.sendMessage(builder.build()).queue();
                            builder.clear();
                        }
                    } catch (NumberFormatException numberFormatException) {
                        EmbedBuilder builder = new EmbedBuilder();
                        builder.setColor(Color.RED);
                        builder.setDescription("Использование команды: " + "^clear (число сообщений)");
                        channel.sendMessage(builder.build()).queue();
                        builder.clear();
                    }
                } else {
                    EmbedBuilder notnumber = new EmbedBuilder();
                    notnumber.setDescription("Укажите число сообщений!");
                    notnumber.setColor(Color.cyan);
                    channel.sendMessage(notnumber.build()).queue();
                    notnumber.clear();
                }
            } else {
                EmbedBuilder notpermission = new EmbedBuilder();
                notpermission.setTitle("У вас нет прав!");
                channel.sendMessage(notpermission.build()).queue();
                notpermission.clear();
            }
        }
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
        LogCommand logCommand = new LogCommand();
        String commandname = "Clear";
        logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
    }
}
