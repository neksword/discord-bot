package commands.ModerationCommand.moderation;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.ModerationCommand.CheckingChannelsMod.UsedChannelMod;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class Kick extends Command {

    public Kick()
    {
        this.name = "kick";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannelMod usedChannel = new UsedChannelMod();
        if (usedChannel.ReturnMod(event.getMember(), event.getTextChannel(), event.getMessage())) {
            Message message = event.getMessage();
            Member member = event.getMember();
            List<String> args = Arrays.asList(event.getArgs().split("\\s"));
            List<Member> mentionedmember = message.getMentionedMembers();
            if (!mentionedmember.isEmpty() || !(args.size() < 2)) {
                Member target = mentionedmember.get(0);
                if (member.hasPermission(Permission.KICK_MEMBERS) && member.canInteract(target)) {

                    String reason = String.join("", args.subList(1, args.size()));
                    target.kick()
                            .reason(String.format("Kick by: %#s, with reason: %s", member, reason)).queue();
                    EmbedBuilder kick = new EmbedBuilder();
                    kick.setDescription(target.getEffectiveName() + " выгнан!");
                    kick.setColor(Color.RED);
                    event.reply(kick.build());
                    kick.clear();

                } else {
                    EmbedBuilder norPermission = new EmbedBuilder();
                    norPermission.setDescription("У вас недостаточно прав!");
                    norPermission.setColor(Color.RED);
                    event.reply(norPermission.build());
                    norPermission.clear();
                    return;
                }
            } else {
                EmbedBuilder notMentioned = new EmbedBuilder();
                notMentioned.setDescription("Укажите участника!");
                notMentioned.setColor(Color.RED);
                event.reply(notMentioned.build());
                notMentioned.clear();
                return;
            }
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Kick";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}


