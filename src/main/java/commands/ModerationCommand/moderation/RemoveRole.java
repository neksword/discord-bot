package commands.ModerationCommand.moderation;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.ModerationCommand.CheckingChannelsMod.UsedChannelMod;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class RemoveRole extends Command {
    public RemoveRole()
    {
        this.name = "removerole";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannelMod usedChannel = new UsedChannelMod();
        if (usedChannel.ReturnMod(event.getMember(), event.getTextChannel(), event.getMessage())) {
        Member member = event.getMember();
        Message message = event.getMessage();
        Guild guild = event.getGuild();
        TextChannel channel = event.getTextChannel();
        if (member.hasPermission(Permission.MANAGE_ROLES)) {
            List<Member> members = message.getMentionedMembers();
            List<Role> roles = message.getMentionedRoles();
            if (members.isEmpty() || roles.isEmpty()) {
                channel.sendMessage("Укажите пользователя и роль!").queue();
            }
            Member member1 = members.get(0);
            Role role = roles.get(0);
            guild.removeRoleFromMember(member1, role).queue();
            channel.sendMessage(role.getAsMention() + " удалена у: " + member1.getAsMention()).queue();
        } else {
            channel.sendMessage(member.getAsMention() + " у вас нет прав!").queue();
        }
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Removerole";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
    }
    }
}
