package commands.ModerationCommand.moderation.JavaTest;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.utils.AttachmentOption;
import org.aeonbits.owner.ConfigFactory;

import java.io.*;

public class TestJava extends ListenerAdapter {
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        if (command[0].equalsIgnoreCase(cfg.prefix() + "java")) {
            if (event.getMember().getId().equals("387521150281973760")) {
                try {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 1; i < command.length; i++) {
                        builder.append(command[i]).append(" ");
                    }
                    File file = new File("/home/" + cfg.homedirectory() + "/untitled3/src/main/java/Main.java");
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
                    int start = 8;
                    int end = builder.toString().length() - 4;
                    char[] dst = new char[end - start];
                    builder.toString().getChars(start, end, dst, 0);
                    bufferedWriter.write(dst);
                    bufferedWriter.close();
                    System.out.println(dst);
                    ProcessBuilder processBuilder = new ProcessBuilder("mvn", "package");
                    processBuilder.redirectErrorStream(true);
                    Process process = processBuilder.directory(new File("/home/" + cfg.homedirectory() + "/untitled3")).start();
                    InputStream stdout = process.getInputStream();
                    InputStreamReader isrStdout = new InputStreamReader(stdout);
                    BufferedReader brStdout = new BufferedReader(isrStdout);

                    String line = null;
                    StringBuilder stringBuilder1 = new StringBuilder();
                    while ((line = brStdout.readLine()) != null) {
                        stringBuilder1.append(line).append("\n");
                    }
                    System.out.println(stringBuilder1.toString());
                    File file1 = new File("info.txt");
                    BufferedWriter bufferedWriter1 = new BufferedWriter(new FileWriter(file1));
                    bufferedWriter1.write(stringBuilder1.toString());
                    bufferedWriter1.close();
                    event.getChannel().sendFile(file1, AttachmentOption.SPOILER).queue();
                    ProcessBuilder procBuilder1 = new ProcessBuilder("java", "-jar", "/home/" + cfg.homedirectory() + "/untitled3/target/JavaBot.jar");
                    procBuilder1.redirectErrorStream(true);
                    Process process1 = procBuilder1.start();
                    InputStream stdout1 = process1.getInputStream();
                    InputStreamReader isrStdout1 = new InputStreamReader(stdout1);
                    BufferedReader brStdout1 = new BufferedReader(isrStdout1);

                    String line1 = null;
                    while ((line1 = brStdout1.readLine()) != null) {
                        event.getChannel().sendMessage(line1).queue();
                    }
                    return;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
