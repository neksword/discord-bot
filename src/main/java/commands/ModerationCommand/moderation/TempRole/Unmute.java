package commands.ModerationCommand.moderation.TempRole;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.ModerationCommand.CheckingChannelsMod.UsedChannelMod;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.io.File;
import java.util.List;

public class Unmute extends Command {
    public Unmute(){
        this.name = "unmute";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        final UsedChannelMod usedChannel = new UsedChannelMod();
        if (usedChannel.ReturnMod(event.getMember(), event.getTextChannel(), event.getMessage())) {
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            List<Member> memberList = event.getMessage().getMentionedMembers();
            if (memberList.isEmpty()) {
                event.getMessage().reply("Укажите пользователя!").queue();
                System.out.println(event.getGuild());
            } else {
                if (new File("/home/" + cfg.homedirectory() + "/TempRoleInfo/" + memberList.get(0).getId() + ".json").exists()) {
                    File file = new File("/home/" + cfg.homedirectory() + "/TempRoleInfo/" + memberList.get(0).getId() + ".json");
                    file.delete();
                    Role role = event.getGuild().getRoleById("807832552719122442");
                    event.getGuild().removeRoleFromMember(memberList.get(0), role).queue();
                    event.getMessage().reply("Участник размучен.").queue();
                } else {
                    event.getMessage().reply("Участник не был замучен.").queue();
                }
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Unmute";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
