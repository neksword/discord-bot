package commands.ModerationCommand.moderation.TempRole;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.ModerationCommand.CheckingChannelsMod.UsedChannelMod;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TempRole extends Command {
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(4);
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public TempRole()
    {
        this.name = "mute";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getMessage().getContentRaw().split("\\s");
        final UsedChannelMod usedChannel = new UsedChannelMod();
        if (usedChannel.ReturnMod(event.getMember(), event.getTextChannel(), event.getMessage())) {
            List<Role> roles = event.getMember().getRoles();
            Optional<Role> roleContainer = roles.stream().filter(role -> role.getId().equals("837336373044183106")).findAny();
            if (roleContainer.isPresent()) {
                if (args.length < 3) {
                    event.getMessage().reply("**Использование команды - " + cfg.prefix() + "mute @упоминание (время)h.**").queue();
                } else {
                    List<Member> members = event.getMessage().getMentionedMembers();
                    int testint = args[2].length();
                    char[] dst = new char[(testint - 1)];
                    args[2].getChars(0, testint - 1, dst, 0);
                    char timeunit = args[2].charAt(testint - 1);
                    System.out.println("TimeUnit: " + timeunit);
                    if (!members.isEmpty()) {
                        if (members.size() == 1) {
                                if (("h").equals(String.valueOf(timeunit))) {
                                    if (new File("/home/" + cfg.homedirectory() + "/TempRoleInfo/" + members.get(0).getId() + ".json").exists()){
                                        int time = Integer.parseInt(String.valueOf(dst));
                                        ObjectMapper mapper = new ObjectMapper();
                                        File file = new File("/home/" + cfg.homedirectory() + "/TempRoleInfo/" + members.get(0).getId() + ".json");
                                        try {
                                            TempRoleInfo tempRoleInfo = mapper.readValue(file,TempRoleInfo.class);
                                            tempRoleInfo.setTime(tempRoleInfo.getTime() + (time*60));
                                            mapper.writeValue(file,tempRoleInfo);
                                            Runnable runnable = () -> {
                                                if (file.exists()) {
                                                    Role role = event.getGuild().getRoleById("807832552719122442");
                                                    event.getGuild().removeRoleFromMember(members.get(0), role).queue();
                                                    System.out.println("Убрано у: " + members.get(0).getId() + " | " + members.get(0).getUser().getName());
                                                    file.delete();
                                                }
                                            };
                                            ScheduledFuture<?> roleDelete = scheduler.schedule(runnable, tempRoleInfo.getTime() + (time*60), TimeUnit.MINUTES);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else {
                                        int time = Integer.parseInt(String.valueOf(dst));
                                        Role role = event.getGuild().getRoleById("807832552719122442");
                                        event.getGuild().addRoleToMember(members.get(0), role).queue();
                                        Role role1 = event.getGuild().getRoleById("807879328704364554");
                                        event.getGuild().removeRoleFromMember(members.get(0), role1).queue();
                                        ObjectMapper mapper = new ObjectMapper();
                                        File file = new File("/home/" + cfg.homedirectory() + "/TempRoleInfo/" + members.get(0).getId() + ".json");
                                        Runnable runnable = () -> {
                                            try {
                                                TempRoleInfo tempRoleInfo = mapper.readValue(file, TempRoleInfo.class);
                                                if (file.exists()) {
                                                    if (tempRoleInfo.getTime() <= (time * 60)) {
                                                        event.getGuild().removeRoleFromMember(members.get(0), role).queue();
                                                        System.out.println("Убрано у: " + members.get(0).getId() + " | " + members.get(0).getUser().getName());
                                                        file.delete();
                                                    }
                                                }
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        };
                                        ScheduledFuture<?> roleDelete = scheduler.schedule(runnable, time, TimeUnit.HOURS);
                                        System.out.println("Время: " + roleDelete.getDelay(TimeUnit.MINUTES));
                                        final TempRoleInfo tempRoleInfo = new TempRoleInfo(members.get(0), roleDelete.getDelay(TimeUnit.MINUTES));
                                        try {
                                            mapper.writeValue(file, tempRoleInfo);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("Выдано: " + members.get(0).getId() + " | " + members.get(0).getUser().getName());
                                        event.getMessage().reply("**Участник** " + members.get(0).getAsMention() + " **посажен на** `" + time + "` **часа(-ов)!**").queue();
                                    }
                                } else {
                                    event.getMessage().reply("**Использование команды - " + cfg.prefix() + "mute @упоминание (время)h.**").queue();
                                }
                        } else {
                            event.getMessage().reply("**Указать можно только одного участника!**").queue();
                        }
                    } else {
                        event.getMessage().reply("**Укажите пользователя!**").queue();
                    }
                }
            }
            else {
                event.getMessage().reply("**У вас нет прав на использование этой команды!**").queue();
            }
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Mute";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }

}
