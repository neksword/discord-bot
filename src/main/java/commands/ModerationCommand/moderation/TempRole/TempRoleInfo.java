package commands.ModerationCommand.moderation.TempRole;

import net.dv8tion.jda.api.entities.Member;

public class TempRoleInfo {
    private String userID;
    private long time;

    public TempRoleInfo() {

    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public TempRoleInfo(final Member member, long delay) {
        this.userID = member.getId();
        this.time = delay;
    }

    public String getUserID() {
        return this.userID;
    }

    public long getTime() {
        return this.time;
    }
}

