package commands.ModerationCommand.moderation.TempRole;

import BotConfig.BotConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.guild.GuildReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class FileUpdate extends ListenerAdapter {
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(4);
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public void onGuildReady(GuildReadyEvent event) {
        if (event.getGuild().getId().equals("807690589322608690")) {
            ObjectMapper mapper = new ObjectMapper();
            Path dir = Paths.get("/home/" + cfg.homedirectory() + "/TempRoleInfo");
            Runnable runnableupdate = () -> {
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
                    for (Path file : stream) {
                        System.out.println("Файл: " + file.getFileName());
                        final TempRoleInfo tempRoleInfo = mapper.readValue(file.toFile(), TempRoleInfo.class);
                        if (tempRoleInfo.getTime() <= 10){

                        }
                        else {
                            tempRoleInfo.setTime(tempRoleInfo.getTime() - 10);
                            mapper.writeValue(file.toFile(), tempRoleInfo);
                        }
                    }
                } catch (IOException | DirectoryIteratorException x) {
                    System.err.println(x);
                }
            };
            ScheduledFuture<?> update = scheduler.scheduleAtFixedRate(runnableupdate, 1, 10, TimeUnit.MINUTES);
        }
        if (event.getGuild().getId().equals("807690589322608690")) {
            ObjectMapper mapper = new ObjectMapper();
            Path dir = Paths.get("/home/" + cfg.homedirectory() + "/TempRoleInfo");
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
                for (Path file : stream) {
                    System.out.println("Файл: " + file.getFileName());
                    try {
                        final TempRoleInfo tempRoleInfo = mapper.readValue(file.toFile(), TempRoleInfo.class);
                        String filename = file.getFileName().toString();
                        char[] dst = new char[(filename.length() - 5)];
                        filename.getChars(0, filename.length() - 5, dst, 0);
                        System.out.println("ID: " + String.valueOf(dst));
                        Member member = event.getGuild().getMemberById(String.valueOf(dst));
                        System.out.println("Загружен: " + member.getUser().getId() + " | " + member.getUser().getName());
                        Role role = event.getGuild().getRoleById("807832552719122442");
                        Runnable runnable = () -> {
                            try {
                                TempRoleInfo tempRoleInfo1 = mapper.readValue(file.toFile(), TempRoleInfo.class);
                                if (file.toFile().exists()) {
                                    if (tempRoleInfo.getTime() <= tempRoleInfo1.getTime()) {
                                        event.getGuild().removeRoleFromMember(member, role).queue();
                                        System.out.println("Убрано у: " + member.getId() + " | " + member.getUser().getName());
                                        file.toFile().delete();
                                    }
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        };
                        ScheduledFuture<?> roleDelete = scheduler.schedule(runnable, tempRoleInfo.getTime(), TimeUnit.MINUTES);
                        System.out.println("Время: " + roleDelete.getDelay(TimeUnit.MINUTES));
                        try {
                            mapper.writeValue(file.toFile(),tempRoleInfo);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException | DirectoryIteratorException x) {
                System.err.println(x);
            }
        }
    }
}
