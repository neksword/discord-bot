package commands.ModerationCommand.moderation;

import Audit.LogCommand;
import BotConfig.BotConfig;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.ModerationCommand.CheckingChannelsMod.UsedChannelMod;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.util.List;

public class NicknameModify extends Command {

    public NicknameModify()
    {
        this.name = "nick";
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannelMod usedChannel = new UsedChannelMod();
        if (usedChannel.ReturnMod(event.getMember(), event.getTextChannel(), event.getMessage())) {
            Message message = event.getMessage();
            Member member = event.getMember();
            String[] args = event.getArgs().split("\\s");
            String nonick;
            if (member.hasPermission(Permission.NICKNAME_CHANGE, Permission.NICKNAME_MANAGE)) {
                List<Member> mentionedmember = message.getMentionedMembers();
                if (mentionedmember.isEmpty()) {
                    EmbedBuilder nomember = new EmbedBuilder();
                    nomember.setTitle("Укажите пользователя!");
                    nomember.setColor(Color.red);

                    event.getChannel().sendMessage(nomember.build()).queue();
                    nomember.clear();
                } else {
                    Member target = mentionedmember.get(0);
                    EmbedBuilder modify = new EmbedBuilder();
                    modify.setColor(Color.green);
                    if (target.getNickname() == null) {
                        nonick = "Пустой ник";
                    } else {
                        nonick = target.getNickname();
                    }
                    target.modifyNickname(args[2]).queue();
                    modify.addField("Ник пользователя: " + target.getUser().getAsTag() + " изменен!", "Старый ник: " + nonick + "\n" + "Новый ник: " + args[2], false);

                    event.getChannel().sendMessage(modify.build()).queue();
                    modify.clear();
                }
            } else {
                EmbedBuilder nopermission = new EmbedBuilder();
                nopermission.setTitle("У вас недостаточно прав!");
                nopermission.setColor(Color.RED);

                event.getChannel().sendMessage(nopermission.build()).queue();
                nopermission.clear();
            }
        }
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
        LogCommand logCommand = new LogCommand();
        String commandname = "Nick";
        logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
    }
}
