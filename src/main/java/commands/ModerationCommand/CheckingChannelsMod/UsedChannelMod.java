package commands.ModerationCommand.CheckingChannelsMod;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

public class UsedChannelMod {
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public boolean BotMod(final Member member){
        if (member.getUser().isBot()) {
            return true;
        } else {
            return false;
        }
    }
    public boolean ChannelMod(final Member member,final TextChannel textChannel){
        if (textChannel.getId().equals(cfg.channelused()) || textChannel.getId().equals(cfg.musicchannel()) || textChannel.getId().equals(cfg.channelban()) || member.getId().equals(cfg.twinklewineid())){
            return true;
        }
        else {
            return false;
        }
    }
    public boolean ReturnMod(final Member member, final TextChannel textChannel, final Message message){
        if (BotMod(member)){
            return false;
        }
        else if (ChannelMod(member, textChannel)){
            return true;
        }
        else if (!ChannelMod(member, textChannel)){
            InfoMod(textChannel, message);
            return false;
        }
        return false;
    }
    public void InfoMod(final TextChannel textChannel, final Message message){
        TextChannel Channel = textChannel.getGuild().getTextChannelById(cfg.channelban());
        message.reply("**Команды для модерирования только в ** " + Channel.getAsMention()).queue();

    }
}
