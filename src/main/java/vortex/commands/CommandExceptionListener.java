package vortex.commands;

public class CommandExceptionListener extends RuntimeException {
    public static class CommandErrorException extends RuntimeException
    {
        public CommandErrorException(String message)
        {
            super(message);
        }
    }
}
