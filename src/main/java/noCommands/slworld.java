package noCommands;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

public class slworld extends ListenerAdapter {
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        if (event.getChannel().getId().equals("843006060641779764")){
            if (event.getMessageId().equals("843415035430567956")){
                if (event.getReaction().getReactionEmote().getName().equals("✅")){
                    TextChannel channel = event.getGuild().getTextChannelById("842659647164645386");
                    channel.sendMessage(event.getMember().getAsMention() + " хочет присоедениться к серверу!").queue();
                }
            }
        }
    }
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        if (command[0].equalsIgnoreCase(cfg.prefix() + "startrole")) {
            String id = "387521150281973760";
            String memberid = event.getMember().getId();
            if (id.equals(memberid)) {
                TextChannel channel = event.getGuild().getTextChannelById("843006060641779764");
                Message rolemessage = channel.retrieveMessageById("843415035430567956").complete();
                rolemessage.addReaction("✅").queue();
            }
        }
    }
}
