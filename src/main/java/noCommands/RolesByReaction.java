package noCommands;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;


public class RolesByReaction extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        if (command[0].equalsIgnoreCase(cfg.prefix() + "startrole")){
            String id = "387521150281973760";
            String memberid = event.getMember().getId();
            if (id.equals(memberid)){
                TextChannel channel = event.getGuild().getTextChannelById("807807900411232257");
                Message rolemessage = channel.retrieveMessageById("833684909247299624").complete();
                rolemessage.addReaction("\uD83D\uDD2A").queue();
                rolemessage.addReaction("\uD83E\uDDD9\u200D♂️").queue();
                rolemessage.addReaction("\uD83D\uDC73\u200D♂️").queue();
                rolemessage.addReaction("\uD83E\uDDDF\u200D♂️").queue();
                rolemessage.addReaction("⛏").queue();
                rolemessage.addReaction("\uD83D\uDC31").queue();
                rolemessage.addReaction("\uD83C\uDFF4\u200D☠️").queue();
                rolemessage.addReaction("\uD83D\uDC30").queue();
                rolemessage.addReaction("⚒").queue();
                rolemessage.addReaction("\uD83D\uDC7B").queue();
                rolemessage.addReaction("\uD83E\uDDDD️️️").queue();
                event.getMessage().reply("Реакции поставлены!").queue();
            }
            else {
                event.getMessage().reply("**Только создатель может использовать эту команду!** ").queue();
            }
        }
        if (command[0].equalsIgnoreCase(cfg.prefix() + "startrolelol")){
            String id = "387521150281973760";
            String memberid = event.getMember().getId();
            if (id.equals(memberid)){
                TextChannel channel = event.getGuild().getTextChannelById("807807900411232257");
                Message rolemessage = channel.retrieveMessageById("833684909247299624").complete();
                rolemessage.addReaction("\uD83E\uDDDD").queue();
                event.getMessage().reply("Реакции поставлены!").queue();
            }
            else {
                event.getMessage().reply("**Только создатель может использовать эту команду!** ").queue();
            }
        }
    }

    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        TextChannel channel = event.getGuild().getTextChannelById("821571264710836244");
        if (!(event.getUser().isBot())) {
            String message = event.getMessageId();
            MessageReaction eventReaction = event.getReaction();
            if (message.equals("833684909247299624")) {
                //1 among
                if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDD2A")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807792764527706162");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //2 dota
                else if (eventReaction.getReactionEmote().getName().equals("\uD83E\uDDD9\u200D♂️")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807792766230331432");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //3 csgo
                else if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDC73\u200D♂️")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807793632136396800");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //4 left4dead
                else if (eventReaction.getReactionEmote().getName().equals("\uD83E\uDDDF\u200D♂️")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807794800815898646");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //5 minecraft
                else if (eventReaction.getReactionEmote().getName().equals("⛏")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807794579541327933");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //6 super animal royale
                else if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDC31")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("808444344020631612");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //7 apex
                else if (eventReaction.getReactionEmote().getName().equals("\uD83C\uDFF4\u200D☠️")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("809847600239870023");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //8 deceit
                else if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDC30")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("808902512701734912");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //9 rust
                else if (eventReaction.getReactionEmote().getName().equals("⚒")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807793961230008340");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //10 fall
                else if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDC7B")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807939945292759090");
                    event.getGuild().addRoleToMember(member, role).queue();
                }
                //11 LoL
//                else if (eventReaction.getReactionEmote().getName().equals("\uD83E\uDDDD️")) {
//                    channel.sendMessage("контакт").queue();
//                    Member member = event.getMember();
//                    Role role = event.getGuild().getRoleById("824753227895144519");
//                    event.getGuild().addRoleToMember(member, role).queue();
//                }
            }
        }
    }


    public void onGuildMessageReactionRemove(GuildMessageReactionRemoveEvent event) {
        String s = event.getReaction().getReactionEmote().getAsReactionCode();
        System.out.println(s);
        if (!(event.getUser().isBot())) {
            String message = event.getMessageId();
            MessageReaction eventReaction = event.getReaction();
            if (message.equals("833684909247299624")) {
                //1 among
                if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDD2A")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807792764527706162");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //2 dota
                else if (eventReaction.getReactionEmote().getName().equals("\uD83E\uDDD9\u200D♂️")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807792766230331432");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //3 csgo
                else if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDC73\u200D♂️")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807793632136396800");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //4 left4dead
                else if (eventReaction.getReactionEmote().getName().equals("\uD83E\uDDDF\u200D♂️")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807794800815898646");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //5 minecraft
                else if (eventReaction.getReactionEmote().getName().equals("⛏")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807794579541327933");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //6 super animal royale
                else if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDC31")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("808444344020631612");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //7 apex
                else if (eventReaction.getReactionEmote().getName().equals("\uD83C\uDFF4\u200D☠️")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("809847600239870023");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //8 deceit
                else if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDC30")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("808902512701734912");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //9 rust
                else if (eventReaction.getReactionEmote().getName().equals("⚒")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807793961230008340");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //10 fall
                else if (eventReaction.getReactionEmote().getName().equals("\uD83D\uDC7B")) {
                    Member member = event.getMember();
                    Role role = event.getGuild().getRoleById("807939945292759090");
                    event.getGuild().removeRoleFromMember(member, role).queue();
                }
                //11 LoL
//                else if (eventReaction.getReactionEmote().getName().equals("\uD83E\uDDDD")) {
//                    Member member = event.getMember();
//                    Role role = event.getGuild().getRoleById("824753227895144519");
//                    event.getGuild().removeRoleFromMember(member, role).queue();
//                }
            }
        }
    }
}
