package noCommands;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.guild.member.GenericGuildMemberEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleRemoveEvent;
import net.dv8tion.jda.api.events.guild.member.update.GenericGuildMemberUpdateEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;

public class ChangeRole extends ListenerAdapter {

    public void onGuildMemberRoleRemove(GuildMemberRoleRemoveEvent event) {

        Member member = event.getMember();
        List<Role> removedRoles = event.getRoles();
        Optional<Role> roleContainer = removedRoles.stream().filter(role -> role.getName().equals("\uD83D\uDC6E\u200D♂️Косячник\uD83D\uDC6E\u200D♂️")).findAny();
        if (roleContainer.isPresent()) {
            System.out.println("removed role "  + roleContainer.get());
            Role role1 = event.getGuild().getRoleById("807879328704364554");
            event.getGuild().addRoleToMember(member, role1).queue();
           TextChannel channel = event.getGuild().getTextChannelById("810561932951945246");
           channel.sendMessage("Роль участника выдана: " + member.getAsMention()).queue();

        }
    }
}

//    @Override
//    public void onGenericEvent(@NotNull GenericEvent event) {
//        System.out.println(event);
//        super.onGenericEvent(event);
//    }
//}

