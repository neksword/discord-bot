package noCommands;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

public class Verifyreact extends ListenerAdapter {
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        if (command[0].equalsIgnoreCase(cfg.prefix() + "startrole")) {
            String id = "387521150281973760";
            String memberid = event.getMember().getId();
            if (id.equals(memberid)) {
                TextChannel channel = event.getGuild().getTextChannelById("826152964619763762");
                Message rolemessage = channel.retrieveMessageById("826158732479954965").complete();
                rolemessage.addReaction("✅").queue();
            }
        }
    }

    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        if (event.getMessageId().equals("826158732479954965")){
            if (event.getReaction().getReactionEmote().getName().equals("✅")){
                Role role = event.getGuild().getRoleById("807879328704364554");
                event.getGuild().addRoleToMember(event.getMember(),role).queue();
            }
        }
    }
}
