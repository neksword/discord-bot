package noCommands;

import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.guild.GuildReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ServerStats extends ListenerAdapter {
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(4);
    public void onGuildReady(GuildReadyEvent event) {
        Runnable runnable = () -> {
            List<User> users = event.getGuild().getMembers().stream().map(Member::getUser)
                    .filter(user -> !user.isBot()).collect(Collectors.toList());
            VoiceChannel channel = event.getGuild().getVoiceChannelById("808986745613320202");
            channel.getManager().setName("Users: " + users.size()).complete();
            List<Member> idle = event.getGuild().getMembers().stream().filter(member -> member.getOnlineStatus().equals(OnlineStatus.IDLE)).collect(Collectors.toList());
            List<Member> dnd = event.getGuild().getMembers().stream().filter(member -> member.getOnlineStatus().equals(OnlineStatus.DO_NOT_DISTURB)).collect(Collectors.toList());
            List<Member> online = event.getGuild().getMembers().stream().filter(member -> member.getOnlineStatus().equals(OnlineStatus.ONLINE)).collect(Collectors.toList());
            VoiceChannel voiceChannel = event.getGuild().getVoiceChannelById("808986741352562688");
            voiceChannel.getManager().setName("\uD83D\uDFE2" + online.size() + " ⛔" + dnd.size() + " \uD83C\uDF19" + idle.size()).complete();
        };
        ScheduledFuture<?> scheduledFuture = scheduler.scheduleWithFixedDelay(runnable,1,30, TimeUnit.MINUTES);
    }
}
