package UserRankSystem;

import BotConfig.BotConfig;
import UserRankSystem.Economy.DBNewUser;
import UserRankSystem.Economy.LevelRole;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.io.File;
import java.io.IOException;

public class MessageRank extends ListenerAdapter {
    private final MongoCollection<Document> collection;
    BotConfig cfg = ConfigFactory.create(BotConfig.class);

    public MessageRank(MongoCollection<Document> mongoCollection) {
        this.collection = mongoCollection;
    }

    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        if (event.getUser().isBot()) {
            return;
        }
        long id = Long.parseLong(event.getMember().getId());
        Document document = collection.find(new Document("id", id)).first();
        if (document == null) {
            DBNewUser dbNewUser = new DBNewUser();
            dbNewUser.NewUser(collection, event.getMember());
        }
    }

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        if (event.getAuthor().isBot()) {
            return;
        }
        long id = Long.parseLong(event.getMember().getId());
        Document document = collection.find(new Document("id", id)).first();
        if (document == null) {
            DBNewUser dbNewUser = new DBNewUser();
            dbNewUser.NewUser(collection, event.getMember());
        }
        collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("newRank", Integer.parseInt(String.valueOf(document.get("newRank"))) - 1)));
        collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("message", Integer.parseInt(String.valueOf(document.get("message"))) + 1)));
        if ((Integer.parseInt(String.valueOf(document.get("message"))) + Integer.parseInt(String.valueOf(document.get("minvoice")))) >= Integer.parseInt(String.valueOf(document.get("newRank")))) {
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("lvl", Integer.parseInt(String.valueOf(document.get("lvl"))) + 1)));
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("newRank",(int) (Integer.parseInt(String.valueOf(document.get("message"))) + Integer.parseInt(String.valueOf(document.get("minvoice"))) + ((Integer.parseInt(String.valueOf(document.get("message"))) + Integer.parseInt(String.valueOf(document.get("minvoice")))) * 2)))));
            final LevelRole levelRole = new LevelRole();
            levelRole.LevelRole(Integer.parseInt(String.valueOf(document.get("lvl"))), event.getMember(), event.getGuild());
        }
    }
}
