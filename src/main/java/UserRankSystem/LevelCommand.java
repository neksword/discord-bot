package UserRankSystem;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.Economy.DBNewUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class LevelCommand extends Command {
    public final MongoClient mongoClient;
    public final MongoDatabase database;
    public final MongoCollection<Document> collection;
    public LevelCommand(MongoClient mongoClient,MongoDatabase database,MongoCollection<Document> collection) {
        this.name = "level";
        this.guildOnly = true;
        this.mongoClient = mongoClient;
        this.collection = collection;
        this.database = database;
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        String[] args = event.getMessage().getContentRaw().split("\\s");
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            List<Member> members = event.getMessage().getMentionedMembers();
            if (members.isEmpty()) {
                long id = Long.parseLong(event.getMember().getId());
                Document document = collection.find(new Document("id",id)).first();
                if (document == null){
                    DBNewUser dbNewUser = new DBNewUser();
                    dbNewUser.NewUser(collection,event.getMember());
                }
                    EmbedBuilder embedBuilder = new EmbedBuilder();
                    embedBuilder.setAuthor(event.getMember().getEffectiveName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
                    embedBuilder.addField("Рейтинговая система:", "Уровень: " + "`" + document.get("lvl") + "`" + "\n" + "Количество сообщений: " + "`" + document.get("message") + "`" + "\n" + "Опыта до нового уровня: " + "`" + Integer.parseInt(String.valueOf(document.get("newRank"))) + "`" + "\n" + "Время в голосовом канале: `" + Long.parseLong(document.get("minvoice").toString())/1440 + "д." + ((Long.parseLong(document.get("minvoice").toString()))%1440)/60 + "ч." + (((Long.parseLong(document.get("minvoice").toString()))%1440)%60) + "м." + "`" + "\n" + "Количество коинов: `" + document.get("coins") + "`\nКейсы: " + "\n > Кейс №1: `" + document.get("case1") + "`\n > Кейс №2: `" + document.get("case2") + "`\n > Кейс №3: `" + document.get("case3") + "`\n > Кейс №4: `" + document.get("case4") + "`", false);
                    Color purple = new Color(76, 0, 230);
                    embedBuilder.setColor(purple);
                    embedBuilder.setThumbnail(event.getAuthor().getAvatarUrl());
                    embedBuilder.setFooter(event.getGuild().getName(), event.getGuild().getIconUrl());
                    event.getMessage().reply(embedBuilder.build()).queue();
            } else {
                if (members.get(0).getUser().isBot()) {
                    return;
                }
                long id = Long.parseLong(members.get(0).getId());
                Document document = collection.find(new Document("id",id)).first();
                if (document == null){
                    DBNewUser dbNewUser = new DBNewUser();
                    dbNewUser.NewMentionUser(collection,members);
                }
                    EmbedBuilder embedBuilder = new EmbedBuilder();
                    embedBuilder.setAuthor(members.get(0).getEffectiveName(), members.get(0).getUser().getAvatarUrl(), members.get(0).getUser().getEffectiveAvatarUrl());
                embedBuilder.addField("Рейтинговая система:", "Уровень: " + "`" + document.get("lvl") + "`" + "\n" + "Количество сообщений: " + "`" + document.get("message") + "`" + "\n" + "Опыта до нового уровня: " + "`" + Integer.parseInt(String.valueOf(document.get("newRank"))) + "`" + "\n" + "Время в голосовом канале: `" + Long.parseLong(document.get("minvoice").toString())/1440 + "д." + ((Long.parseLong(document.get("minvoice").toString()))%1440)/60 + "ч." + (((Long.parseLong(document.get("minvoice").toString()))%1440)%60) + "м." + "`" + "\n" + "Количество коинов: `" + document.get("coins") + "`\nКейсы: " + "\n > Кейс №1: `" + document.get("case1") + "`\n > Кейс №2: `" + document.get("case2") + "`\n > Кейс №3: `" + document.get("case3") + "`\n > Кейс №4: `" + document.get("case4") + "`", false);
                    Color purple = new Color(76, 0, 230);
                    embedBuilder.setColor(purple);
                    embedBuilder.setThumbnail(members.get(0).getUser().getAvatarUrl());
                    embedBuilder.setFooter(event.getGuild().getName(), event.getGuild().getIconUrl());
                    event.getMessage().reply(embedBuilder.build()).queue();
            }
            TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
            String commandname = "Level";
            LogCommand logCommand = new LogCommand();
            channel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
