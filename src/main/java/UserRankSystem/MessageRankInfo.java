package UserRankSystem;

import net.dv8tion.jda.api.entities.Member;

public class MessageRankInfo {
    private String userID;
    private long rank;
    private int level;
    private int newRank;
    private int money;
    private int cookies;
    private int case1;
    private int case2;
    private int case3;
    private int case4;

    public MessageRankInfo() {

    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setRank(long userrank) {
        this.rank = userrank;
    }

    public void setLevel(int level) {this.level = level;}

    public void setNewRank(int newRank) {this.newRank = newRank;}

    public void setMoney(int money) {this.money = money;}

    public int getCookies() {
        return cookies;
    }

    public void setCookies(int cookies) {
        this.cookies = cookies;
    }

    public int getCase3() {
        return case3;
    }

    public void setCase3(int case3) {
        this.case3 = case3;
    }

    public int getCase2() {
        return case2;
    }

    public void setCase2(int case2) {
        this.case2 = case2;
    }

    public int getCase1() {
        return case1;
    }

    public void setCase1(int case1) {
        this.case1 = case1;
    }

    public int getCase4() {
        return case4;
    }

    public void setCase4(int case4) {
        this.case4 = case4;
    }

    public MessageRankInfo(final Member member, long userrank, int userlevel, int userNewRank, int money, int cookies, int case1, int case2, int case3, int case4) {
        this.userID = member.getId();
        this.rank = userrank;
        this.level = userlevel;
        this.newRank = userNewRank;
        this.money = money;
        this.cookies = cookies;
        this.case1 = case1;
        this.case2 = case2;
        this.case3 = case3;
        this.case4 = case4;
    }
    public int getMoney(){ return this.money;}

    public String getUserID() {
        return this.userID;
    }

    public long getRank() {
        return this.rank;
    }

    public int getLevel() {return this.level;}

    public int getNewRank() {return this.newRank;}

}
