package UserRankSystem.Economy;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class WorkCommand extends Command {

    public WorkCommand() {
        this.name = "work";
        this.guildOnly = true;
        this.cooldown = 1000;
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            File memberFile = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
            ObjectMapper mapper = new ObjectMapper();
            final MessageRankInfo messageRankInfoMember;
            try {
                messageRankInfoMember = mapper.readValue(memberFile, MessageRankInfo.class);
                Random random = new Random();
                int min = 1;
                int max = 5;
                int diff = max - min;
                int work = random.nextInt(diff + 1) + min;
                String[] workarr = new String[6];
                workarr[1] = "Фриланс";
                workarr[2] = "Завод";
                workarr[3] = "Магазин";
                workarr[4] = "Школа";
                workarr[5] = "Дворник";
                int min1 = 100;
                int max1 = 2000;
                int diff1 = max1 - min1;
                int money = random.nextInt(diff1 + 1) + min1;
                messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + money);
                mapper.writeValue(memberFile, messageRankInfoMember);
                EmbedBuilder embedBuilder = new EmbedBuilder();
                Color purple = new Color(76, 0, 230);
                embedBuilder.setColor(purple);
                embedBuilder.setAuthor(event.getAuthor().getAsTag(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
                embedBuilder.setFooter(event.getGuild().getName(), event.getGuild().getIconUrl());
                embedBuilder.setTitle("Отрасль работы - " + workarr[work] + ", вы заработали - `" + money + "`");
                event.getMessage().reply(embedBuilder.build()).queue();
                TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
                LogCommand logCommand = new LogCommand();
                String commandname = "Work";
                logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
