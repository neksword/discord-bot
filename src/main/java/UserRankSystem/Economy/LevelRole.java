package UserRankSystem.Economy;

import UserRankSystem.MessageRankInfo;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;

public class LevelRole {
    public void LevelRole(final int level, final Member member , final Guild guild){
        Role role5level = guild.getRoleById("849517353838575626");
        Role role10level = guild.getRoleById("849517728115982386");
        Role role15level = guild.getRoleById("849518045504340028");
        Role role20level = guild.getRoleById("849518385632641034");
        Role role25level = guild.getRoleById("849518932456112139");
        if (level == 5){
            guild.addRoleToMember(member,role5level).queue();
        }
        else if (level == 10){
            guild.removeRoleFromMember(member,role5level).queue();
            guild.addRoleToMember(member,role10level).queue();
        }
        else if (level == 15){
            guild.removeRoleFromMember(member,role10level).queue();
            guild.addRoleToMember(member,role15level).queue();
        }
        else if (level == 20){
            guild.removeRoleFromMember(member,role15level).queue();
            guild.addRoleToMember(member,role20level).queue();
        }
        else if (level == 25){
            guild.removeRoleFromMember(member,role20level).queue();
            guild.addRoleToMember(member,role25level).queue();
        }
    }
}
