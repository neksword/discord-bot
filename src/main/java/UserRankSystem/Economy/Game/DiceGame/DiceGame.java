package UserRankSystem.Economy.Game.DiceGame;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.Economy.BotMoney.BotMoneyInfo;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


public class DiceGame extends Command {
    public DiceGame()
    {
        this.name = "dice";
        this.guildOnly = true;
        this.cooldown = 60;
    }
    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                List<Member> members = event.getMessage().getMentionedMembers();
                if (!(new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json").exists())) {
                    File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                    file.createNewFile();
                    final MessageRankInfo rankInfo = new MessageRankInfo(event.getMember(), 1, 1, 15, 1,1,0,0,0,0);
                    mapper.writeValue(file, rankInfo);
                }
                if (!members.isEmpty()) {
                    if (!(new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + members.get(0).getId() + ".json").exists())) {
                        File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + members.get(0).getId() + ".json");
                        file.createNewFile();
                        final MessageRankInfo rankInfo = new MessageRankInfo(members.get(0), 1, 1, 15, 1,1,0,0,0,0);
                        mapper.writeValue(file, rankInfo);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            String[] command = event.getMessage().getContentRaw().split("\\s");
            try {
                if (command.length >= 2) {
                    File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                    ObjectMapper mapper = new ObjectMapper();
                    int stavka = Integer.parseInt(command[1].trim());
                    MessageRankInfo messageRankInfo = mapper.readValue(file, MessageRankInfo.class);
                    System.out.println("stavka - " + stavka);
                    System.out.println("money - " + messageRankInfo.getMoney());
                    if (stavka < 99) {
                        event.getMessage().reply("**Укажите ставку побольше!**").queue();
                    } else {
                        if (stavka > messageRankInfo.getMoney()) {
                            event.replyError("**У вас недостатчно монет!**");
                        } else {
                            File file1 = new File("/home/" + cfg.homedirectory() + "/BotMoney.json");
                            BotMoneyInfo botMoneyInfo = mapper.readValue(file1, BotMoneyInfo.class);
                            System.out.println("bot - " + botMoneyInfo.getMoney());
                            int stavka2 = stavka + stavka;
                            System.out.println("stavka2 - " + stavka2);
                            System.out.println(stavka2 > botMoneyInfo.getDicemoney());
                            if (stavka2 > botMoneyInfo.getDicemoney()) {
                                Random random = new Random();
                                int min1 = 1;
                                int max1 = 3;
                                int diff1 = max1 - min1;
                                int dice1 = random.nextInt(diff1 + 1) + min1;
                                int dice2 = random.nextInt(diff1 + 1) + min1;
                                int minbot = 3;
                                int maxbot = 10;
                                int diffbot = maxbot - minbot;
                                AtomicInteger diceBot = new AtomicInteger(random.nextInt(diffbot + 1) + minbot);
                                event.reply("**Подкидываю кубики...\uD83C\uDFB2**", message -> {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    int dicemember = dice1 + dice2;
                                    if (dicemember >= diceBot.get()) {
                                        diceBot.set(12);
                                    }

                                        builder.setDescription("❌ Вы проиграли!");
                                        builder.addField("Твой счёт - " + dicemember, "1. " + emoji(dice1) + "\n" + "2. " + emoji(dice2), true);
                                        builder.addField("Счёт бота - " + diceBot, "", true);
                                        builder.setColor(Color.RED);

                                        messageRankInfo.setMoney(messageRankInfo.getMoney() - stavka);
                                        botMoneyInfo.setDicemoney((int) (botMoneyInfo.getDicemoney() + stavka * 0.1));
                                        try {
                                            mapper.writeValue(file, messageRankInfo);
                                            mapper.writeValue(file1, botMoneyInfo);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    message.editMessage(builder.build()).queueAfter(3, TimeUnit.SECONDS);
                                });
                            } else {
                                Random random = new Random();
                                int min1 = 1;
                                int max1 = 6;
                                int diff1 = max1 - min1;
                                int dice1 = random.nextInt(diff1 + 1) + min1;
                                int dice2 = random.nextInt(diff1 + 1) + min1;
                                int minbot = 1;
                                int maxbot = 12;
                                int diffbot = maxbot - minbot;
                                AtomicInteger diceBot = new AtomicInteger(random.nextInt(diffbot + 1) + minbot);
                                event.reply("**Подкидываю кубики...\uD83C\uDFB2**", message -> {
                                    EmbedBuilder builder = new EmbedBuilder();
                                    int dicemember = dice1 + dice2;
                                    if (diceBot.get() == 0){
                                        diceBot.set(diceBot.get() + 1);
                                    }
                                    if (dicemember > diceBot.get()) {
                                        builder.setDescription("✅ Вы выиграли!");
                                        builder.addField("Твой счёт - " + dicemember, "1. " + emoji(dice1) + "\n" + "2. " + emoji(dice2), true);
                                        builder.setColor(Color.GREEN);
                                        builder.addField("Счёт бота - " + diceBot, "", true);

                                        messageRankInfo.setMoney(messageRankInfo.getMoney() + stavka);
                                        botMoneyInfo.setDicemoney((int) (botMoneyInfo.getDicemoney() + stavka * 0.1 - stavka));
                                        try {
                                            mapper.writeValue(file, messageRankInfo);
                                            mapper.writeValue(file1, botMoneyInfo);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else if (dicemember < diceBot.get()) {
                                        builder.setDescription("❌ Вы проиграли!");
                                        builder.addField("Твой счёт - " + dicemember, "1. " + emoji(dice1) + "\n" + "2. " + emoji(dice2), true);
                                        builder.addField("Счёт бота - " + diceBot, "", true);
                                        builder.setColor(Color.RED);

                                        messageRankInfo.setMoney(messageRankInfo.getMoney() - stavka);
                                        botMoneyInfo.setDicemoney((int) (botMoneyInfo.getDicemoney() + stavka * 0.1));
                                        try {
                                            mapper.writeValue(file, messageRankInfo);
                                            mapper.writeValue(file1, botMoneyInfo);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else if (dicemember == diceBot.get()) {
                                        builder.setDescription("\uD83D\uDD30Ничья!");
                                        builder.addField("Твой счёт - " + dicemember, "1. " + emoji(dice1) + "\n" + "2. " + emoji(dice2), true);
                                        builder.addField("Счёт бота - " + diceBot, "", true);
                                        builder.setColor(Color.YELLOW);
                                    }
                                    message.editMessage(builder.build()).queueAfter(3, TimeUnit.SECONDS);
                                });
                            }
                        }
                    }
                } else {
                    event.replyError("**Укажите ставку!**");
                }
            } catch (NumberFormatException | IOException e) {
                e.printStackTrace();
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Dice";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
    public String emoji(int number){
        if (number == 1) return "1️⃣";
        if (number == 2) return "2️⃣";
        if (number == 3) return "3️⃣";
        if (number == 4) return "4️⃣";
        if (number == 5) return "5️⃣";
        if (number == 6) return "6️⃣";
        return "АШИБКА";
    }
}

