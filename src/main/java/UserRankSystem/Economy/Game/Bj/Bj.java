package UserRankSystem.Economy.Game.Bj;

import BotConfig.BotConfig;
import UserRankSystem.Economy.Game.Bj.BotLose.Hit;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.mongodb.client.MongoCollection;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.Button;
import net.dv8tion.jda.api.requests.restaction.interactions.ReplyAction;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.util.Arrays;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Bj extends ListenerAdapter {
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    private final Map<String, BjInfo> BjCache = new ConcurrentHashMap<String, BjInfo>();
    private MongoCollection<Document> collection;
    private EventWaiter waiter;

    public Bj(MongoCollection<Document> collection, EventWaiter waiter) {
        this.collection = collection;
        this.waiter = waiter;
    }

    public void onSlashCommand(SlashCommandEvent event) {
        if (event.getName().equals("bj")) {
            EmbedBuilder builder = new EmbedBuilder();
            BjInfo bjInfo = new BjInfo();
            bjInfo.setCard(CardCreate());
            int[] user = bjInfo.getUser();
            int[] card = bjInfo.getCard();
            AtomicInteger card12 = new AtomicInteger();
            for (int i = 0; i < 2; i++) {
                int e = Random();
                if (card[e] == 0) {
                    for (int i1 = 0; i1 < 5; i1++) {
                        int e1 = Random();
                        if (card[e1] == 0) {
                        } else {
                            user[i1] = card[e1];
                            card[e1] = 0;
                            break;
                        }
                    }
                } else {
                    user[i] = card[e];
                    card[e] = 0;
                }
            }
            int[] bot = bjInfo.getBot();
            for (int i = 0; i < 2; i++) {
                int e = Random();
                if (card[e] == 0) {
                    for (int i1 = 0; i1 < 5; i1++) {
                        int e1 = Random();
                        if (card[e1] == 0) {
                        } else {
                            bot[i1] = card[e1];
                            card[e1] = 0;
                            break;
                        }
                    }
                } else {
                    bot[i] = card[e];
                    card[e] = 0;
                }
            }

            for (int i = 0; i < 2; i++){
                if (user[0] == 11){
                    int[] userTuz = bjInfo.getUsertuz();
                    userTuz[0] = 1;
                    bjInfo.setUsertuz(userTuz);
                }
                if (user[1] == 11){
                    int[] userTuz = bjInfo.getUsertuz();
                    if (userTuz[0] == 0){
                        userTuz[0] = 1;
                        bjInfo.setUsertuz(userTuz);
                    }
                    else {
                        userTuz[1] = 1;
                        bjInfo.setUsertuz(userTuz);
                    }
                }
            }

            for (int i = 0; i < 2; i++){
                if (bot[0] == 11){
                    int[] botTuz = bjInfo.getBottuz();
                    botTuz[0] = 1;
                    bjInfo.setBottuz(botTuz);
                }
                if (bot[1] == 11){
                    int[] botTuz = bjInfo.getBottuz();
                    if (botTuz[0] == 0){
                        botTuz[0] = 1;
                        bjInfo.setBottuz(botTuz);
                    }
                    else {
                        botTuz[1] = 1;
                        bjInfo.setBottuz(botTuz);
                    }
                }
            }


            builder.setDescription("Чтобы взять карту - `hit`, чтобы закончить `stand`");
            builder.setAuthor(event.getMember().getUser().getName(), event.getMember().getUser().getEffectiveAvatarUrl(), event.getMember().getUser().getEffectiveAvatarUrl());
            builder.addField("Ваши карты:", "```" + user[0] + " | " + user[1] + "```", true);
            builder.addField("Карты бота:", "```" + bot[0] + " | " + "```", true);
            builder.addField("Скрытая карта:", bot[1] + " | ", false);
            ReplyAction message = event.replyEmbeds(builder.build()).addActionRow(Button.success("hit", "Hit"), Button.danger("stand", "Stand"));
            bjInfo.setMessage(message.complete().retrieveOriginal().complete());
            bjInfo.setCard(card);
            bjInfo.setBot(bot);
            bjInfo.setUser(user);
            BjCache.put(event.getMember().getId(), bjInfo);
        }
    }
        public void onButtonClick(ButtonClickEvent event) {
            if (event.getComponentId().equals("hit")) {
                if (BjCache.get(event.getMember().getId()) != null) {
                    if (BjCache.get(event.getMember().getId()).getMessage().getId().equals(event.getMessageId())) {
                        if (BjCache.get(event.getMember().getId()).isBotwin()) {
                            //botwin
                        } else {
                            //botlose
                            final Hit hit = new Hit(BjCache);
                            int cardnum = hit.UserHit(event);
                            int cardnumbot = hit.DealerHit(event);
                            final BjInfo bjInfo = BjCache.get(event.getMember().getId());
                            AtomicInteger userCardSum = new AtomicInteger();
                            int[] user = bjInfo.getUser();
                            for (int i = 0; i < user.length; i++) {
                                if (user[i] != 0) {
                                    userCardSum.getAndAdd(1);
                                }
                            }
                            StringBuilder stringBuilder = new StringBuilder();
                            for (int i = 0; i <= userCardSum.get() - 1; i++) {
                                stringBuilder.append(user[i]).append(" | ");
                            }
                            AtomicInteger botCardSum = new AtomicInteger();
                            int[] bot = bjInfo.getBot();
                            for (int i = 0; i < bot.length; i++) {
                                if (bot[i] != 0) {
                                    botCardSum.getAndAdd(1);
                                }
                            }
                            StringBuilder stringbuilder = new StringBuilder();
                            for (int i = 0; i < botCardSum.get() - 1; i++) {
                                stringbuilder.append(bot[i]).append(" | ");
                            }
                            EmbedBuilder builder = new EmbedBuilder(bjInfo.getMessage().getEmbeds().get(0));
                            builder.clearFields();
                            builder.addField("Ваши карты:", "```" + stringBuilder.toString() + "```", true);
                            builder.addField("Карты бота:", "```" + stringbuilder.toString() + "```", true);
                            builder.addField("Скрытая карта", String.valueOf(bot[botCardSum.get() - 1]), false);
                            bjInfo.getMessage().editMessage(builder.build()).queue();
                            event.getMessage().reply(Arrays.toString(BjCache.get(event.getMember().getId()).getUser()) + " | " + Arrays.toString(BjCache.get(event.getMember().getId()).getBot()) + " | " + Arrays.toString(BjCache.get(event.getMember().getId()).getBottuz()) + " | " + Arrays.toString(BjCache.get(event.getMember().getId()).getUsertuz())).queue();
//                        Button button = event.getButton().asDisabled();
//                        event.editButton(button).queue();
                        }
                    }
                }
            }
            else if (event.getComponentId().equals("stand")){

            }
        }
    private void Hit(ButtonClickEvent event){
        if (BjCache.get(event.getMember().getId()) != null) {
            if (BjCache.get(event.getMember().getId()).isBotwin()) {
                //botwin
            } else {
                //botlose
                final Hit hit = new Hit(BjCache);
                int cardnum = hit.UserHit(event);
                int cardnumbot = hit.DealerHit(event);
                final BjInfo bjInfo = BjCache.get(event.getMember().getId());
                AtomicInteger userCardSum = new AtomicInteger();
                int[] user = bjInfo.getUser();
                for (int i = 0; i < user.length; i++){
                    if (user[i] != 0) {
                        userCardSum.getAndAdd(1);
                    }
                }
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i <= userCardSum.get() - 1; i++){
                    stringBuilder.append(user[i]).append(" | ");
                }
                AtomicInteger botCardSum = new AtomicInteger();
                int[] bot = bjInfo.getBot();
                for (int i = 0; i < bot.length; i++){
                    if (bot[i] != 0) {
                        botCardSum.getAndAdd(1);
                    }
                }
                StringBuilder stringbuilder = new StringBuilder();
                for (int i = 0; i < botCardSum.get() - 1; i++){
                    stringbuilder.append(bot[i]).append(" | ");
                }

                EmbedBuilder builder = new EmbedBuilder(bjInfo.getMessage().getEmbeds().get(0));
                builder.clearFields();
                builder.addField("Ваши карты:","```" +  stringBuilder.toString() + "```", true);
                builder.addField("Карты бота:","```" +  stringbuilder.toString() + "```", true);
                builder.addField("Скрытая карта", String.valueOf(bot[botCardSum.get() - 1]), false);
                bjInfo.getMessage().editMessage(builder.build()).queue();
                event.getMessage().reply(Arrays.toString(BjCache.get(event.getMember().getId()).getUser())).queue();
                event.getMessage().reply(Arrays.toString(BjCache.get(event.getMember().getId()).getBot())).queue();
                event.getMessage().delete().queue();
            }
        }
    }
    private int Random() {
        Random random = new Random();
        int min = 0;
        int max = 51;
        int diff = max - min;
        int num = random.nextInt(diff + 1) + min;
        return num;
    }
    //дальше параметры карт
    private int CardNum(int card) {
        switch (card) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 6:
                return 6;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 9;
            case 10:
                return 10;
            case 11:
                return 1;
            case 12:
                return 2;
            case 13:
                return 3;
            case 14:
                return 4;
            case 15:
                return 5;
            case 16:
                return 6;
            case 17:
                return 7;
            case 18:
                return 8;
            case 19:
                return 9;
            case 20:
                return 10;
            case 21:
                return 1;
            case 22:
                return 2;
            case 23:
                return 3;
            case 24:
                return 4;
            case 25:
                return 5;
            case 26:
                return 6;
            case 27:
                return 7;
            case 28:
                return 8;
            case 29:
                return 9;
            case 30:
                return 10;
            case 31:
                return 1;
            case 32:
                return 2;
            case 33:
                return 3;
            case 34:
                return 4;
            case 35:
                return 5;
            case 36:
                return 6;
            case 37:
                return 7;
            case 38:
                return 8;
            case 39:
                return 9;
            case 40:
                return 10;
            case 41:
                return 1;
            case 42:
                return 2;
            case 43:
                return 3;
            case 44:
                return 4;
            case 45:
                return 5;
            case 46:
                return 6;
            case 47:
                return 7;
            case 48:
                return 8;
            case 49:
                return 9;
            case 50:
                return 10;
            case 51:
                return 10;
            case 52:
                return 11;
        }
        return 1;
    }

    private int[] CardCreate() {
        int[] user = new int[52];
        user[0] = 2;
        user[1] = 3;
        user[2] = 4;
        user[3] = 5;
        user[4] = 6;
        user[5] = 7;
        user[6] = 8;
        user[7] = 9;
        user[8] = 10;
        user[9] = 2;
        user[10] = 3;
        user[11] = 4;
        user[12] = 5;
        user[13] = 6;
        user[14] = 7;
        user[15] = 8;
        user[16] = 9;
        user[17] = 10;
        user[18] = 2;
        user[19] = 3;
        user[20] = 4;
        user[21] = 5;
        user[22] = 6;
        user[23] = 7;
        user[24] = 8;
        user[25] = 9;
        user[26] = 10;
        user[27] = 2;
        user[28] = 3;
        user[29] = 4;
        user[30] = 5;
        user[31] = 6;
        user[32] = 7;
        user[33] = 8;
        user[34] = 9;
        user[35] = 10;
        user[36] = 10;
        user[37] = 10;
        user[38] = 10;
        user[39] = 10;
        user[40] = 10;
        user[41] = 10;
        user[42] = 10;
        user[43] = 10;
        user[44] = 10;
        user[45] = 10;
        user[46] = 10;
        user[47] = 10;
        user[48] = 11;
        user[49] = 11;
        user[50] = 11;
        user[51] = 11;
        return user;
    }
}
