package UserRankSystem.Economy.Game.Bj;

import net.dv8tion.jda.api.entities.Message;

public class BjInfo {
    private int[] card = new int[52];
    private int[] user = new int[10];
    private int[] bot = new int[10];
    private int[] Usertuz = new int[4];
    private int[] Bottuz = new int[4];


    public int[] getBottuz() {
        return Bottuz;
    }

    public void setBottuz(int[] bottuz) {
        Bottuz = bottuz;
    }

    public int[] getUsertuz() {
        return Usertuz;
    }

    public void setUsertuz(int[] usertuz) {
        Usertuz = usertuz;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    private int stage;
    private Message message;

    public boolean isBotwin() {
        return Botwin;
    }

    public void setBotwin(boolean botwin) {
        Botwin = botwin;
    }

    private boolean Botwin;
    public BjInfo(){

    }

    public int[] getCard() {
        return card;
    }

    public void setCard(int[] card) {
        this.card = card;
    }
    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public int[] getBot() {
        return bot;
    }

    public void setBot(int[] bot) {
        this.bot = bot;
    }

    public int[] getUser() {
        return user;
    }

    public void setUser(int[] user) {
        this.user = user;
    }

    public BjInfo(int[] card, Message message){
        this.card = card;
        this.message = message;
    }
}
