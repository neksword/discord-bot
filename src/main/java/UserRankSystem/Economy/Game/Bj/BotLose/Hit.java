package UserRankSystem.Economy.Game.Bj.BotLose;

import UserRankSystem.Economy.Game.Bj.BjInfo;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;

import java.util.Arrays;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Hit {
    private final Map<String, BjInfo> BjCache;
    public Hit(final Map<String, BjInfo> BjCache){
        this.BjCache = BjCache;
    }
    public int UserHit(ButtonClickEvent event){
        final BjInfo bjInfo = BjCache.get(event.getMember().getId());
        int[] userCard = bjInfo.getUser();
        int[] card = bjInfo.getCard();
        AtomicInteger userCardSum = new AtomicInteger();
        AtomicInteger card12 = new AtomicInteger();
        for (int i = 0; i < userCard.length; i++){
            if (userCard[i] != 0){
                userCardSum.addAndGet(1);
            }
            else {
                break;
            }
        }
        while (true){
            int card1 = Random();
            if (card[card1] != 0){
                userCard[userCardSum.get()] = card[card1];
                card12.set(card1);
                card[card1] = 0;
                break;
            }
        }
        BjCache.get(event.getMember().getId()).setUser(userCard);
        BjCache.get(event.getMember().getId()).setCard(card);
        tuzCheck(bjInfo.getUsertuz(), userCard, userCardSum.get(),false, event, userCard[userCardSum.get()]);
        return card12.get();
    }
    public int DealerHit(ButtonClickEvent event){
        final BjInfo bjInfo = BjCache.get(event.getMember().getId());
        int[] botCard = bjInfo.getBot();
        int[] card = bjInfo.getCard();
        AtomicInteger botCardSum = new AtomicInteger();
        AtomicInteger card12 = new AtomicInteger();
        for (int i = 0; i < botCard.length; i++){
            if (botCard[i] != 0){
                botCardSum.addAndGet(1);
            }
            else {
                break;
            }
        }
        while (true){
            int card1 = Random();
            if (card[card1] != 0){
                botCard[botCardSum.get()] = card[card1];
                card[card1] = 0;
                card12.set(card1);
                break;
            }
        }
        BjCache.get(event.getMember().getId()).setBot(botCard);
        BjCache.get(event.getMember().getId()).setCard(card);
        tuzCheck(bjInfo.getBottuz(), botCard, botCardSum.get(),true, event, botCard[botCardSum.get()]);
        return card12.get();
    }
    private int Random() {
        Random random = new Random();
        int min = 0;
        int max = 51;
        int diff = max - min;
        int num = random.nextInt(diff + 1) + min;
        return num;
    }
    private void tuzCheck(int[] cardTuz, int[] card, int cardNum, boolean bot, ButtonClickEvent event, int cardHit) {
        if (Arrays.stream(card).sum() > 21) {
            if (cardHit == 11) {
                if (bot) {
                    cardTuz = BjCache.get(event.getMember().getId()).getBottuz();
                    if (cardTuz[0] == 0) {
                        cardTuz[0] = 1;
                        BjCache.get(event.getMember().getId()).setBottuz(cardTuz);
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setBot(card);
                    } else if (cardTuz[0] != 0 && cardTuz[1] == 0) {
                        cardTuz[1] = 1;
                        BjCache.get(event.getMember().getId()).setBottuz(cardTuz);
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setBot(card);
                    } else if (cardTuz[0] != 0 && cardTuz[1] != 0 && cardTuz[2] == 0) {
                        cardTuz[2] = 1;
                        BjCache.get(event.getMember().getId()).setBottuz(cardTuz);
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setBot(card);
                    } else {
                        cardTuz[3] = 1;
                        BjCache.get(event.getMember().getId()).setBottuz(cardTuz);
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setBot(card);
                    }
                } else {
                    cardTuz = BjCache.get(event.getMember().getId()).getUsertuz();
                    if (cardTuz[0] == 0) {
                        cardTuz[0] = 1;
                        BjCache.get(event.getMember().getId()).setUsertuz(cardTuz);
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setUser(card);
                    } else if (cardTuz[0] != 0 && cardTuz[1] == 0) {
                        cardTuz[1] = 1;
                        BjCache.get(event.getMember().getId()).setUsertuz(cardTuz);
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setUser(card);
                    } else if (cardTuz[0] != 0 && cardTuz[1] != 0 && cardTuz[2] == 0) {
                        cardTuz[2] = 1;
                        BjCache.get(event.getMember().getId()).setUsertuz(cardTuz);
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setUser(card);
                    } else {
                        cardTuz[3] = 1;
                        BjCache.get(event.getMember().getId()).setUsertuz(cardTuz);
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setUser(card);
                    }
                }
            }
            else {
                if (bot){
                    cardTuz = BjCache.get(event.getMember().getId()).getBottuz();
                    AtomicInteger botTuzCard = new AtomicInteger();
                    for (int i = 0; i <= 3; i++){
                        if (cardTuz[i] != 0){
                            botTuzCard.getAndAdd(1);
                        }
                        else {
                            if (i == 0) {
                                botTuzCard.getAndAdd(0);
                                break;
                            }
                            else {
                                botTuzCard.getAndAdd(1);
                                break;
                            }
                        }
                    }
                    if (cardTuz[botTuzCard.get()] != 0){
                        cardTuz[botTuzCard.get()] = 1;
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setBottuz(cardTuz);
                        BjCache.get(event.getMember().getId()).setBot(card);
                    }
                }
                else {
                    cardTuz = BjCache.get(event.getMember().getId()).getUsertuz();
                    AtomicInteger userTuzCard = new AtomicInteger();
                    for (int i = 0; i <= 3; i++){
                        if (cardTuz[i] != 0){
                            userTuzCard.getAndAdd(1);
                        }
                        else {
                            if (i == 0) {
                                userTuzCard.getAndAdd(0);
                                break;
                            }
                            else {
                                userTuzCard.getAndAdd(1);
                                break;
                            }
                        }
                    }
                    if (cardTuz[userTuzCard.get()] != 0){
                        cardTuz[userTuzCard.get()] = 1;
                        card[cardNum] = 1;
                        BjCache.get(event.getMember().getId()).setUsertuz(cardTuz);
                        BjCache.get(event.getMember().getId()).setUser(card);
                    }
                }
            }
        }
        else {
            if (cardHit == 11){
                if (bot){
                    cardTuz = BjCache.get(event.getMember().getId()).getBottuz();
                    AtomicInteger botTuzCard = new AtomicInteger();
                    for (int i = 0; i <= 3; i++){
                        if (cardTuz[i] != 0){
                            botTuzCard.getAndAdd(1);
                        }
                        else {
                            if (i == 0) {
                                botTuzCard.getAndAdd(0);
                                break;
                            }
                            else {
                                botTuzCard.getAndAdd(1);
                                break;
                            }
                        }
                    }
                    cardTuz[botTuzCard.get()] = 1;
                    BjCache.get(event.getMember().getId()).setBottuz(cardTuz);
                }
                else {
                    cardTuz = BjCache.get(event.getMember().getId()).getUsertuz();
                    AtomicInteger userTuzCard = new AtomicInteger();
                    for (int i = 0; i <= 3; i++){
                        if (cardTuz[i] != 0){
                            userTuzCard.getAndAdd(1);
                        }
                        else {
                            if (i == 0) {
                                userTuzCard.getAndAdd(0);
                                break;
                            }
                            else {
                                userTuzCard.getAndAdd(1);
                                break;
                            }
                        }
                    }
                    cardTuz[userTuzCard.get()] = 1;
                    BjCache.get(event.getMember().getId()).setUsertuz(cardTuz);
                }
            }
        }
    }
}
