package UserRankSystem.Economy.Game.Bj;

import java.util.Arrays;
import java.util.Map;

public class BjCheck {
    private final Map<String, BjInfo> BjCache;
    public BjCheck(final Map<String, BjInfo> BjCache){
        this.BjCache = BjCache;
    }

    public boolean BJWin(int[] userCard){
        if (Arrays.stream(userCard).sum() == 21){
            return true;
        }
        return false;
    }
    public boolean BJLose(int[] botCard){
        if (Arrays.stream(botCard).sum() == 21){
            return true;
        }
        return false;
    }
}
