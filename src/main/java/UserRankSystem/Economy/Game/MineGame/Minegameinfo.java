package UserRankSystem.Economy.Game.MineGame;

public class Minegameinfo {
    private int pickaxe;

    public Minegameinfo(){

    }

    public void setPickaxe(int pickaxe) {
        this.pickaxe = pickaxe;
    }

    public Minegameinfo(int userpickaxe){
        this.pickaxe = userpickaxe;
    }

    public int getPickaxe() {
        return pickaxe;
    }
}
