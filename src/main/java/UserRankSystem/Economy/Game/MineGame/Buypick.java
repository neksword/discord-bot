package UserRankSystem.Economy.Game.MineGame;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Buypick extends Command {
    public Buypick() {
        this.name = "buypick";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            BotConfig cfg = ConfigFactory.create(BotConfig.class);
            if (!(new File("/home/" + cfg.homedirectory() + "/GameBotDiscord/MineGame/" + event.getMember().getId() + ".json").exists())) {
                try {
                    File file = new File("/home/" + cfg.homedirectory() + "/GameBotDiscord/MineGame/" + event.getMember().getId() + ".json");
                    file.createNewFile();
                    ObjectMapper mapper = new ObjectMapper();
                    Minegameinfo minegameinfo = new Minegameinfo(0);
                    mapper.writeValue(file, minegameinfo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                File file = new File("/home/" + cfg.homedirectory() + "/GameBotDiscord/MineGame/" + event.getMember().getId() + ".json");
                File file1 = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                ObjectMapper mapper = new ObjectMapper();
                try {
                    Minegameinfo minegameinfo = mapper.readValue(file, Minegameinfo.class);
                    MessageRankInfo messageRankInfo = mapper.readValue(file1, MessageRankInfo.class);
                    if (command.length == 1) {
                        EmbedBuilder embedBuilder = new EmbedBuilder();
                        embedBuilder.setAuthor(event.getMember().getNickname(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
                        embedBuilder.setTitle("Магазин:");
                        if (minegameinfo.getPickaxe() == 0) {
                            embedBuilder.setDescription("У вас нет кирки.");
                        } else if (minegameinfo.getPickaxe() == 1) {
                            embedBuilder.setDescription("У вас каменная кирка.");
                        } else if (minegameinfo.getPickaxe() == 2) {
                            embedBuilder.setDescription("У вас железная кирка.");
                        } else if (minegameinfo.getPickaxe() == 3) {
                            embedBuilder.setDescription("У вас алмазная кирка.");
                        }
                        embedBuilder.addField("Кирки: ", "1. Каменная кирка | 10000 \n" + "2. Железная кирка | 200000 \n" + "3. Алмазная кирка | 700000", false);
                        embedBuilder.addField("Чем дороже кирка, тем больше она выкапывает.", "", false);
                        Color purple = new Color(76, 0, 230);
                        embedBuilder.setColor(purple);
                        embedBuilder.setFooter(event.getGuild().getName(), event.getGuild().getIconUrl());
                        event.getMessage().reply(embedBuilder.build()).queue();
                    } else {
                        if (command[1].equalsIgnoreCase(String.valueOf(1))) {
                            if (messageRankInfo.getMoney() >= 10000) {
                                messageRankInfo.setMoney(messageRankInfo.getMoney() - 10000);
                                minegameinfo.setPickaxe(1);
                                mapper.writeValue(file, minegameinfo);
                                mapper.writeValue(file1, messageRankInfo);
                                event.getMessage().reply("**Вы купили каменную кирку!**").queue();
                            } else {
                                event.getMessage().reply("**У вас недостаточно монет!**").queue();
                            }
                        } else if (command[1].equalsIgnoreCase(String.valueOf(2))) {
                            if (messageRankInfo.getMoney() >= 200000) {
                                messageRankInfo.setMoney(messageRankInfo.getMoney() - 200000);
                                minegameinfo.setPickaxe(2);
                                mapper.writeValue(file, minegameinfo);
                                mapper.writeValue(file1, messageRankInfo);
                                event.getMessage().reply("**Вы купили железную кирку!**").queue();
                            } else {
                                event.getMessage().reply("**У вас недостаточно монет!**").queue();
                            }
                        } else if (command[1].equalsIgnoreCase(String.valueOf(3))) {
                            if (messageRankInfo.getMoney() >= 700000) {
                                messageRankInfo.setMoney(messageRankInfo.getMoney() - 700000);
                                minegameinfo.setPickaxe(3);
                                mapper.writeValue(file, minegameinfo);
                                mapper.writeValue(file1, messageRankInfo);
                                event.getMessage().reply("**Вы купили алмазную кирку!**").queue();
                            } else {
                                event.getMessage().reply("**У вас недостаточно монет!**").queue();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Buypick";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
