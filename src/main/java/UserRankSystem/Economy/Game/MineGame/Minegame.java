package UserRankSystem.Economy.Game.MineGame;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Minegame extends Command {
    public Minegame() {
        this.name = "mine";
        this.guildOnly = true;
        this.cooldown = 120;
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            if (!(new File("/home/" + cfg.homedirectory() + "/GameBotDiscord/MineGame/" + event.getMember().getId() + ".json").exists())) {
                try {
                    File file = new File("/home/" + cfg.homedirectory() + "/GameBotDiscord/MineGame/" + event.getMember().getId() + ".json");
                    file.createNewFile();
                    ObjectMapper mapper = new ObjectMapper();
                    Minegameinfo minegameinfo = new Minegameinfo(0);
                    mapper.writeValue(file, minegameinfo);
                    event.getMessage().reply("**Информация о вас создана, введите команду ещё раз!**").queue();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                File file = new File("/home/" + cfg.homedirectory() + "/GameBotDiscord/MineGame/" + event.getMember().getId() + ".json");
                File file1 = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                ObjectMapper mapper = new ObjectMapper();
                try {
                    Minegameinfo minegameinfo = mapper.readValue(file, Minegameinfo.class);
                    MessageRankInfo messageRankInfo = mapper.readValue(file1, MessageRankInfo.class);
                    if (minegameinfo.getPickaxe() == 0) {
                        if (messageRankInfo.getMoney() >= 100) {
                            int a = 50;
                            int b = 150;
                            int diff = b - a;
                            Random random = new Random();
                            int newMoney = random.nextInt(diff + 1) + a;                            messageRankInfo.setMoney(messageRankInfo.getMoney() - 100 + newMoney);
                            mapper.writeValue(file1, messageRankInfo);
                            event.getMessage().reply("**Вы выкопали ресурсов на** `" + newMoney + "` **монет!**").queue();
                        } else {
                            event.getMessage().reply("**У вас недостаточно средств для начала игры! Требуется минимум 100 монет**").queue();
                        }
                    } else if (minegameinfo.getPickaxe() == 1) {
                        if (messageRankInfo.getMoney() >= 1000) {
                            int a = 800;
                            int b = 1300;
                            int diff = b - a;
                            Random random = new Random();
                            int newMoney = random.nextInt(diff + 1) + a;                            messageRankInfo.setMoney(messageRankInfo.getMoney() - 1000 + newMoney);
                            mapper.writeValue(file1, messageRankInfo);
                            event.getMessage().reply("**Вы выкопали ресурсов на** `" + newMoney + "` **монет!**").queue();
                        } else {
                            event.getMessage().reply("**У вас недостаточно средств для начала игры! Требуется минимум 1000 монет**").queue();
                        }
                    } else if (minegameinfo.getPickaxe() == 2) {
                        if (messageRankInfo.getMoney() >= 9000) {
                            int a = 8000;
                            int b = 9700;
                            int diff = b - a;
                            Random random = new Random();
                            int newMoney = random.nextInt(diff + 1) + a;
                            messageRankInfo.setMoney(messageRankInfo.getMoney() - 9000 + newMoney);
                            mapper.writeValue(file1, messageRankInfo);
                            event.getMessage().reply("**Вы выкопали ресурсов на** `" + newMoney + "` **монет!**").queue();
                        } else {
                            event.getMessage().reply("**У вас недостаточно средств для начала игры! Требуется минимум 9000 монет**").queue();
                        }
                    } else if (minegameinfo.getPickaxe() == 3) {
                        if (messageRankInfo.getMoney() >= 15000) {
                            int a = 14000;
                            int b = 15000;
                            int diff = b - a;
                            Random random = new Random();
                            int newMoney = random.nextInt(diff + 1) + a;
                            messageRankInfo.setMoney(messageRankInfo.getMoney() - 15000 + newMoney);
                            mapper.writeValue(file1, messageRankInfo);
                            event.getMessage().reply("**Вы выкопали ресурсов на** `" + newMoney + "` **монет!**").queue();
                        } else {
                            event.getMessage().reply("**У вас недостаточно средств для начала игры! Требуется минимум 15000 монет**").queue();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Mine";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
