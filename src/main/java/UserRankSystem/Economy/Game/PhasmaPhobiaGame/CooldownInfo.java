package UserRankSystem.Economy.Game.PhasmaPhobiaGame;

import java.util.Date;

public class CooldownInfo {
    private long time;

    public CooldownInfo(){

    }

    public void setTime(long time) {
        this.time = time;
    }

    public CooldownInfo(final Date date){

        this.time = date.getTime();
    }
    public long getTime(){return time;}
}
