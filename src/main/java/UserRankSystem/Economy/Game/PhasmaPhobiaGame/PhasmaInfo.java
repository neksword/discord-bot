package UserRankSystem.Economy.Game.PhasmaPhobiaGame;

public class PhasmaInfo {
    private int room;
    private int houseroommax;
    private int ghostroom;
    private boolean roomyes;
    private boolean EMF;
    private boolean thermometer;
    private boolean scattereditems;
    private boolean notepad;
    private boolean talk;
    private int evidence;
    private boolean hunting;

    public PhasmaInfo(){

    }

    public void setGhostroom(int ghostroom) {
        this.ghostroom = ghostroom;
    }

    public void setRoom(int userroom) {
        this.room = userroom;
    }

    public int getHouseroommax() {
        return houseroommax;
    }

    public void setHouseroommax(int houseroommax) {
        this.houseroommax = houseroommax;
    }

    public int getGhostroom() {
        return ghostroom;
    }

    public int getRoom() {
        return room;
    }

    public boolean getRoomyes() {
        return roomyes;
    }

    public void setRoomyes(boolean roomyes) {
        this.roomyes = roomyes;
    }

    public boolean isRoomyes() {
        return roomyes;
    }

    public boolean isEMF() {
        return EMF;
    }

    public void setEMF(boolean EMF) {
        this.EMF = EMF;
    }

    public boolean isThermometer() {
        return thermometer;
    }

    public void setThermometer(boolean thermometer) {
        this.thermometer = thermometer;
    }

    public boolean isScattereditems() {
        return scattereditems;
    }

    public void setScattereditems(boolean scattereditems) {
        this.scattereditems = scattereditems;
    }

    public boolean isNotepad() {
        return notepad;
    }

    public void setNotepad(boolean notepad) {
        this.notepad = notepad;
    }

    public boolean isTalk() {
        return talk;
    }

    public void setTalk(boolean talk) {
        this.talk = talk;
    }

    public int getEvidence() {
        return evidence;
    }

    public void setEvidence(int evidence) {
        this.evidence = evidence;
    }

    public boolean isHunting() {
        return hunting;
    }

    public void setHunting(boolean hunting) {
        this.hunting = hunting;
    }

    public PhasmaInfo(int userroom, int houseroom, int ghostroom, boolean roomyes, boolean EMF, boolean thermometer, boolean scattereditems, boolean notepad, boolean talk, int evidence, boolean hunting){
        this.room = userroom;
        this.houseroommax = houseroom;
        this.ghostroom = ghostroom;
        this.roomyes = roomyes;
        this.EMF = EMF;
        this.thermometer = thermometer;
        this.scattereditems = scattereditems;
        this.notepad = notepad;
        this.talk = talk;
        this.evidence = evidence;
        this.hunting = hunting;
    }
}
