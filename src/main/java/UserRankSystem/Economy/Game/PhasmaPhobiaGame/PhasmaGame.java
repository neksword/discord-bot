package UserRankSystem.Economy.Game.PhasmaPhobiaGame;

import BotConfig.BotConfig;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.*;

public class PhasmaGame extends ListenerAdapter {
    private final Map<String, PhasmaInfo> phasmaCache = new ConcurrentHashMap<>();
    private final Map<String, CooldownInfo> cooldawnCache = new ConcurrentHashMap<>();
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(4);
    BotConfig cfg = ConfigFactory.create(BotConfig.class);

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        if (command[0].equalsIgnoreCase(cfg.prefix() + "phasma")) {
            final UsedChannel usedChannel = new UsedChannel();
            if (usedChannel.Return(event.getMember(), event.getChannel(), event.getMessage())) {
                Member member = event.getMember();
                if (!phasmaCache.containsKey(member.getId())) {
                    long time = new Date().getTime();
                    if (cooldawnCache.get(member.getId()) == null) {
                        final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                        cooldawnCache.put(member.getId(), cooldownInfo);
                        cooldawnCache.get(member.getId()).setTime(new Date().getTime() - 70000);
                    }
                    if (time > (cooldawnCache.get(member.getId()).getTime() + 60000)) {
                        cooldawnCache.remove(member.getId());
                        if (command.length == 1) {
                            ObjectMapper mapper = new ObjectMapper();
                            File memberFile = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + member.getId() + ".json");
                            try {
                                final MessageRankInfo messageRankInfoMember = mapper.readValue(memberFile, MessageRankInfo.class);
                                if (messageRankInfoMember.getMoney() < 10000) {
                                    event.getMessage().reply("**У вас недостаточно средств! Для начала игры нужно `10000` монет.**").queue();
                                } else {
                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() - 10000);
                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                    Message message = event.getMessage();
                                    final PhasmaInfo phasmaInfo = new PhasmaInfo();
                                    phasmaCache.put(member.getId(), phasmaInfo);
                                    int min = 8;
                                    int max = 10;
                                    int diff = max - min;
                                    Random random = new Random();
                                    int houseroom = random.nextInt(diff + 1) + min;
                                    int min1 = 1;
                                    int diff1 = houseroom - min1;
                                    Random random1 = new Random();
                                    int ghostroom = random1.nextInt(diff1 + 1) + min1;
                                    phasmaCache.get(member.getId()).setHouseroommax(houseroom);
                                    phasmaCache.get(member.getId()).setGhostroom(ghostroom);
                                    System.out.println(ghostroom);
                                    System.out.println(houseroom);
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor(event.getAuthor().getName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
                                    builder.setTitle("Добро пожаловать в PhasmaPhobia!");
                                    builder.addField("Команды:", "В вашем доме - `" + phasmaCache.get(member.getId()).getHouseroommax() + "` комнат.\n" + "Для начала игры выберите комнату - " + cfg.prefix() + "phasma room (номер).\n" + "Просмотреть ваши улики - " + cfg.prefix() + "phasma evidence.\n" + "Проверить блокнот - " + cfg.prefix() + "phasma notepad.\n" + "Проверить радиоприёмник - " + cfg.prefix() + "phasma talk.\n" + "Проверить детектор ЭМП - " + cfg.prefix() + "phasma EMF.\n" + "Проверить температуру - " + cfg.prefix() + "phasma temp.\n", false);
                                    Color purple = new Color(76, 0, 230);
                                    builder.setColor(purple);
                                    event.getMessage().reply(builder.build()).queue();
                                    Runnable runnable = () -> {
                                        if (phasmaCache.containsKey(member.getId())) {
                                            event.getMessage().reply("**!!Внимание началась ОХОТА!!**").queue();
                                            phasmaCache.get(member.getId()).setHunting(true);
                                            Runnable runnable3 = () -> {
                                                Random random2 = new Random();
                                                if (random2.nextBoolean()) {
                                                    event.getMessage().reply("**Вы смогли выжить! У вас была благовония!**").queue();
                                                    phasmaCache.get(member.getId()).setHunting(false);
                                                } else {
                                                    event.getMessage().reply("**Вас догнали! Был слышен хруст вашего позвоночника, ваша плоть была отделена от костей...**").queue();
                                                    phasmaCache.remove(member.getId());
                                                }
                                            };
                                            ScheduledFuture<?> scheduledFuture3 = scheduler.schedule(runnable3, 10, TimeUnit.SECONDS);
                                        }
                                    };
                                    Runnable runnable1 = () -> {
                                        if (phasmaCache.containsKey(member.getId())) {
                                            ScheduledFuture<?> scheduledFuture = scheduler.schedule(runnable, 1, TimeUnit.MINUTES);
                                            event.getMessage().reply("**Охота начнётся через 1 минуту!**").queue();
                                        }
                                    };
                                    ScheduledFuture<?> scheduledFuture1 = scheduler.schedule(runnable1, 30, TimeUnit.SECONDS);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        event.getMessage().reply("У вас не прошёл кулдаун! Следующую игру можно начать через - `" + (int) (60000 - (time - cooldawnCache.get(member.getId()).getTime())) / 1000 + "` секунд.").queue();
                    }
                } else {
                    try {
                        ObjectMapper mapper = new ObjectMapper();
                        File memberFile = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + member.getId() + ".json");
                        final MessageRankInfo messageRankInfoMember = mapper.readValue(memberFile, MessageRankInfo.class);
                        if (command.length == 1) {
                            event.getMessage().reply("**Вы уже играете!**").queue();
                        } else {
                            if (command[1].equalsIgnoreCase("room")) {
                                if (!phasmaCache.get(member.getId()).getRoomyes()) {
                                    if (command.length < 3) {
                                        event.getMessage().reply("**Укажите номер комнаты!**").queue();
                                    } else {
                                        try {
                                            int room = Integer.parseInt(command[2].trim());
                                            System.out.println(phasmaCache.get(member.getId()).getHouseroommax());
                                            if (room < 0 || room > phasmaCache.get(member.getId()).getHouseroommax()) {
                                                event.getMessage().reply("**Укажите существующий номер комнаты!**").queue();
                                            } else {
                                                phasmaCache.get(member.getId()).setRoom(room);
                                                if (room == phasmaCache.get(member.getId()).getGhostroom()) {
                                                    phasmaCache.get(member.getId()).setRoomyes(true);
                                                    int min = 1;
                                                    int max = 3;
                                                    int diff = max - min;
                                                    Random random = new Random();
                                                    int sensor = random.nextInt(diff + 1) + min;
                                                    if (sensor == 1) {
                                                        int min1 = 2;
                                                        int max1 = 5;
                                                        int diff1 = max1 - min1;
                                                        Random random1 = new Random();
                                                        int sensor1 = random1.nextInt(diff1 + 1) + min1;
                                                        event.getMessage().reply("**!!Внимание!! Сработал детектор ЭМП, уровень - ** `" + sensor1 + "`.").queue();
                                                        if (sensor1 == 5) {
                                                            phasmaCache.get(member.getId()).setEMF(true);
                                                            phasmaCache.get(member.getId()).setEvidence(1);
                                                            System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        }
                                                    } else if (sensor == 2) {
                                                        event.getMessage().reply("**!!Внимание!! Сработал термометр, температура отрицательна!**").queue();
                                                        phasmaCache.get(member.getId()).setThermometer(true);
                                                        phasmaCache.get(member.getId()).setEvidence(1);
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                    } else if (sensor == 3) {
                                                        event.getMessage().reply("**!!Внимание!! В этой комнате раскиданы вещи, призрак здесь!**").queue();
                                                        phasmaCache.get(member.getId()).setScattereditems(true);
                                                        phasmaCache.get(member.getId()).setEvidence(1);
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                    }
                                                } else {
                                                    event.getMessage().reply("**Детекторы молчат, комната пуста.**").queue();
                                                }
                                            }
                                        } catch (NumberFormatException nfe) {
                                            event.getMessage().reply("**Укажите число!**").queue();
                                        }
                                    }
                                } else {
                                    event.getMessage().reply("**Вы уже нашли комнату с призраком!**").queue();
                                }
                            } else if (command[1].equalsIgnoreCase("evidence")) {
                                if (phasmaCache.get(member.getId()).isHunting()) {
                                    event.getMessage().reply("**Идёт охота!**").queue();
                                } else {
                                    if (phasmaCache.get(member.getId()).getRoomyes()) {
                                        EmbedBuilder builder = new EmbedBuilder();
                                        builder.setAuthor(event.getAuthor().getName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
                                        builder.setTitle("Ваши улики:");
                                        PhasmaInfo phasmaInfo = phasmaCache.get(member.getId());
                                        builder.addField("Если стоит true - улика засчитана!", "ЭМП #5 - " + phasmaInfo.isEMF() + "\n" + "Термометр - " + phasmaInfo.isThermometer() + "\n" + "Раскиданные вещи - " + phasmaInfo.isScattereditems() + "\n" + "Блокнот - " + phasmaInfo.isNotepad() + "\n" + "Радиоприёмник - " + phasmaInfo.isTalk(), false);
                                        builder.setFooter("PhasmaPhobia");
                                        Color purple = new Color(76, 0, 230);
                                        builder.setColor(purple);
                                        event.getMessage().reply(builder.build()).queue();
                                    } else {
                                        event.getMessage().reply("**Вы ещё не нашли комнату!**").queue();
                                    }
                                }
                            } else if (command[1].equalsIgnoreCase("notepad")) {
                                if (phasmaCache.get(member.getId()).isHunting()) {
                                    event.getMessage().reply("**Идёт охота!**").queue();
                                } else {
                                    if (phasmaCache.get(member.getId()).getRoomyes()) {
                                        if (phasmaCache.get(member.getId()).isNotepad()) {
                                            event.getMessage().reply("**Записи уже есть!**").queue();
                                        } else {
                                            Message message = event.getMessage().reply("**Подождите `10` секунд!**").complete();
                                            Runnable runnable = () -> {
                                                Random random = new Random();
                                                if (random.nextBoolean()) {
                                                    phasmaCache.get(member.getId()).setNotepad(true);
                                                    message.editMessage("**Записи есть!**").queue();
                                                    System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                    phasmaCache.get(member.getId()).setEvidence(phasmaCache.get(member.getId()).getEvidence() + 1);
                                                    System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                    if (phasmaCache.get(member.getId()).getEvidence() >= 3) {
                                                        PhasmaInfo phasmaInfo = phasmaCache.get(member.getId());
                                                        try {
                                                            if (phasmaInfo.isEMF() && phasmaInfo.isNotepad() && phasmaInfo.isScattereditems()) {
                                                                phasmaCache.remove(member.getId());
                                                                messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                mapper.writeValue(memberFile, messageRankInfoMember);
                                                                event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, записи в блокноте, разбросанные вещи. Призрак - Ревенант.**").queue();
                                                                final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                cooldawnCache.put(member.getId(), cooldownInfo);
                                                            } else if (phasmaInfo.isThermometer() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                phasmaCache.remove(member.getId());
                                                                messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                mapper.writeValue(memberFile, messageRankInfoMember);
                                                                event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - отрицательная температура, радиоприёмник, записи в блокноте. Призрак - Они.**").queue();
                                                                final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                cooldawnCache.put(member.getId(), cooldownInfo);
                                                            } else if (phasmaInfo.isEMF() && phasmaInfo.isThermometer() && phasmaInfo.isTalk()) {
                                                                phasmaCache.remove(member.getId());
                                                                messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                mapper.writeValue(memberFile, messageRankInfoMember);
                                                                event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, отрицательная температура, радиоприёмник. Призрак - Банши.**").queue();
                                                                final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                cooldawnCache.put(member.getId(), cooldownInfo);
                                                            } else if (phasmaInfo.isScattereditems() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                phasmaCache.remove(member.getId());
                                                                messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                mapper.writeValue(memberFile, messageRankInfoMember);
                                                                event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - радиоприёмник, записи в блокноте, разбросанные вещи. Призрак - Мираж.**").queue();
                                                                final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                cooldawnCache.put(member.getId(), cooldownInfo);
                                                            } else {
                                                                phasmaCache.remove(member.getId());
                                                                messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                mapper.writeValue(memberFile, messageRankInfoMember);
                                                                event.getMessage().reply("**Вы смогли выжить, но с вашими уликами не получается узнать призрака! Похоже вы открыли новый вид!**").queue();
                                                                final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                cooldawnCache.put(member.getId(), cooldownInfo);
                                                            }
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else {
                                                    message.editMessage("**Записей нет!**").queue();
                                                }
                                            };
                                            ScheduledFuture<?> scheduledFuture = scheduler.schedule(runnable, 10, TimeUnit.SECONDS);
                                        }
                                    } else {
                                        event.getMessage().reply("**Вы ещё не нашли комнату!**").queue();
                                    }
                                }
                            } else if (command[1].equalsIgnoreCase("talk")) {
                                if (phasmaCache.get(member.getId()).isHunting()) {
                                    event.getMessage().reply("**Идёт охота!**").queue();
                                } else {
                                    if (phasmaCache.get(member.getId()).isRoomyes()) {
                                        if (phasmaCache.get(member.getId()).isTalk()) {
                                            event.getMessage().reply("**Призрак уже разговаривал!**").queue();
                                        } else {
                                            if (command.length <= 2) {
                                                event.getMessage().reply("**Вы забыли обратиться к призраку!**").queue();
                                            } else {
                                                final StringBuilder stringBuilder = new StringBuilder();
                                                for (int i = 2; i < command.length; i++) {
                                                    stringBuilder.append(command[i]).append(" ");
                                                }
                                                System.out.println(stringBuilder.toString());
                                                Random random = new Random();
                                                if (stringBuilder.toString().equalsIgnoreCase("Сколько тебе лет? ")) {
                                                    if (random.nextBoolean()) {
                                                        phasmaCache.get(member.getId()).setTalk(true);
                                                        int min = 1;
                                                        int max = 3;
                                                        int diff = max - min;
                                                        int answer = random.nextInt(diff + 1) + min;
                                                        String[] age = new String[4];
                                                        age[1] = "**Я старше чем ты думаешь...**";
                                                        age[2] = "**Как младенцу.**";
                                                        age[3] = "**Почти как тебе.**";
                                                        event.getMessage().reply(age[answer]).queue();
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        phasmaCache.get(member.getId()).setEvidence(phasmaCache.get(member.getId()).getEvidence() + 1);
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        if (phasmaCache.get(member.getId()).getEvidence() >= 3) {
                                                            PhasmaInfo phasmaInfo = phasmaCache.get(member.getId());
                                                            try {
                                                                if (phasmaInfo.isEMF() && phasmaInfo.isNotepad() && phasmaInfo.isScattereditems()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, записи в блокноте, разбросанные вещи. Призрак - Ревенант.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isThermometer() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - отрицательная температура, радиоприёмник, записи в блокноте. Призрак - Они.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isEMF() && phasmaInfo.isThermometer() && phasmaInfo.isTalk()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, отрицательная температура, радиоприёмник. Призрак - Банши.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isScattereditems() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - радиоприёмник, записи в блокноте, разбросанные вещи. Призрак - Мираж.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить, но с вашими уликами не получается узнать призрака! Похоже вы открыли новый вид!**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                }
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else {
                                                        event.getMessage().reply("**Нет ответа...**").queue();
                                                    }
                                                } else if (stringBuilder.toString().equalsIgnoreCase("Как тебя зовут? ")) {
                                                    if (random.nextBoolean()) {
                                                        phasmaCache.get(member.getId()).setTalk(true);
                                                        event.getMessage().reply("**Как саму смерть.**").queue();
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        phasmaCache.get(member.getId()).setEvidence(phasmaCache.get(member.getId()).getEvidence() + 1);
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        if (phasmaCache.get(member.getId()).getEvidence() >= 3) {
                                                            PhasmaInfo phasmaInfo = phasmaCache.get(member.getId());
                                                            try {
                                                                if (phasmaInfo.isEMF() && phasmaInfo.isNotepad() && phasmaInfo.isScattereditems()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, записи в блокноте, разбросанные вещи. Призрак - Ревенант.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isThermometer() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - отрицательная температура, радиоприёмник, записи в блокноте. Призрак - Они.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isEMF() && phasmaInfo.isThermometer() && phasmaInfo.isTalk()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, отрицательная температура, радиоприёмник. Призрак - Банши.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isScattereditems() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - радиоприёмник, записи в блокноте, разбросанные вещи. Призрак - Мираж.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить, но с вашими уликами не получается узнать призрака! Похоже вы открыли новый вид!**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                }
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else {
                                                        event.getMessage().reply("**Нет ответа...**").queue();
                                                    }
                                                } else if (stringBuilder.toString().equalsIgnoreCase("Ты злой? ")) {
                                                    if (random.nextBoolean()) {
                                                        phasmaCache.get(member.getId()).setTalk(true);
                                                        event.getMessage().reply("**Твой позвоночник скоро треснет...**").queue();
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        phasmaCache.get(member.getId()).setEvidence(phasmaCache.get(member.getId()).getEvidence() + 1);
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        if (phasmaCache.get(member.getId()).getEvidence() >= 3) {
                                                            PhasmaInfo phasmaInfo = phasmaCache.get(member.getId());
                                                            try {
                                                                if (phasmaInfo.isEMF() && phasmaInfo.isNotepad() && phasmaInfo.isScattereditems()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, записи в блокноте, разбросанные вещи. Призрак - Ревенант.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isThermometer() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - отрицательная температура, радиоприёмник, записи в блокноте. Призрак - Они.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isEMF() && phasmaInfo.isThermometer() && phasmaInfo.isTalk()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, отрицательная температура, радиоприёмник. Призрак - Банши.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isScattereditems() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - радиоприёмник, записи в блокноте, разбросанные вещи. Призрак - Мираж.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить, но с вашими уликами не получается узнать призрака! Похоже вы открыли новый вид!**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                }
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else {
                                                        event.getMessage().reply("**Нет ответа...**").queue();
                                                    }
                                                } else if (stringBuilder.toString().equalsIgnoreCase("Ты здесь? ")) {
                                                    if (random.nextBoolean()) {
                                                        phasmaCache.get(member.getId()).setTalk(true);
                                                        event.getMessage().reply("**Я у тебя за спиной.**").queue();
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        phasmaCache.get(member.getId()).setEvidence(phasmaCache.get(member.getId()).getEvidence() + 1);
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        if (phasmaCache.get(member.getId()).getEvidence() >= 3) {
                                                            PhasmaInfo phasmaInfo = phasmaCache.get(member.getId());
                                                            try {
                                                                if (phasmaInfo.isEMF() && phasmaInfo.isNotepad() && phasmaInfo.isScattereditems()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, записи в блокноте, разбросанные вещи. Призрак - Ревенант.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isThermometer() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - отрицательная температура, радиоприёмник, записи в блокноте. Призрак - Они.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isEMF() && phasmaInfo.isThermometer() && phasmaInfo.isTalk()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, отрицательная температура, радиоприёмник. Призрак - Банши.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isScattereditems() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - радиоприёмник, записи в блокноте, разбросанные вещи. Призрак - Мираж.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить, но с вашими уликами не получается узнать призрака! Похоже вы открыли новый вид!**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                }
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else {
                                                        event.getMessage().reply("**Нет ответа...**").queue();
                                                    }
                                                } else {
                                                    if (random.nextBoolean()) {
                                                        phasmaCache.get(member.getId()).setTalk(true);
                                                        event.getMessage().reply("_Дыхнул на ухо._").queue();
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        phasmaCache.get(member.getId()).setEvidence(phasmaCache.get(member.getId()).getEvidence() + 1);
                                                        System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                        if (phasmaCache.get(member.getId()).getEvidence() >= 3) {
                                                            PhasmaInfo phasmaInfo = phasmaCache.get(member.getId());
                                                            try {
                                                                if (phasmaInfo.isEMF() && phasmaInfo.isNotepad() && phasmaInfo.isScattereditems()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, записи в блокноте, разбросанные вещи. Призрак - Ревенант.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isThermometer() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - отрицательная температура, радиоприёмник, записи в блокноте. Призрак - Они.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isEMF() && phasmaInfo.isThermometer() && phasmaInfo.isTalk()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, отрицательная температура, радиоприёмник. Призрак - Банши.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else if (phasmaInfo.isScattereditems() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - радиоприёмник, записи в блокноте, разбросанные вещи. Призрак - Мираж.**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                } else {
                                                                    phasmaCache.remove(member.getId());
                                                                    messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                                    mapper.writeValue(memberFile, messageRankInfoMember);
                                                                    event.getMessage().reply("**Вы смогли выжить, но с вашими уликами не получается узнать призрака! Похоже вы открыли новый вид!**").queue();
                                                                    final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                                    cooldawnCache.put(member.getId(), cooldownInfo);
                                                                }
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else {
                                                        event.getMessage().reply("**Нет ответа...**").queue();
                                                    }

                                                }
                                            }
                                        }
                                    } else {
                                        event.getMessage().reply("**Вы ещё не нашли комнату!**").queue();
                                    }
                                }
                            } else if (command[1].equalsIgnoreCase("emf")) {
                                if (phasmaCache.get(member.getId()).isHunting()) {
                                    event.getMessage().reply("**Идёт охота!**").queue();
                                } else {
                                    if (phasmaCache.get(member.getId()).isRoomyes()) {
                                        if (!(phasmaCache.get(member.getId()).isEMF())) {
                                            Random random = new Random();
                                            int min = 2;
                                            int max = 5;
                                            int diff = max - min;
                                            int sensor = random.nextInt(diff + 1) + min;
                                            if (sensor == 5) {
                                                phasmaCache.get(member.getId()).setEMF(true);
                                                event.getMessage().reply("**!!Внимание!! Детектор ЭМП показал `5` уровень!**").queue();
                                                System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                phasmaCache.get(member.getId()).setEvidence(phasmaCache.get(member.getId()).getEvidence() + 1);
                                                System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                if (phasmaCache.get(member.getId()).getEvidence() >= 3) {
                                                    PhasmaInfo phasmaInfo = phasmaCache.get(member.getId());
                                                    try {
                                                        if (phasmaInfo.isEMF() && phasmaInfo.isNotepad() && phasmaInfo.isScattereditems()) {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, записи в блокноте, разбросанные вещи. Призрак - Ревенант.**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        } else if (phasmaInfo.isThermometer() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - отрицательная температура, радиоприёмник, записи в блокноте. Призрак - Они.**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        } else if (phasmaInfo.isEMF() && phasmaInfo.isThermometer() && phasmaInfo.isTalk()) {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, отрицательная температура, радиоприёмник. Призрак - Банши.**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        } else if (phasmaInfo.isScattereditems() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - радиоприёмник, записи в блокноте, разбросанные вещи. Призрак - Мираж.**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        } else {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить, но с вашими уликами не получается узнать призрака! Похоже вы открыли новый вид!**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        }
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            } else {
                                                event.getMessage().reply("**Уровень - `" + sensor + "`**").queue();
                                            }
                                        } else {
                                            event.getMessage().reply("**Уровень ЭМП уже равен `5`.**").queue();
                                        }
                                    } else {
                                        event.getMessage().reply("**Вы ещё не нашли комнату!**").queue();
                                    }
                                }
                            } else if (command[1].equalsIgnoreCase("temp")) {
                                if (phasmaCache.get(member.getId()).isHunting()) {
                                    event.getMessage().reply("**Идёт охота!**").queue();
                                } else {
                                    if (phasmaCache.get(member.getId()).isRoomyes()) {
                                        if (!(phasmaCache.get(member.getId()).isThermometer())) {
                                            Random random = new Random();
                                            if (random.nextBoolean()) {
                                                phasmaCache.get(member.getId()).setThermometer(true);
                                                event.getMessage().reply("**!!Внимание!! Температура отрицательна!**").queue();
                                                System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                phasmaCache.get(member.getId()).setEvidence(phasmaCache.get(member.getId()).getEvidence() + 1);
                                                System.out.println(phasmaCache.get(member.getId()).getEvidence());
                                                if (phasmaCache.get(member.getId()).getEvidence() >= 3) {
                                                    PhasmaInfo phasmaInfo = phasmaCache.get(member.getId());
                                                    try {
                                                        if (phasmaInfo.isEMF() && phasmaInfo.isNotepad() && phasmaInfo.isScattereditems()) {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, записи в блокноте, разбросанные вещи. Призрак - Ревенант.**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        } else if (phasmaInfo.isThermometer() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - отрицательная температура, радиоприёмник, записи в блокноте. Призрак - Они.**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        } else if (phasmaInfo.isEMF() && phasmaInfo.isThermometer() && phasmaInfo.isTalk()) {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - ЭПМ #5, отрицательная температура, радиоприёмник. Призрак - Банши.**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        } else if (phasmaInfo.isScattereditems() && phasmaInfo.isTalk() && phasmaInfo.isNotepad()) {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить и успешно определили тип призрака! Улики - радиоприёмник, записи в блокноте, разбросанные вещи. Призрак - Мираж.**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        } else {
                                                            phasmaCache.remove(member.getId());
                                                            messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() + 20000);
                                                            mapper.writeValue(memberFile, messageRankInfoMember);
                                                            event.getMessage().reply("**Вы смогли выжить, но с вашими уликами не получается узнать призрака! Похоже вы открыли новый вид!**").queue();
                                                            final CooldownInfo cooldownInfo = new CooldownInfo(new Date());
                                                            cooldawnCache.put(member.getId(), cooldownInfo);
                                                        }
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            } else {
                                                event.getMessage().reply("**Температура положительна!**").queue();
                                            }
                                        } else {
                                            event.getMessage().reply("**Температура уже отрицательна!**").queue();
                                        }
                                    } else {
                                        event.getMessage().reply("**Вы ещё не нашли комнату!**").queue();
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
