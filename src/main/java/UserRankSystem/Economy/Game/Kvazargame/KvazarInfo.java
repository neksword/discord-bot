package UserRankSystem.Economy.Game.Kvazargame;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

public class KvazarInfo {
    private Member member;
    private Message message;
    private TextChannel channel;
    private int stavka;
    private int score;

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Message getMessage() {

        return message;
    }

    public TextChannel getChannel() {
        return channel;
    }

    public void setChannel(TextChannel channel) {
        this.channel = channel;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public int getStavka() {
        return stavka;
    }

    public void setStavka(int stavka) {
        this.stavka = stavka;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public KvazarInfo(final Message message, final Member member, final TextChannel channel, final int stavka, final int score){
        this.member = member;
        this.message = message;
        this.channel = channel;
        this.stavka = stavka;
        this.score = score;
    }
}
