package UserRankSystem.Economy.Game.Kvazargame;

import java.util.Date;

public class CooldownInfoKvazar {
    private long time;

    public CooldownInfoKvazar(){

    }

    public void setTime(long time) {
        this.time = time;
    }

    public CooldownInfoKvazar(final Date date){

        this.time = date.getTime();
    }
    public long getTime(){return time;}
}
