package UserRankSystem.Economy.Game.Kvazargame;

import BotConfig.BotConfig;
import UserRankSystem.Economy.Game.PhasmaPhobiaGame.CooldownInfo;
import UserRankSystem.Economy.Game.PhasmaPhobiaGame.PhasmaInfo;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Kvazar extends ListenerAdapter {
    private final Map<String, KvazarInfo> kvazarCache = new ConcurrentHashMap<>();
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(4);
    private final Map<String, CooldownInfoKvazar> cooldawnCache = new ConcurrentHashMap<>();
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            if (!(new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json").exists())) {
                File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                file.createNewFile();
                final MessageRankInfo rankInfo = new MessageRankInfo(event.getMember(), 1, 1, 15, 1, 1,0,0,0,0);
                mapper.writeValue(file, rankInfo);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] command = event.getMessage().getContentRaw().split("\\s");
        if (command[0].equalsIgnoreCase(cfg.prefix() + "kvazar")) {
            final UsedChannel usedChannel = new UsedChannel();
            if (usedChannel.Return(event.getMember(), event.getChannel(), event.getMessage())) {
                if (kvazarCache.get(event.getMember().getId()) == null) {
                    Member member = event.getMember();
                    long time = new Date().getTime();
                    if (cooldawnCache.get(member.getId()) == null) {
                        final CooldownInfoKvazar cooldownInfo = new CooldownInfoKvazar(new Date());
                        cooldawnCache.put(member.getId(), cooldownInfo);
                        cooldawnCache.get(member.getId()).setTime(new Date().getTime() - 70000);
                    }
                    if (command.length < 2) {
                        event.getMessage().reply("**Укажите ставку!**").queue();
                    } else {
                        if (time > (cooldawnCache.get(member.getId()).getTime() + 60000)) {
                            cooldawnCache.remove(member.getId());
                            try {
                                int stavka = Integer.parseInt(command[1].trim());
                                if (stavka < 99) {
                                    event.getMessage().reply("**Укажите ставку побольше!**").queue();
                                } else {
                                    ObjectMapper mapper = new ObjectMapper();
                                    File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                                    final MessageRankInfo messageRankInfo = mapper.readValue(file, MessageRankInfo.class);
                                    if (stavka > messageRankInfo.getMoney()) {
                                        event.getMessage().reply("**У вас недостаточно монет!**").queue();
                                    } else {
                                        Color purple = new Color(76, 0, 230);
                                        int min = 1;
                                        int max = 16;
                                        int diff = max - min;
                                        Random random = new Random();
                                        int score = random.nextInt(diff + 1) + min;
                                        EmbedBuilder builder = new EmbedBuilder();
                                        builder.setColor(purple);
                                        builder.setAuthor(event.getAuthor().getName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
                                        builder.setTitle("Квазар");
                                        builder.setColor(Color.BLUE);
                                        builder.setDescription("Выбирите действие \n one - [1 - 8] \n two - [4 - 7] \n close - закончить игру");
                                        builder.addField("Твой счёт", String.valueOf(score), true);
                                        Message message = event.getMessage().reply(builder.build()).complete();
                                        final KvazarInfo kvazarInfo = new KvazarInfo(message, event.getMember(), event.getChannel(), stavka, 0);
                                        messageRankInfo.setMoney(messageRankInfo.getMoney() - stavka);
                                        mapper.writeValue(file, messageRankInfo);
                                        kvazarCache.put(event.getMember().getId(), kvazarInfo);
                                        kvazarCache.get(event.getMember().getId()).setScore(score);
                                        Runnable runnable = () -> {
                                            if (kvazarCache.get(event.getMember().getId()) == null) {
                                            } else {
                                                kvazarCache.remove(event.getMember().getId());
                                                EmbedBuilder embedBuilder = new EmbedBuilder(message.getEmbeds().get(0));
                                                embedBuilder.setDescription("Время вышло!");
                                                embedBuilder.setColor(Color.RED);
                                                message.editMessage(embedBuilder.build()).queue();
                                            }
                                        };
                                        ScheduledFuture<?> scheduledFuture = scheduler.schedule(runnable, 1, TimeUnit.MINUTES);
                                    }
                                }
                            } catch (NumberFormatException | IOException e) {
                                e.printStackTrace();
                            }

                        } else {
                            event.getMessage().reply("У вас не прошёл кулдаун! Следующую игру можно начать через - `" + (int) (60000 - (time - cooldawnCache.get(member.getId()).getTime())) / 1000 + "` секунд.").queue();
                        }
                    }
                } else {
                    event.getMessage().reply("**Игра уже идёт!**").queue();
                }
            }
        } else if (command[0].equalsIgnoreCase("one")) {
            if (kvazarCache.get(event.getMember().getId()) != null) {
                if (kvazarCache.get(event.getMember().getId()).getChannel().getId().equals(event.getChannel().getId())) {
                    final KvazarInfo kvazarInfo = kvazarCache.get(event.getMember().getId());
                    TextChannel channel = kvazarInfo.getChannel();
                    Message message = channel.retrieveMessageById(kvazarInfo.getMessage().getId()).complete();
                    EmbedBuilder builder = new EmbedBuilder(message.getEmbeds().get(0));
                    if (!builder.getFields().isEmpty()) {
                        int user = kvazarInfo.getScore();
                        builder.clearFields();
                        int min = 1;
                        int max = 8;
                        int diff = max - min;
                        Random random = new Random();
                        int score = random.nextInt(diff + 1) + min;
                        if ((user + score) > 20){
                            builder.addField("Твой счёт", String.valueOf(user + score),true);
                            builder.setDescription("Вы проиграли!");
                            builder.setColor(Color.RED);
                            kvazarCache.remove(event.getMember().getId());
                            final CooldownInfoKvazar cooldownInfo = new CooldownInfoKvazar(new Date());
                            cooldawnCache.put(event.getMember().getId(), cooldownInfo);
                        }
                        else {
                            builder.addField("Твой счёт", String.valueOf(user + score), true);
                            kvazarInfo.setScore(user + score);
                        }
                        message.editMessage(builder.build()).queue();
                        event.getMessage().delete().queue();
                    } else {
                        int user = kvazarInfo.getScore();
                        builder.clearFields();
                        int min = 1;
                        int max = 8;
                        int diff = max - min;
                        Random random = new Random();
                        int score = random.nextInt(diff + 1) + min;
                        builder.addField("Твой счёт", String.valueOf(score), true);
                        kvazarInfo.setScore(score);
                        message.editMessage(builder.build()).queue();
                        event.getMessage().delete().queue();
                    }
                }
            }
        } else if (command[0].equalsIgnoreCase("two")) {
            if (kvazarCache.get(event.getMember().getId()) != null) {
                if (kvazarCache.get(event.getMember().getId()).getChannel().getId().equals(event.getChannel().getId())) {
                    final KvazarInfo kvazarInfo = kvazarCache.get(event.getMember().getId());
                    TextChannel channel = kvazarInfo.getChannel();
                    Message message = channel.retrieveMessageById(kvazarInfo.getMessage().getId()).complete();
                    EmbedBuilder builder = new EmbedBuilder(message.getEmbeds().get(0));
                    if (!builder.getFields().isEmpty()) {
                        int user = kvazarInfo.getScore();
                        builder.clearFields();
                        int min = 4;
                        int max = 7;
                        int diff = max - min;
                        Random random = new Random();
                        int score = random.nextInt(diff + 1) + min;
                        if ((user + score) > 20){
                            builder.addField("Твой счёт", String.valueOf(user + score),true);
                            builder.setDescription("Вы проиграли!");
                            builder.setColor(Color.RED);
                            kvazarCache.remove(event.getMember().getId());
                            final CooldownInfoKvazar cooldownInfo = new CooldownInfoKvazar(new Date());
                            cooldawnCache.put(event.getMember().getId(), cooldownInfo);
                        }
                        else {
                            builder.addField("Твой счёт", String.valueOf(user + score), true);
                            kvazarInfo.setScore(user + score);
                        }
                        message.editMessage(builder.build()).queue();
                        event.getMessage().delete().queue();
                    } else {
                        int user = kvazarInfo.getScore();
                        builder.clearFields();
                        int min = 4;
                        int max = 7;
                        int diff = max - min;
                        Random random = new Random();
                        int score = random.nextInt(diff + 1) + min;
                        builder.addField("Твой счёт", String.valueOf(score), true);
                        kvazarInfo.setScore(score);
                        message.editMessage(builder.build()).queue();
                        event.getMessage().delete().queue();
                    }
                }
            }
        } else if (command[0].equalsIgnoreCase("close")){
            if (kvazarCache.get(event.getMember().getId()) != null) {
                if (kvazarCache.get(event.getMember().getId()).getChannel().getId().equals(event.getChannel().getId())) {
                    final KvazarInfo kvazarInfo = kvazarCache.get(event.getMember().getId());
                    if (!kvazarInfo.getMessage().getEmbeds().get(0).getFields().isEmpty()){
                        EmbedBuilder builder = new EmbedBuilder(kvazarInfo.getMessage().getEmbeds().get(0));
                        int user = kvazarInfo.getScore();
                        AtomicInteger money = new AtomicInteger();
                        int stavka = kvazarInfo.getStavka();
                        StringBuilder stringBuilder = new StringBuilder();
                        if (user < 15){
                            stringBuilder.append("Вы проиграли, ваша ставка < 15");
                        }
                        else if (user == 15){
                            money.set(stavka/4);
                        }
                        else if (user == 16){
                            money.set(stavka/2);
                        }
                        else if (user == 17){
                            money.set(stavka);
                        }
                        else if (user == 18){
                            money.set(stavka + stavka/4);
                        }
                        else if (user == 19){
                            money.set(stavka + stavka/2);
                        }
                        else if (user == 20){
                            money.set(stavka * 2);
                        }
                        else if (user > 20){
                            stringBuilder.append("Вы проиграли, ваша ставка > 20");
                        }
                        ObjectMapper mapper = new ObjectMapper();
                        File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                        try {
                            final MessageRankInfo messageRankInfo = mapper.readValue(file, MessageRankInfo.class);
                            int newMoney = money.get();
                            if (newMoney != 0){
                                builder.setDescription("Вы выиграли!");
                                builder.clearFields();
                                builder.addField("Твой счёт", String.valueOf(user),true);
                                builder.setColor(Color.GREEN);
                                messageRankInfo.setMoney(messageRankInfo.getMoney() + newMoney);
                                mapper.writeValue(file,messageRankInfo);
                                final CooldownInfoKvazar cooldownInfo = new CooldownInfoKvazar(new Date());
                                cooldawnCache.put(event.getMember().getId(), cooldownInfo);
                            }
                            else {
                                builder.setDescription(stringBuilder.toString());
                                builder.clearFields();
                                builder.addField("Твой счёт", String.valueOf(user),true);
                                builder.setColor(Color.RED);
                                final CooldownInfoKvazar cooldownInfo = new CooldownInfoKvazar(new Date());
                                cooldawnCache.put(event.getMember().getId(), cooldownInfo);
                            }
                            kvazarCache.remove(event.getMember().getId());
                            kvazarInfo.getMessage().editMessage(builder.build()).queue();
                            event.getMessage().delete().queue();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
