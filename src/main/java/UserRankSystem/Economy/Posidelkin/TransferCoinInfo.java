package UserRankSystem.Economy.Posidelkin;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

public class TransferCoinInfo {
    private String memberID;
    private String messageId;
    private String channelID;
    private int Money;

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getChannelID() {
        return channelID;
    }

    public void setChannelID(String channelID) {
        this.channelID = channelID;
    }

    public int getMoney() {
        return Money;
    }

    public void setMoney(int money) {
        Money = money;
    }

    public TransferCoinInfo(final Message message, final Member member, final TextChannel channel, final int Money){
        this.memberID = member.getId();
        this.messageId = message.getId();
        this.channelID = channel.getId();
        this.Money = Money;
    }
}
