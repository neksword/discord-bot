package UserRankSystem.Economy.Posidelkin;

import BotConfig.BotConfig;
import UserRankSystem.Economy.BotMoney.BotMoneyInfo;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.*;

public class TransferCoin extends ListenerAdapter {
    private final Map<String, TransferCoinInfo> transferCoinInfoCache = new ConcurrentHashMap<>();
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(4);
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        if (command[0].equalsIgnoreCase(cfg.prefix() + "передача")){
            if (event.getMember().getId().equals("805413570803531777")){
                if (event.getChannel().getId().equals(cfg.botaudit())){
                    if (command.length == 3){
                        Member member = event.getGuild().getMemberById(command[1]);
                        try {
                        int money = Integer.parseInt(command[2].trim());
                        int newMoney = money*10;
                        int newMoney1 = (int) (newMoney - newMoney*0.1);
                        File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + member.getId() + ".json");
                        ObjectMapper mapper = new ObjectMapper();
                            MessageRankInfo messageRankInfo = mapper.readValue(file,MessageRankInfo.class);
                            messageRankInfo.setMoney(messageRankInfo.getMoney() + newMoney1);
                            mapper.writeValue(file,messageRankInfo);
                            event.getChannel().sendMessage( member.getId() + " +").queue();
                            File file1 = new File("/home/" + cfg.homedirectory() + "/BotMoney.json");
                            BotMoneyInfo botMoneyInfo = mapper.readValue(file1,BotMoneyInfo.class);
                            botMoneyInfo.setMoney(botMoneyInfo.getMoney() + (newMoney - newMoney1));
                            mapper.writeValue(file1,botMoneyInfo);
                            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
                            EmbedBuilder builder = new EmbedBuilder();
                            builder.setAuthor(member.getEffectiveName(),member.getUser().getAvatarUrl(),member.getUser().getEffectiveAvatarUrl());
                            builder.setTitle("Были переведены коины в монеты!");
                            builder.addField("Информация:","**Монет переведено - ** `" + newMoney + "`" + "\n" + "**Дата - **`" + new Date().toString() + "`" + "\n" + "**Комиссия перевода - **`" + (newMoney - newMoney1) + "`",false);
                            builder.addField("Пользователь:","Имя: `" + member.getEffectiveName() + "` " + "(" + member.getAsMention() + ")" + "\n" + "ID: `" + member.getId() + "`",false);
                            logchannel.sendMessage(builder.build()).queue();
                        } catch (IOException | NumberFormatException e) {
                            e.printStackTrace();
                            event.getChannel().sendMessage(member.getId() + " -").queue();
                            event.getGuild().getTextChannelById(cfg.musicchannel()).sendMessage("**ОШИБКА В ПЕРЕВОДЕ**").queue();
                        }
                    }
                    else {
                        event.getGuild().getTextChannelById(cfg.musicchannel()).sendMessage("**ОШИБКА В ПЕРЕВОДЕ**").queue();
                        event.getChannel().sendMessage(event.getMember().getId() + " -").queue();
                    }
                }
            }
        }
        else if (command[0].equalsIgnoreCase(cfg.prefix() + "transfer") || command[0].equalsIgnoreCase("D!transfer")){
            final UsedChannel usedChannel = new UsedChannel();
            if (usedChannel.Return(event.getMember(), event.getChannel(), event.getMessage())) {
                if (command.length >= 2){
                    try {
                        int money = Integer.parseInt(command[1].trim());
                        File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                        ObjectMapper mapper = new ObjectMapper();
                        MessageRankInfo messageRankInfo = mapper.readValue(file,MessageRankInfo.class);
                        if (messageRankInfo.getMoney() < money){
                            event.getMessage().reply("**У вас недостаточно монет!**").queue();
                        }
                        else {
                            event.getGuild().getTextChannelById(cfg.botaudit()).sendMessage("\"перевести " + event.getMember().getId() + " " + money).queue();
                            Message message = event.getMessage().reply("**Ожидайте результаты перевода!**").complete();
                            final TransferCoinInfo transferCoinInfo = new TransferCoinInfo(message,event.getMember(), event.getChannel(),money);
                            transferCoinInfoCache.put(event.getMember().getId(),transferCoinInfo);
                            Runnable runnable = () -> {
                                if (transferCoinInfoCache.get(event.getMember().getId()) != null) {
                                    message.editMessage("**Ошибка в переводе!**").queue();
                                    transferCoinInfoCache.remove(event.getMember().getId());
                                }
                            };
                            ScheduledFuture<?> scheduledFuture = scheduler.schedule(runnable,15, TimeUnit.SECONDS);
                        }
                    }
                    catch (NumberFormatException| IOException exception){
                        exception.printStackTrace();
                        event.getMessage().reply("**Укажите правильное количество монет!**").queue();
                    }
                }
                else {
                    event.getMessage().reply("**Укажите количество монет для перевода!**").queue();
                }
            }
        }
        else if (event.getChannel().getId().equals(cfg.botaudit())){
            if (event.getMember().getId().equals("805413570803531777")){
            if (command.length == 2) {
                Member member = event.getGuild().getMemberById(command[0]);
                if (member != null) {
                    if (command[0].equals(member.getId())) {
                        if (transferCoinInfoCache.get(member.getId()) != null) {
                            if (command[1].equals("+")) {
                                TextChannel channel = event.getGuild().getTextChannelById(transferCoinInfoCache.get(member.getId()).getChannelID());
                                Message message = channel.retrieveMessageById(transferCoinInfoCache.get(member.getId()).getMessageId()).complete();
                                try {
                                    int money = transferCoinInfoCache.get(member.getId()).getMoney();
                                    int coin = money/10;
                                    int coin1 = (int) (coin*0.1);
                                    File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + member.getId() + ".json");
                                    ObjectMapper mapper = new ObjectMapper();
                                    MessageRankInfo messageRankInfo = mapper.readValue(file,MessageRankInfo.class);
                                    System.out.println(transferCoinInfoCache.get(member.getId()).getMoney());
                                    messageRankInfo.setMoney(messageRankInfo.getMoney() - transferCoinInfoCache.get(member.getId()).getMoney());
                                    System.out.println(messageRankInfo.getMoney());
                                    System.out.println(messageRankInfo.getMoney() - transferCoinInfoCache.get(member.getId()).getMoney());
                                    mapper.writeValue(file,messageRankInfo);
                                    message.editMessage("**Вы успешно перевели монеты в коины!**").queue();
                                    TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
                                    EmbedBuilder builder = new EmbedBuilder();
                                    builder.setAuthor(member.getEffectiveName(),member.getUser().getAvatarUrl(),member.getUser().getEffectiveAvatarUrl());
                                    builder.setTitle("Были переведены монеты!");
                                    builder.addField("Информация:","**Монет переведено - ** `" + transferCoinInfoCache.get(member.getId()).getMoney() + "`" + "\n" + "**Дата - **`" + new Date().toString() + "`" + "\n" + "**Комиссия перевода - **`" + coin1 + "`",false);
                                    builder.addField("Пользователь:","Имя: `" + member.getEffectiveName() + "` " + "(" + member.getAsMention() + ")" + "\n" + "ID: `" + member.getId() + "`",false);
                                    logchannel.sendMessage(builder.build()).queue();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                transferCoinInfoCache.remove(member.getId());
                            } else if (command[1].equals("-")) {
                                TextChannel channel = event.getGuild().getTextChannelById(transferCoinInfoCache.get(member.getId()).getChannelID());
                                Message message = channel.retrieveMessageById(transferCoinInfoCache.get(member.getId()).getMessageId()).complete();
                                message.editMessage("**Ошибка в переводе!**").queue();
                                transferCoinInfoCache.remove(member.getId());
                            }
                        }
                        }
                    }
                }
            }
        }
    }
}
