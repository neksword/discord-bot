package UserRankSystem.Economy;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class GiveMoneyCommand extends Command {

    public GiveMoneyCommand() {
        this.name = "givemoney";
        this.guildOnly = true;
        this.cooldown = 20;
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            String[] command = event.getMessage().getContentRaw().split("\\s");
            List<Member> members = event.getMessage().getMentionedMembers();
            if (members.isEmpty()) {
                event.getMessage().reply("**Укажите пользователя для перевода!**").queue();
            } else {
                if (command.length < 3) {
                    event.getMessage().reply("Использование команды: " + cfg.prefix() + "givemoney @пользователь (количество монет)").queue();
                } else {
                    try {
                        int money = Integer.parseInt(command[2]);
                        ObjectMapper mapper = new ObjectMapper();
                        try {
                            if (!(new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json").exists())) {
                                File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                                file.createNewFile();
                                final MessageRankInfo rankInfo = new MessageRankInfo(event.getMember(), 1, 1, 15, 1,1,0,0,0,0);
                                mapper.writeValue(file, rankInfo);
                            }
                            if (!members.isEmpty()) {
                                if (!(new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + members.get(0).getId() + ".json").exists())) {
                                    File file = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + members.get(0).getId() + ".json");
                                    file.createNewFile();
                                    final MessageRankInfo rankInfo = new MessageRankInfo(members.get(0), 1, 1, 15, 1,1,0,0,0,0);
                                    mapper.writeValue(file, rankInfo);
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            File member = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + event.getMember().getId() + ".json");
                            File mentionedMember = new File("/home/" + cfg.homedirectory() + "/UserRankSystemMessage/" + members.get(0).getId() + ".json");
                            final MessageRankInfo messageRankInfoMember = mapper.readValue(member, MessageRankInfo.class);
                            final MessageRankInfo messageRankInfoMentionedMember = mapper.readValue(mentionedMember, MessageRankInfo.class);
                            if (messageRankInfoMember.getMoney() < money) {
                                event.getMessage().reply("**У вас недостаточно средств!**").queue();
                            } else {
                                messageRankInfoMember.setMoney(messageRankInfoMember.getMoney() - money);
                                messageRankInfoMentionedMember.setMoney(messageRankInfoMentionedMember.getMoney() + money);
                                mapper.writeValue(member, messageRankInfoMember);
                                mapper.writeValue(mentionedMember, messageRankInfoMentionedMember);
                                event.getMessage().reply("**Монеты переведены участнику -** " + members.get(0).getAsMention()).queue();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (NumberFormatException nfe) {
                        event.getMessage().reply("Использование команды: " + cfg.prefix() + "givemoney @пользователь (количество монет)").queue();
                    }
                }
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "GiveMoney";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
