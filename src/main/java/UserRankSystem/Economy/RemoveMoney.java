package UserRankSystem.Economy;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class RemoveMoney extends Command {
    private final MongoCollection<Document> collection;
    public RemoveMoney(MongoCollection<Document> mongoCollection) {
        this.name = "removemoney";
        this.guildOnly = true;
        this.cooldown = 20;
        this.ownerCommand = true;
        this.collection = mongoCollection;
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            String[] command = event.getMessage().getContentRaw().split("\\s");
            if (event.isOwner()) {
             List<Member> members = event.getMessage().getMentionedMembers();
                if (members.isEmpty()) {
                    event.getMessage().reply("**Укажите пользователя для перевода!**").queue();
                } else {
                    if (command.length < 2) {
                        event.getMessage().reply("Использование команды: " + cfg.prefix() + "removemoney @пользователь (количество коинов)").queue();
                    } else {
                        try {
                                int money = Integer.parseInt(command[2]);
                                long id1 = Long.parseLong(event.getMember().getId());
                                Document document1 = collection.find(new Document("id",id1)).first();
                                if (document1 == null){
                                    DBNewUser dbNewUser = new DBNewUser();
                                    dbNewUser.NewUser(collection,event.getMember());
                                }
                                long id = Long.parseLong(members.get(0).getId());
                                Document document = collection.find(new Document("id",id)).first();
                                if (document == null){
                                    DBNewUser dbNewUser = new DBNewUser();
                                    dbNewUser.NewMentionUser(collection,members);
                                }
                                collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) - money)));

                                event.getMessage().reply("**Коины удалены у участника -** " + members.get(0).getAsMention()).queue();
                        } catch (NumberFormatException nfe) {
                            event.getMessage().reply("Использование команды: " + cfg.prefix() + "removemoney @пользователь (количество коинов)").queue();
                        }
                    }
                }
            } else {
                event.getMessage().reply("**Вы не можете использовать эту команду!**").queue();
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Removemoney";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
