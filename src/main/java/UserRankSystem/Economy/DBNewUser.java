package UserRankSystem.Economy;

import com.mongodb.client.MongoCollection;
import net.dv8tion.jda.api.entities.Member;
import org.bson.Document;

import java.util.List;

public class DBNewUser {
    public void NewUser(MongoCollection<Document> collection, Member member){
        long id = Long.parseLong(member.getId());
        Document document1 = new Document("id", id);
        document1.put("member_name",member.getUser().getName());
        document1.put("minvoice",0);
        document1.put("coins",0);
        document1.put("time","NO INFO");
        document1.put("count_status","stop");
        document1.put("lvl",1);
        document1.put("newRank",15);
        document1.put("message",0);
        document1.put("case1",0);
        document1.put("case2",0);
        document1.put("case3",0);
        document1.put("case4",0);
        document1.put("cookies",0);
        collection.insertOne(document1);
    }
    public void NewMentionUser(MongoCollection<Document> collection, List<Member> memberList){
        Document document1 = new Document("id",Long.parseLong(memberList.get(0).getId()));
        document1.put("member_name",memberList.get(0).getUser().getName());
        document1.put("minvoice",0);
        document1.put("coins",0);
        document1.put("time","NO INFO");
        document1.put("count_status","stop");
        document1.put("lvl",1);
        document1.put("newRank",15);
        document1.put("message",0);
        document1.put("case1",0);
        document1.put("case2",0);
        document1.put("case3",0);
        document1.put("case4",0);
        document1.put("cookies",0);
        collection.insertOne(document1);
    }
}
