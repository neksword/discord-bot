package UserRankSystem.Economy.Shop;

import BotConfig.BotConfig;
import com.mongodb.client.MongoCollection;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.awt.*;
import java.util.Map;
import java.util.concurrent.*;

public class    ShopCommand extends ListenerAdapter {
    private final Map<String, ShopInfo> ShopCache = new ConcurrentHashMap<String, ShopInfo>();
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    MongoCollection<Document> collection;
    public ShopCommand(MongoCollection<Document> collection){
        this.collection = collection;
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
    String[] command = event.getMessage().getContentRaw().split("\\s");
        if (command[0].equalsIgnoreCase(cfg.prefix() + "shop")){
            EmbedBuilder builder = new EmbedBuilder();
            builder.setAuthor(event.getAuthor().getName(),event.getAuthor().getAvatarUrl(),event.getAuthor().getEffectiveAvatarUrl());
            builder.setTitle("Добро пожаловать в Магазин!");
            int doc = (int) collection.countDocuments();
            for (int i = 1; i <= 15; i++){
                Document document = collection.find(new Document("ids",String.valueOf(i))).first();
                builder.addField("| ID: " + document.get("ids") + " > " + document.get("price") + " коинов.","| " + (document.get("role_id")) + "\n",true);
            }
            Color purple = new Color(76, 0, 230);
            builder.setColor(purple);
            builder.setFooter("Раздел 1 из 3");
            Message message = event.getMessage().reply(builder.build()).complete();
            message.addReaction("1️⃣").queue();
            message.addReaction("2️⃣").queue();
            message.addReaction("3️⃣").queue();
            Runnable runnable = () -> {
                message.removeReaction("1️⃣").queue();
                message.removeReaction("2️⃣").queue();
                message.removeReaction("3️⃣").queue();
                ShopCache.remove(message.getId());
            };
            ScheduledFuture<?> reactionDelete = scheduler.schedule(runnable,1, TimeUnit.MINUTES);
            ShopInfo shopInfo = new ShopInfo();
            shopInfo.setMessage(message);
            shopInfo.setMember(event.getMember());
            shopInfo.setPage(1);
            ShopCache.put(message.getId(),shopInfo);
        }
    }

    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        if (event.getMember().getUser().isBot()){

        }
        else if (ShopCache.containsKey(event.getMessageId())) {
            if (event.getMember().getId().equals(ShopCache.get(event.getMessageId()).getMember().getId())) {
                Message message = ShopCache.get(event.getMessageId()).getMessage();
                if (event.getReaction().getReactionEmote().getAsReactionCode().equals("1️⃣")) {
                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setAuthor(event.getMember().getUser().getName(), event.getMember().getUser().getAvatarUrl(), event.getMember().getUser().getEffectiveAvatarUrl());
                    builder.setTitle("Добро пожаловать в Магазин!");
                    int doc = (int) collection.countDocuments();
                    for (int i = 1; i <= 15; i++) {
                        Document document = collection.find(new Document("ids", String.valueOf(i))).first();
                        builder.addField("| ID: " + document.get("ids") + " > " + document.get("price") + " коинов.", "| " + (document.get("role_id")) + "\n", true);
                    }
                    Color purple = new Color(76, 0, 230);
                    builder.setColor(purple);
                    builder.setFooter("Раздел 1 из 3");
                    message.editMessage(builder.build()).queue();
                }
                else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("2️⃣")) {
                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setAuthor(event.getMember().getUser().getName(), event.getMember().getUser().getAvatarUrl(), event.getMember().getUser().getEffectiveAvatarUrl());
                    builder.setTitle("Добро пожаловать в Магазин!");
                    int doc = (int) collection.countDocuments();
                    for (int i = 16; i <= 30; i++) {
                        Document document = collection.find(new Document("ids", String.valueOf(i))).first();
                        builder.addField("| ID: " + document.get("ids") + " > " + document.get("price") + " коинов.", "| " + (document.get("role_id")) + "\n", true);
                    }
                    Color purple = new Color(76, 0, 230);
                    builder.setColor(purple);
                    builder.setFooter("Раздел 2 из 3");
                    message.editMessage(builder.build()).queue();
                }
                else if (event.getReaction().getReactionEmote().getAsReactionCode().equals("3️⃣")) {
                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setAuthor(event.getMember().getUser().getName(), event.getMember().getUser().getAvatarUrl(), event.getMember().getUser().getEffectiveAvatarUrl());
                    builder.setTitle("Добро пожаловать в Магазин!");
                    int doc = (int) collection.countDocuments();
                    for (int i = 31; i <= doc; i++) {
                        Document document = collection.find(new Document("ids", String.valueOf(i))).first();
                        builder.addField("| ID: " + document.get("ids") + " > " + document.get("price") + " коинов.","| " + (document.get("role_id")) + "\n", true);
                    }
                    builder.setFooter("Раздел 3 из 3");
                    message.editMessage(builder.build()).queue();
                }
            }
            event.getReaction().removeReaction(event.getUser()).queue();
        }
    }
}
