package UserRankSystem.Economy.BotMoney;

public class BotMoneyInfo {
    private int money;
    private int dicemoney;
    private int bjmoney;

    public BotMoneyInfo(){

    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getDicemoney() {
        return dicemoney;
    }

    public void setDicemoney(int dicemoney) {
        this.dicemoney = dicemoney;
    }

    public int getBjmoney() {
        return bjmoney;
    }

    public void setBjmoney(int bjmoney) {
        this.bjmoney = bjmoney;
    }

    public BotMoneyInfo(final int money, final int dicemoney, final int bjmoney){
        this.money = money;
        this.dicemoney = dicemoney;
        this.bjmoney = bjmoney;
    }
}
