package UserRankSystem.Economy.BotMoney;

import BotConfig.BotConfig;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import org.aeonbits.owner.ConfigFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;

public class BotMoney extends Command {

    public BotMoney() {
        this.name = "botmoney";
        this.guildOnly = true;
        this.ownerCommand = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        if (event.isOwner()) {
            if (!new File("/home/" + cfg.homedirectory() + "/BotMoney.json").exists()) {
                File file = new File("/home/" + cfg.homedirectory() + "/BotMoney.json");
                try {
                    file.createNewFile();
                    ObjectMapper mapper = new ObjectMapper();
                    BotMoneyInfo botMoneyInfo = new BotMoneyInfo(1,1,1);
                    mapper.writeValue(file, botMoneyInfo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            File file = new File("/home/" + cfg.homedirectory() + "/BotMoney.json");
            ObjectMapper mapper = new ObjectMapper();
            AtomicInteger integer = new AtomicInteger();
            try {
                Path dir = Paths.get("/home/" + cfg.homedirectory() + "/UserRankSystemMessage");
                DirectoryStream<Path> stream = null;
                try {
                    stream = Files.newDirectoryStream(dir);
                    for (Path file1 : stream) {
                        MessageRankInfo messageRankInfo = mapper.readValue(file1.toFile(),MessageRankInfo.class);
                        integer.addAndGet(messageRankInfo.getMoney());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                BotMoneyInfo botMoneyInfo = mapper.readValue(file, BotMoneyInfo.class);
                event.getMessage().reply("Казна переводов - " + botMoneyInfo.getMoney() + "\n" + "DiceGame - " + botMoneyInfo.getDicemoney() + "\n" + "Всего монет - " + integer.get() + "\n" + "Казна блекджека - " + botMoneyInfo.getBjmoney()).queue();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}