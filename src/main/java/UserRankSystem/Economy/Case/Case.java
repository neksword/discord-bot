package UserRankSystem.Economy.Case;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.Economy.DBNewUser;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Case extends Command {
    private final Map<Integer, CaseInfo> CaseCache = new ConcurrentHashMap<Integer, CaseInfo>();
    private final MongoCollection<Document> collection;
    private final String channel = "840668559365505107";
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public Case(MongoCollection<Document> collection){
        this.guildOnly = true;
        this.name = "case";
        this.cooldown = 30;
        this.collection = collection;
    }
    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel channel = new UsedChannel();
        if (channel.Return(event.getMember(), event.getTextChannel(), event.getMessage())){
            long id = Long.parseLong(event.getMember().getId());
            Document document = collection.find(new Document("id", id)).first();
            if (document == null) {
                DBNewUser dbNewUser = new DBNewUser();
                dbNewUser.NewUser(collection, event.getMember());
            }
            String[] command = event.getMessage().getContentRaw().split("\\s");
            if (command.length < 2){
                event.replyError("**Укажите номер кейса!**");
            }
            try {
                int Case = Integer.parseInt(command[1].trim());
                if (Case == 1) {
                    Case1(document,event);
                }
                else if (Case == 4) {
                    Case4(document,event);
                }
                else if (Case == 2){
                    Case2(document,event);
                }
                else if (Case == 3){
                    Case3(document,event);
                }
            }
            catch (NumberFormatException e){
                event.replyError("**Укажите номер кейса!**");
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "Case";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
    private void Case4(Document document, CommandEvent event) throws IOException {
        if (Integer.parseInt(String.valueOf(document.get("case4"))) > 0) {
            if (!CaseCache.containsKey(4)) {
                case4Start(event);
            }
            long id = Long.parseLong(event.getMember().getId());
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case4", Integer.parseInt(String.valueOf(document.get("case4"))) - 1)));
            //win
                int win = Random4();
                int[] Win = CaseCache.get(4).getWin();
                if (Win[win] != 0){
                    event.getMessage().reply(WinBuilder4(Win[win],event).build()).queue();
                    Win[win] = 0;
                    CaseCache.get(4).setWin(Win);
                    case4Stop(event);
                }
                else {
                    while (true){
                        int win1 = Random4();
                        if (Win[win1] != 0){
                            event.getMessage().reply(WinBuilder4(Win[win1],event).build()).queue();
                            Win[win1] = 0;
                            CaseCache.get(4).setWin(Win);
                            break;
                        }
                    }
                    case4Stop(event);
                }
        }
        else {
            event.replyError("**У вас нет этого кейса!**");
        }
    }
    private int Random4(){
        Random random = new Random();
        int min = 0;
        int max = 99;
        int diff = max - min;
        int case1 = random.nextInt(diff + 1) + min;
        return case1;
    }
    private void check4(int random, int moneyRub){
        final CaseInfo caseInfo = CaseCache.get(4);
        int[] Win = caseInfo.getWin();
        if (Win[random] == 0) {
            Win[random] = moneyRub;
            caseInfo.setWin(Win);
        }
        else {
            while (true){
                int r = Random4();
                if (Win[r] == 0){
                    Win[r] = moneyRub;
                    break;
                }
            }
        }
    }
    private void case4Stop(CommandEvent event){
        AtomicInteger atomicInteger = new AtomicInteger();
        for (int i = 0; i <= 99; i++){
            if (CaseCache.get(4).getWin()[i] == 0){
                atomicInteger.set(atomicInteger.get() + 1);
            }
            else {
                break;
            }
        }
        if (atomicInteger.get() == 100){
            case4Start(event);
        }
    }
    private void case4Start(CommandEvent event){
        CaseInfo caseInfo = new CaseInfo(4, 2500 * 100);
        CaseCache.put(4, caseInfo);
        check4(Random4(), 1000);
        for (int i = 1; i <= 4; i++) {
            check4(Random4(), 500);
        }
        for (int i = 1; i <= 7; i++) {
            check4(Random4(), 300);
        }
        for (int i = 1; i <= 13; i++) {
            check4(Random4(), 100);
        }
        for (int i = 1; i <= 15; i++) {
            check4(Random4(), 80);
        }
        for (int i = 1; i <= 20; i++) {
            check4(Random4(), 50);
        }
        for (int i = 1; i <= 40; i++) {
            check4(Random4(), 10);
        }
    }
    private EmbedBuilder WinBuilder4(int coinswin,CommandEvent event){
        long id = Long.parseLong(event.getMember().getId());
        Document document = collection.find(new Document("id", id)).first();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setAuthor(event.getMember().getEffectiveName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
        embedBuilder.setTitle("Кейс №4");
        embedBuilder.setDescription(WinNum4(coinswin));
        Color purple = new Color(76, 0, 230);
        embedBuilder.setColor(purple);
        embedBuilder.addField("","Для получения приза обращайтесь к " + event.getGuild().getMemberById("282232240010690560").getAsMention(),false);
        embedBuilder.setFooter("У вас осталось - " + document.get("case4") + " кейсов.");
        EmbedBuilder builder = new EmbedBuilder(embedBuilder);
        builder.addField("Сумма = " + coinswin,"",false);
        event.getGuild().getTextChannelById(channel).sendMessage(builder.build()).queue();
        return embedBuilder;
    }
    private String WinNum4(int win){
        if (win == 50){
            return "Вам выпала вещь для игры CS:GO на платформе Steam.";
        }
        else if (win == 10){
            return "Вам выпала вещь для игры CS:GO на платформе Steam.";
        }
        else if (win == 80){
            return "Вам выпала вещь для игры CS:GO на платформе Steam.";
        }
        else {
            return "Вам выпало - `" + win + "` рублей";
        }
    }
    private void Case1(Document document, CommandEvent event) throws IOException {
        if (Integer.parseInt(String.valueOf(document.get("case1"))) > 0) {
            if (!CaseCache.containsKey(1)) {
                case1Start(event);
            }
            long id = Long.parseLong(event.getMember().getId());
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case1", Integer.parseInt(String.valueOf(document.get("case1"))) - 1)));
            //win
            int win = Random1();
            int[] Win = CaseCache.get(1).getWin();
            if (Win[win] != 0){
                event.getMessage().reply(WinBuilder1(Win[win],event).build()).queue();
                collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) + Win[win])));
                Win[win] = 0;
                CaseCache.get(1).setWin(Win);
                case1Stop(event);
            }
            else {
                while (true){
                    int win1 = Random1();
                    if (Win[win1] != 0){
                        event.getMessage().reply(WinBuilder1(Win[win1],event).build()).queue();
                        collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) + Win[win])));
                        Win[win1] = 0;
                        CaseCache.get(1).setWin(Win);
                        break;
                    }
                }
                case1Stop(event);
            }
        }
        else {
            event.replyError("**У вас нет этого кейса!**");
        }
    }
    private int Random1(){
        Random random = new Random();
        int min = 0;
        int max = 99;
        int diff = max - min;
        int case1 = random.nextInt(diff + 1) + min;
        return case1;
    }
    private void check1(int random, int moneyRub){
        final CaseInfo caseInfo = CaseCache.get(1);
        int[] Win = caseInfo.getWin();
        if (Win[random] == 0) {
            Win[random] = moneyRub;
            caseInfo.setWin(Win);
        }
        else {
            while (true){
                int r = Random1();
                if (Win[r] == 0){
                    Win[r] = moneyRub;
                    break;
                }
            }
        }
    }
    private void case1Stop(CommandEvent event){
        AtomicInteger atomicInteger = new AtomicInteger();
        for (int i = 0; i <= 99; i++){
            if (CaseCache.get(1).getWin()[i] == 0){
                atomicInteger.set(atomicInteger.get() + 1);
            }
            else {
                break;
            }
        }
        if (atomicInteger.get() == 100){
            case1Start(event);
        }
    }
    private void case1Start(CommandEvent event){
        CaseInfo caseInfo = new CaseInfo(4, 2500 * 100);
        CaseCache.put(1, caseInfo);
        check1(Random1(), 50000);
        for (int i = 1; i <= 4; i++) {
            check1(Random1(), 25000);
        }
        for (int i = 1; i <= 7; i++) {
            check1(Random1(), 15000);
        }
        for (int i = 1; i <= 13; i++) {
            check1(Random1(), 5000);
        }
        for (int i = 1; i <= 15; i++) {
            check1(Random1(), 4000);
        }
        for (int i = 1; i <= 20; i++) {
            check1(Random1(), 2500);
        }
        for (int i = 1; i <= 40; i++) {
            check1(Random1(), 500);
        }
    }
    private EmbedBuilder WinBuilder1(int coinswin,CommandEvent event){
        long id = Long.parseLong(event.getMember().getId());
        Document document = collection.find(new Document("id", id)).first();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setAuthor(event.getMember().getEffectiveName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
        embedBuilder.setTitle("Кейс №1");
        embedBuilder.setDescription("Вам выпало - `" + coinswin + "` коинов.");
        Color purple = new Color(76, 0, 230);
        embedBuilder.setColor(purple);
        embedBuilder.setFooter("У вас осталось - " + document.get("case1") + " кейсов.");
        EmbedBuilder builder = new EmbedBuilder(embedBuilder);
        builder.addField("Сумма = " + coinswin,"",false);
        event.getGuild().getTextChannelById(channel).sendMessage(builder.build()).queue();
        return embedBuilder;
    }
    private void Case2(Document document, CommandEvent event) throws IOException {
        if (Integer.parseInt(String.valueOf(document.get("case2"))) > 0) {
            if (!CaseCache.containsKey(2)) {
                case2Start(event);
            }
            long id = Long.parseLong(event.getMember().getId());
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case2", Integer.parseInt(String.valueOf(document.get("case2"))) - 1)));
            //win
            int win = Random2();
            int[] Win = CaseCache.get(2).getWin();
            if (Win[win] != 0){
                event.getMessage().reply(WinBuilder2(Win[win],event).build()).queue();
                Win[win] = 0;
                CaseCache.get(2).setWin(Win);
                case2Stop(event);
            }
            else {
                while (true){
                    int win1 = Random2();
                    if (Win[win1] != 0){
                        event.getMessage().reply(WinBuilder2(Win[win1],event).build()).queue();
                        Win[win1] = 0;
                        CaseCache.get(2).setWin(Win);
                        break;
                    }
                }
                case2Stop(event);
            }
        }
        else {
            event.replyError("**У вас нет этого кейса!**");
        }
    }
    private int Random2(){
        Random random = new Random();
        int min = 0;
        int max = 99;
        int diff = max - min;
        int case1 = random.nextInt(diff + 1) + min;
        return case1;
    }
    private void check2(int random, int moneyRub){
        final CaseInfo caseInfo = CaseCache.get(2);
        int[] Win = caseInfo.getWin();
        if (Win[random] == 0) {
            Win[random] = moneyRub;
            caseInfo.setWin(Win);
        }
        else {
            while (true){
                int r = Random2();
                if (Win[r] == 0){
                    Win[r] = moneyRub;
                    break;
                }
            }
        }
    }
    private void case2Stop(CommandEvent event){
        AtomicInteger atomicInteger = new AtomicInteger();
        for (int i = 0; i <= 99; i++){
            if (CaseCache.get(2).getWin()[i] == 0){
                atomicInteger.set(atomicInteger.get() + 1);
            }
            else {
                break;
            }
        }
        if (atomicInteger.get() == 100){
            case2Start(event);
        }
    }
    private void case2Start(CommandEvent event){
        CaseInfo caseInfo = new CaseInfo(2, 2500 * 100);
        CaseCache.put(2, caseInfo);
        check2(Random2(), 100000);
        for (int i = 1; i <= 4; i++) {
            check2(Random2(), 50000);
        }
        for (int i = 1; i <= 7; i++) {
            check2(Random2(), 30000);
        }
        for (int i = 1; i <= 13; i++) {
            check2(Random2(), 20);
        }
        for (int i = 1; i <= 15; i++) {
            check2(Random2(), 16);
        }
        for (int i = 1; i <= 20; i++) {
            check2(Random2(), 10);
        }
        for (int i = 1; i <= 40; i++) {
            check2(Random2(), 2);
        }
    }
    private EmbedBuilder WinBuilder2(int coinswin,CommandEvent event){
        long id = Long.parseLong(event.getMember().getId());
        Document document = collection.find(new Document("id", id)).first();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setAuthor(event.getMember().getEffectiveName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
        embedBuilder.setTitle("Кейс №2");
        embedBuilder.setDescription(WinNum2(coinswin, document));
        if (coinswin == 100000){
        }
        else if (coinswin == 50000){
        }
        else if (coinswin == 30000){
        }
        else {
            embedBuilder.addField("","Для получения приза обращайтесь к " + event.getGuild().getMemberById("282232240010690560").getAsMention(),false);
        }
        Color purple = new Color(76, 0, 230);
        embedBuilder.setColor(purple);
        embedBuilder.setFooter("У вас осталось - " + document.get("case2") + " кейсов.");
        EmbedBuilder builder = new EmbedBuilder(embedBuilder);
        builder.addField("Сумма = " + coinswin,"",false);
        event.getGuild().getTextChannelById(channel).sendMessage(builder.build()).queue();
        return embedBuilder;
    }
    private String WinNum2(int win, Document document){
        if (win == 100000){
            long id = Long.parseLong(String.valueOf(document.get("id")));
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) + 100000)));
            return "Вам выпало - `100000` коинов!";
        }
        else if (win == 50000){
            long id = Long.parseLong(String.valueOf(document.get("id")));
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) + 50000)));
            return "Вам выпало - `50000` коинов!";
        }
        else if (win == 30000){
            long id = Long.parseLong(String.valueOf(document.get("id")));
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) + 30000)));
            return "Вам выпало - `30000` коинов!";
        }
        else {
            return "Вам выпала вещь для игры CS:GO на платформе Steam.";
        }
    }
    private void Case3(Document document, CommandEvent event) throws IOException {
        if (Integer.parseInt(String.valueOf(document.get("case3"))) > 0) {
            if (!CaseCache.containsKey(3)) {
                case3Start(event);
            }
            long id = Long.parseLong(event.getMember().getId());
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case3", Integer.parseInt(String.valueOf(document.get("case3"))) - 1)));
            //win
            int win = Random3();
            int[] Win = CaseCache.get(3).getWin();
            if (Win[win] != 0){
                event.getMessage().reply(WinBuilder3(Win[win],event).build()).queue();
                Win[win] = 0;
                CaseCache.get(3).setWin(Win);
                case3Stop(event);
            }
            else {
                while (true){
                    int win1 = Random3();
                    if (Win[win1] != 0){
                        event.getMessage().reply(WinBuilder3(Win[win1],event).build()).queue();
                        Win[win1] = 0;
                        CaseCache.get(3).setWin(Win);
                        break;
                    }
                }
                case3Stop(event);
            }
        }
        else {
            event.replyError("**У вас нет этого кейса!**");
        }
    }
    private int Random3(){
        Random random = new Random();
        int min = 0;
        int max = 99;
        int diff = max - min;
        int case1 = random.nextInt(diff + 1) + min;
        return case1;
    }
    private void check3(int random, int moneyRub){
        final CaseInfo caseInfo = CaseCache.get(3);
        int[] Win = caseInfo.getWin();
        if (Win[random] == 0) {
            Win[random] = moneyRub;
            caseInfo.setWin(Win);
        }
        else {
            while (true){
                int r = Random3();
                if (Win[r] == 0){
                    Win[r] = moneyRub;
                    break;
                }
            }
        }
    }
    private void case3Stop(CommandEvent event){
        AtomicInteger atomicInteger = new AtomicInteger();
        for (int i = 0; i <= 99; i++){
            if (CaseCache.get(3).getWin()[i] == 0){
                atomicInteger.set(atomicInteger.get() + 1);
            }
            else {
                break;
            }
        }
        if (atomicInteger.get() == 100){
            case2Start(event);
        }
    }
    private void case3Start(CommandEvent event){
        CaseInfo caseInfo = new CaseInfo(3, 2500 * 100);
        CaseCache.put(3, caseInfo);
        check3(Random3(), 600);
        for (int i = 1; i <= 4; i++) {
            check3(Random3(), 300);
        }
        for (int i = 1; i <= 7; i++) {
            check3(Random3(), 180);
        }
        for (int i = 1; i <= 13; i++) {
            check3(Random3(), 30000);
        }
        for (int i = 1; i <= 15; i++) {
            check3(Random3(), 24000);
        }
        for (int i = 1; i <= 20; i++) {
            check3(Random3(), 15000);
        }
        for (int i = 1; i <= 40; i++) {
            check3(Random3(), 3000);
        }
    }
    private EmbedBuilder WinBuilder3(int coinswin,CommandEvent event) {
        long id = Long.parseLong(event.getMember().getId());
        Document document = collection.find(new Document("id", id)).first();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setAuthor(event.getMember().getEffectiveName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
        embedBuilder.setTitle("Кейс №3");
        embedBuilder.setDescription(WinNum3(coinswin, document));
        Color purple = new Color(76, 0, 230);
        embedBuilder.setColor(purple);
         if (coinswin == 30000) {
        } else if (coinswin == 24000) {
        } else if (coinswin == 15000) {
        } else if (coinswin == 3000) {
        } else {
            embedBuilder.addField("", "Для получения приза обращайтесь к " + event.getGuild().getMemberById("282232240010690560").getAsMention(), false);
        }
        embedBuilder.setFooter("У вас осталось - " + document.get("case3") + " кейсов.");
         EmbedBuilder builder = new EmbedBuilder(embedBuilder);
         builder.addField("Сумма = " + coinswin,"",false);
        event.getGuild().getTextChannelById(channel).sendMessage(builder.build()).queue();
        return embedBuilder;
    }
    private String WinNum3(int win, Document document){
        if (win == 30000){
            long id = Long.parseLong(String.valueOf(document.get("id")));
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) + 30000)));
            return "Вам выпало - `30000` коинов!";
        }
        else if (win == 24000){
            long id = Long.parseLong(String.valueOf(document.get("id")));
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) + 24000)));
            return "Вам выпало - `24000` коинов!";
        }
        else if (win == 15000){
            long id = Long.parseLong(String.valueOf(document.get("id")));
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) + 15000)));
            return "Вам выпало - `15000` коинов!";
        }
        else if (win == 3000){
            long id = Long.parseLong(String.valueOf(document.get("id")));
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) + 3000)));
            return "Вам выпало - `3000` коинов!";
        }
        else {
            return "Вам выпало - `" + win + "` рублей.";
        }
    }
}
