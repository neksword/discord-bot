package UserRankSystem.Economy.Case;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.Economy.DBNewUser;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class CaseShop extends Command {
    private final MongoCollection<Document> collection;
    BotConfig cfg = ConfigFactory.create(BotConfig.class);

    public CaseShop(MongoCollection<Document> collection) {
        this.guildOnly = true;
        this.name = "shopcase";
        this.collection = collection;
        this.cooldown = 30;
    }

    @Override
    protected void execute(CommandEvent event) {
        final UsedChannel usedChannel = new UsedChannel();
        if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
            long id = Long.parseLong(event.getMember().getId());
            Document document = collection.find(new Document("id", id)).first();
            if (document == null) {
                DBNewUser dbNewUser = new DBNewUser();
                dbNewUser.NewUser(collection, event.getMember());
            }
            String[] command = event.getMessage().getContentRaw().split("\\s");
            if (command.length == 1) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setAuthor(event.getAuthor().getName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getEffectiveAvatarUrl());
                embedBuilder.setTitle("Магазин кейсов");
                Color purple = new Color(76, 0, 230);
                embedBuilder.setColor(purple);
                embedBuilder.setDescription("Для покупки - shopcase (номер кейса)");
                embedBuilder.addField("Кейс №1", "Стоимость - 5000 коинов", true);
                embedBuilder.addField("Кейс №2", "Стоимость - 10000 коинов", false);
                embedBuilder.addField("Кейс №3", "Стоимость - 30000 коинов", false);
                embedBuilder.addField("Кейс №4", "Стоимость - 50000 коинов", false);
                embedBuilder.setFooter(event.getGuild().getName(), event.getGuild().getIconUrl());
                event.getMessage().reply(embedBuilder.build()).queue();
            } else {
                try {
                    int Case = Integer.parseInt(command[1].trim());
                    if (Case == 1) {
                        if (Integer.parseInt(String.valueOf(document.get("coins"))) < 5000) {
                            event.replyError("**У вас не достаточно коинов!**");
                        } else {
                            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) - 5000)));
                            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case1", Integer.parseInt(String.valueOf(document.get("case1"))) + 1)));
                            event.replySuccess("**Вы купили Кейс №1**");
                        }
                    } else if (Case == 4) {
                        if (Integer.parseInt(String.valueOf(document.get("coins"))) < 50000) {
                            event.replyError("**У вас не достаточно коинов!**");
                        } else {
                            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) - 50000)));
                            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case4", Integer.parseInt(String.valueOf(document.get("case4"))) + 1)));
                            event.replySuccess("**Вы купили Кейс №4**");
                        }
                    } else if (Case == 2) {
                    if (Integer.parseInt(String.valueOf(document.get("coins"))) < 10000) {
                        event.replyError("**У вас не достаточно коинов!**");
                    } else {
                        collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) - 10000)));
                        collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case2", Integer.parseInt(String.valueOf(document.get("case2"))) + 1)));
                        event.replySuccess("**Вы купили Кейс №2**");
                    }
                    } else if (Case == 3) {
                        if (Integer.parseInt(String.valueOf(document.get("coins"))) < 30000) {
                            event.replyError("**У вас не достаточно коинов!**");
                        } else {
                            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("coins", Integer.parseInt(String.valueOf(document.get("coins"))) - 30000)));
                            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case3", Integer.parseInt(String.valueOf(document.get("case3"))) + 1)));
                            event.replySuccess("**Вы купили Кейс №3**");
                        }
                }
                } catch (NumberFormatException e) {
                    event.replyError("**Укажите номер кейса!**");
                }
                TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
                LogCommand logCommand = new LogCommand();
                String commandname = "Caseshop";
                logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
            }
        }
    }
}
