package UserRankSystem.Economy.Case;

import java.util.Arrays;

public class CaseInfo {
    private int Case;
    private int CaseMoney;
    boolean o1;
    boolean o2;
    boolean o3;
    boolean o4;
    boolean o5;
    boolean o6;
    boolean o7;
    boolean o8;
    boolean o9;
    boolean o10;
    boolean o11;
    boolean o12;
    boolean o13;
    boolean o14;
    boolean o15;
    boolean o16;
    boolean o17;
    boolean o18;
    boolean o19;
    boolean o20;
    boolean o21;
    boolean o22;
    boolean o23;
    boolean o24;
    boolean o25;
    boolean o26;
    boolean o27;
    boolean o28;
    boolean o29;
    boolean o30;
    boolean o31;
    boolean o32;
    boolean o33;
    boolean o34;
    boolean o35;
    boolean o36;
    boolean o37;
    boolean o38;
    boolean o39;
    boolean o40;
    boolean o41;
    boolean o42;
    boolean o43;
    boolean o44;
    boolean o45;
    boolean o46;
    boolean o47;
    boolean o48;
    boolean o49;
    boolean o50;
    boolean o51;
    boolean o52;
    boolean o53;
    boolean o54;
    boolean o55;
    boolean o56;
    boolean o57;
    boolean o58;
    boolean o59;
    boolean o60;
    boolean o61;
    boolean o62;
    boolean o63;
    boolean o64;
    boolean o65;
    boolean o66;
    boolean o67;
    boolean o68;
    boolean o69;
    boolean o70;
    boolean o71;
    boolean o72;
    boolean o73;
    boolean o74;
    boolean o75;
    boolean o76;
    boolean o77;
    boolean o78;
    boolean o79;
    boolean o80;
    boolean o81;
    boolean o82;
    boolean o83;
    boolean o84;
    boolean o85;
    boolean o86;
    boolean o87;
    boolean o88;
    boolean o89;
    boolean o90;
    boolean o91;
    boolean o92;
    boolean o93;
    boolean o94;
    boolean o95;
    boolean o96;
    boolean o97;
    boolean o98;
    boolean o99;
    boolean o100;


    public boolean isO19() {
        return o19;
    }

    public void setO19(boolean o19) {
        this.o19 = o19;
    }

    public boolean isO20() {
        return o20;
    }

    public void setO20(boolean o20) {
        this.o20 = o20;
    }

    public boolean isO21() {
        return o21;
    }

    public void setO21(boolean o21) {
        this.o21 = o21;
    }

    public boolean isO22() {
        return o22;
    }

    public void setO22(boolean o22) {
        this.o22 = o22;
    }

    public boolean isO23() {
        return o23;
    }

    public void setO23(boolean o23) {
        this.o23 = o23;
    }

    public boolean isO24() {
        return o24;
    }

    public void setO24(boolean o24) {
        this.o24 = o24;
    }

    public boolean isO25() {
        return o25;
    }

    public void setO25(boolean o25) {
        this.o25 = o25;
    }

    public boolean isO26() {
        return o26;
    }

    public void setO26(boolean o26) {
        this.o26 = o26;
    }

    public boolean isO27() {
        return o27;
    }

    public void setO27(boolean o27) {
        this.o27 = o27;
    }

    public boolean isO28() {
        return o28;
    }

    public void setO28(boolean o28) {
        this.o28 = o28;
    }

    public boolean isO29() {
        return o29;
    }

    public void setO29(boolean o29) {
        this.o29 = o29;
    }

    public boolean isO30() {
        return o30;
    }

    public void setO30(boolean o30) {
        this.o30 = o30;
    }

    public boolean isO31() {
        return o31;
    }

    public void setO31(boolean o31) {
        this.o31 = o31;
    }

    public boolean isO32() {
        return o32;
    }

    public void setO32(boolean o32) {
        this.o32 = o32;
    }

    public boolean isO33() {
        return o33;
    }

    public void setO33(boolean o33) {
        this.o33 = o33;
    }

    public boolean isO34() {
        return o34;
    }

    public void setO34(boolean o34) {
        this.o34 = o34;
    }

    public boolean isO35() {
        return o35;
    }

    public void setO35(boolean o35) {
        this.o35 = o35;
    }

    public boolean isO36() {
        return o36;
    }

    public void setO36(boolean o36) {
        this.o36 = o36;
    }

    public boolean isO37() {
        return o37;
    }

    public void setO37(boolean o37) {
        this.o37 = o37;
    }

    public boolean isO38() {
        return o38;
    }

    public void setO38(boolean o38) {
        this.o38 = o38;
    }

    public boolean isO39() {
        return o39;
    }

    public void setO39(boolean o39) {
        this.o39 = o39;
    }

    public boolean isO40() {
        return o40;
    }

    public void setO40(boolean o40) {
        this.o40 = o40;
    }

    public boolean isO41() {
        return o41;
    }

    public void setO41(boolean o41) {
        this.o41 = o41;
    }

    public boolean isO42() {
        return o42;
    }

    public void setO42(boolean o42) {
        this.o42 = o42;
    }

    public boolean isO43() {
        return o43;
    }

    public void setO43(boolean o43) {
        this.o43 = o43;
    }

    public boolean isO44() {
        return o44;
    }

    public void setO44(boolean o44) {
        this.o44 = o44;
    }

    public boolean isO45() {
        return o45;
    }

    public void setO45(boolean o45) {
        this.o45 = o45;
    }

    public boolean isO46() {
        return o46;
    }

    public void setO46(boolean o46) {
        this.o46 = o46;
    }

    public boolean isO47() {
        return o47;
    }

    public void setO47(boolean o47) {
        this.o47 = o47;
    }

    public boolean isO48() {
        return o48;
    }

    public void setO48(boolean o48) {
        this.o48 = o48;
    }

    public boolean isO49() {
        return o49;
    }

    public void setO49(boolean o49) {
        this.o49 = o49;
    }

    public boolean isO50() {
        return o50;
    }

    public void setO50(boolean o50) {
        this.o50 = o50;
    }

    public boolean isO51() {
        return o51;
    }

    public void setO51(boolean o51) {
        this.o51 = o51;
    }

    public boolean isO52() {
        return o52;
    }

    public void setO52(boolean o52) {
        this.o52 = o52;
    }

    public boolean isO53() {
        return o53;
    }

    public void setO53(boolean o53) {
        this.o53 = o53;
    }

    public boolean isO54() {
        return o54;
    }

    public void setO54(boolean o54) {
        this.o54 = o54;
    }

    public boolean isO55() {
        return o55;
    }

    public void setO55(boolean o55) {
        this.o55 = o55;
    }

    public boolean isO56() {
        return o56;
    }

    public void setO56(boolean o56) {
        this.o56 = o56;
    }

    public boolean isO57() {
        return o57;
    }

    public void setO57(boolean o57) {
        this.o57 = o57;
    }

    public boolean isO58() {
        return o58;
    }

    public void setO58(boolean o58) {
        this.o58 = o58;
    }

    public boolean isO59() {
        return o59;
    }

    public void setO59(boolean o59) {
        this.o59 = o59;
    }

    public boolean isO60() {
        return o60;
    }

    public void setO60(boolean o60) {
        this.o60 = o60;
    }

    public boolean isO61() {
        return o61;
    }

    public void setO61(boolean o61) {
        this.o61 = o61;
    }

    public boolean isO62() {
        return o62;
    }

    public void setO62(boolean o62) {
        this.o62 = o62;
    }

    public boolean isO63() {
        return o63;
    }

    public void setO63(boolean o63) {
        this.o63 = o63;
    }

    public boolean isO64() {
        return o64;
    }

    public void setO64(boolean o64) {
        this.o64 = o64;
    }

    public boolean isO65() {
        return o65;
    }

    public void setO65(boolean o65) {
        this.o65 = o65;
    }

    public boolean isO66() {
        return o66;
    }

    public void setO66(boolean o66) {
        this.o66 = o66;
    }

    public boolean isO67() {
        return o67;
    }

    public void setO67(boolean o67) {
        this.o67 = o67;
    }

    public boolean isO68() {
        return o68;
    }

    public void setO68(boolean o68) {
        this.o68 = o68;
    }

    public boolean isO69() {
        return o69;
    }

    public void setO69(boolean o69) {
        this.o69 = o69;
    }

    public boolean isO70() {
        return o70;
    }

    public void setO70(boolean o70) {
        this.o70 = o70;
    }

    public boolean isO71() {
        return o71;
    }

    public void setO71(boolean o71) {
        this.o71 = o71;
    }

    public boolean isO72() {
        return o72;
    }

    public void setO72(boolean o72) {
        this.o72 = o72;
    }

    public boolean isO73() {
        return o73;
    }

    public void setO73(boolean o73) {
        this.o73 = o73;
    }

    public boolean isO74() {
        return o74;
    }

    public void setO74(boolean o74) {
        this.o74 = o74;
    }

    public boolean isO75() {
        return o75;
    }

    public void setO75(boolean o75) {
        this.o75 = o75;
    }

    public boolean isO76() {
        return o76;
    }

    public void setO76(boolean o76) {
        this.o76 = o76;
    }

    public boolean isO77() {
        return o77;
    }

    public void setO77(boolean o77) {
        this.o77 = o77;
    }

    public boolean isO78() {
        return o78;
    }

    public void setO78(boolean o78) {
        this.o78 = o78;
    }

    public boolean isO79() {
        return o79;
    }

    public void setO79(boolean o79) {
        this.o79 = o79;
    }

    public boolean isO80() {
        return o80;
    }

    public void setO80(boolean o80) {
        this.o80 = o80;
    }

    public boolean isO81() {
        return o81;
    }

    public void setO81(boolean o81) {
        this.o81 = o81;
    }

    public boolean isO82() {
        return o82;
    }

    public void setO82(boolean o82) {
        this.o82 = o82;
    }

    public boolean isO83() {
        return o83;
    }

    public void setO83(boolean o83) {
        this.o83 = o83;
    }

    public boolean isO84() {
        return o84;
    }

    public void setO84(boolean o84) {
        this.o84 = o84;
    }

    public boolean isO85() {
        return o85;
    }

    public void setO85(boolean o85) {
        this.o85 = o85;
    }

    public boolean isO86() {
        return o86;
    }

    public void setO86(boolean o86) {
        this.o86 = o86;
    }

    public boolean isO87() {
        return o87;
    }

    public void setO87(boolean o87) {
        this.o87 = o87;
    }

    public boolean isO88() {
        return o88;
    }

    public void setO88(boolean o88) {
        this.o88 = o88;
    }

    public boolean isO89() {
        return o89;
    }

    public void setO89(boolean o89) {
        this.o89 = o89;
    }

    public boolean isO90() {
        return o90;
    }

    public void setO90(boolean o90) {
        this.o90 = o90;
    }

    public boolean isO91() {
        return o91;
    }

    public void setO91(boolean o91) {
        this.o91 = o91;
    }

    public boolean isO92() {
        return o92;
    }

    public void setO92(boolean o92) {
        this.o92 = o92;
    }

    public boolean isO93() {
        return o93;
    }

    public void setO93(boolean o93) {
        this.o93 = o93;
    }

    public boolean isO94() {
        return o94;
    }

    public void setO94(boolean o94) {
        this.o94 = o94;
    }

    public boolean isO95() {
        return o95;
    }

    public void setO95(boolean o95) {
        this.o95 = o95;
    }

    public boolean isO96() {
        return o96;
    }

    public void setO96(boolean o96) {
        this.o96 = o96;
    }

    public boolean isO97() {
        return o97;
    }

    public void setO97(boolean o97) {
        this.o97 = o97;
    }

    public boolean isO98() {
        return o98;
    }

    public void setO98(boolean o98) {
        this.o98 = o98;
    }

    public boolean isO99() {
        return o99;
    }

    public void setO99(boolean o99) {
        this.o99 = o99;
    }

    public boolean isO100() {
        return o100;
    }

    public void setO100(boolean o100) {
        this.o100 = o100;
    }

    public boolean isO13() {
        return o13;
    }

    public void setO13(boolean o13) {
        this.o13 = o13;
    }

    public boolean isO14() {
        return o14;
    }

    public void setO14(boolean o14) {
        this.o14 = o14;
    }

    public boolean isO16() {
        return o16;
    }

    public void setO16(boolean o16) {
        this.o16 = o16;
    }

    public boolean isO17() {
        return o17;
    }

    public void setO17(boolean o17) {
        this.o17 = o17;
    }

    public boolean isO18() {
        return o18;
    }

    public void setO18(boolean o18) {
        this.o18 = o18;
    }

    public boolean isO1() {
        return o1;
    }

    public void setO1(boolean o1) {
        this.o1 = o1;
    }

    public boolean isO2() {
        return o2;
    }

    public void setO2(boolean o2) {
        this.o2 = o2;
    }

    public boolean isO3() {
        return o3;
    }

    public void setO3(boolean o3) {
        this.o3 = o3;
    }

    public boolean isO4() {
        return o4;
    }

    public void setO4(boolean o4) {
        this.o4 = o4;
    }

    public boolean isO5() {
        return o5;
    }

    public void setO5(boolean o5) {
        this.o5 = o5;
    }

    public boolean isO6() {
        return o6;
    }

    public void setO6(boolean o6) {
        this.o6 = o6;
    }

    public boolean isO7() {
        return o7;
    }

    public void setO7(boolean o7) {
        this.o7 = o7;
    }

    public boolean isO8() {
        return o8;
    }

    public void setO8(boolean o8) {
        this.o8 = o8;
    }

    public boolean isO9() {
        return o9;
    }

    public void setO9(boolean o9) {
        this.o9 = o9;
    }

    public boolean isO10() {
        return o10;
    }

    public void setO10(boolean o10) {
        this.o10 = o10;
    }

    public boolean isO11() {
        return o11;
    }

    public void setO11(boolean o11) {
        this.o11 = o11;
    }

    public boolean isO12() {
        return o12;
    }

    public void setO12(boolean o12) {
        this.o12 = o12;
    }

    public boolean isO15() {
        return o15;
    }

    public void setO15(boolean o15) {
        this.o15 = o15;
    }

    private int f1;
    private int f2;
    private int f3;
    private int f4;
    private int f5;
    private int f6;
    private int f7;
    private int f8;
    private int f9;
    private int f10;
    private int f11;
    private int f12;
    private int f13;
    private int f14;
    private int f15;
    private int f16;
    private int f17;
    private int f18;
    private int f19;
    private int f20;
    private int f21;
    private int f22;
    private int f23;
    private int f24;
    private int f25;
    private int f26;
    private int f27;
    private int f28;
    private int f29;
    private int f30;
    private int f31;
    private int f32;
    private int f33;
    private int f34;
    private int f35;
    private int f36;
    private int f37;
    private int f38;
    private int f39;
    private int f40;
    private int f41;
    private int f42;
    private int f43;
    private int f44;
    private int f45;
    private int f46;
    private int f47;
    private int f48;
    private int f49;
    private int f50;
    private int f51;
    private int f52;
    private int f53;
    private int f54;
    private int f55;
    private int f56;
    private int f57;
    private int f58;
    private int f59;
    private int f60;
    private int f61;
    private int f62;
    private int f63;
    private int f64;
    private int f65;
    private int f66;
    private int f67;
    private int f68;
    private int f69;
    private int f70;
    private int f71;
    private int f72;
    private int f73;
    private int f74;
    private int f75;
    private int f76;
    private int f77;
    private int f78;
    private int f79;
    private int f80;
    private int f81;
    private int f82;
    private int f83;
    private int f84;
    private int f85;
    private int f86;
    private int f87;
    private int f88;
    private int f89;
    private int f90;
    private int f91;
    private int f92;
    private int f93;
    private int f94;
    private int f95;
    private int f96;
    private int f97;
    private int f98;
    private int f99;

    public int[] getWin() {
        return win;
    }

    public void setWin(int[] win) {
        this.win = win;
    }

    int[] win = new int[100];

    public int getF1() {
        return f1;
    }

    public void setF1(int f1) {
        this.f1 = f1;
    }

    public int getF2() {
        return f2;
    }

    public void setF2(int f2) {
        this.f2 = f2;
    }

    public int getF3() {
        return f3;
    }

    public void setF3(int f3) {
        this.f3 = f3;
    }

    public int getF4() {
        return f4;
    }

    public void setF4(int f4) {
        this.f4 = f4;
    }

    public int getF5() {
        return f5;
    }

    public void setF5(int f5) {
        this.f5 = f5;
    }

    public int getF6() {
        return f6;
    }

    public void setF6(int f6) {
        this.f6 = f6;
    }

    public int getF7() {
        return f7;
    }

    public void setF7(int f7) {
        this.f7 = f7;
    }

    public int getF8() {
        return f8;
    }

    public void setF8(int f8) {
        this.f8 = f8;
    }

    public int getF9() {
        return f9;
    }

    public void setF9(int f9) {
        this.f9 = f9;
    }

    public int getF10() {
        return f10;
    }

    public void setF10(int f10) {
        this.f10 = f10;
    }

    public int getF11() {
        return f11;
    }

    public void setF11(int f11) {
        this.f11 = f11;
    }

    public int getF12() {
        return f12;
    }

    public void setF12(int f12) {
        this.f12 = f12;
    }

    public int getF13() {
        return f13;
    }

    public void setF13(int f13) {
        this.f13 = f13;
    }

    public int getF14() {
        return f14;
    }

    public void setF14(int f14) {
        this.f14 = f14;
    }

    public int getF15() {
        return f15;
    }

    public void setF15(int f15) {
        this.f15 = f15;
    }

    public int getF16() {
        return f16;
    }

    public void setF16(int f16) {
        this.f16 = f16;
    }

    public int getF17() {
        return f17;
    }

    public void setF17(int f17) {
        this.f17 = f17;
    }

    public int getF18() {
        return f18;
    }

    public void setF18(int f18) {
        this.f18 = f18;
    }

    public int getF19() {
        return f19;
    }

    public void setF19(int f19) {
        this.f19 = f19;
    }

    public int getF20() {
        return f20;
    }

    public void setF20(int f20) {
        this.f20 = f20;
    }

    public int getF21() {
        return f21;
    }

    public void setF21(int f21) {
        this.f21 = f21;
    }

    public int getF22() {
        return f22;
    }

    public void setF22(int f22) {
        this.f22 = f22;
    }

    public int getF23() {
        return f23;
    }

    public void setF23(int f23) {
        this.f23 = f23;
    }

    public int getF24() {
        return f24;
    }

    public void setF24(int f24) {
        this.f24 = f24;
    }

    public int getF25() {
        return f25;
    }

    public void setF25(int f25) {
        this.f25 = f25;
    }

    public int getF26() {
        return f26;
    }

    public void setF26(int f26) {
        this.f26 = f26;
    }

    public int getF27() {
        return f27;
    }

    public void setF27(int f27) {
        this.f27 = f27;
    }

    public int getF28() {
        return f28;
    }

    public void setF28(int f28) {
        this.f28 = f28;
    }

    public int getF29() {
        return f29;
    }

    public void setF29(int f29) {
        this.f29 = f29;
    }

    public int getF30() {
        return f30;
    }

    public void setF30(int f30) {
        this.f30 = f30;
    }

    public int getF31() {
        return f31;
    }

    public void setF31(int f31) {
        this.f31 = f31;
    }

    public int getF32() {
        return f32;
    }

    public void setF32(int f32) {
        this.f32 = f32;
    }

    public int getF33() {
        return f33;
    }

    public void setF33(int f33) {
        this.f33 = f33;
    }

    public int getF34() {
        return f34;
    }

    public void setF34(int f34) {
        this.f34 = f34;
    }

    public int getF35() {
        return f35;
    }

    public void setF35(int f35) {
        this.f35 = f35;
    }

    public int getF36() {
        return f36;
    }

    public void setF36(int f36) {
        this.f36 = f36;
    }

    public int getF37() {
        return f37;
    }

    public void setF37(int f37) {
        this.f37 = f37;
    }

    public int getF38() {
        return f38;
    }

    public void setF38(int f38) {
        this.f38 = f38;
    }

    public int getF39() {
        return f39;
    }

    public void setF39(int f39) {
        this.f39 = f39;
    }

    public int getF40() {
        return f40;
    }

    public void setF40(int f40) {
        this.f40 = f40;
    }

    public int getF41() {
        return f41;
    }

    public void setF41(int f41) {
        this.f41 = f41;
    }

    public int getF42() {
        return f42;
    }

    public void setF42(int f42) {
        this.f42 = f42;
    }

    public int getF43() {
        return f43;
    }

    public void setF43(int f43) {
        this.f43 = f43;
    }

    public int getF44() {
        return f44;
    }

    public void setF44(int f44) {
        this.f44 = f44;
    }

    public int getF45() {
        return f45;
    }

    public void setF45(int f45) {
        this.f45 = f45;
    }

    public int getF46() {
        return f46;
    }

    public void setF46(int f46) {
        this.f46 = f46;
    }

    public int getF47() {
        return f47;
    }

    public void setF47(int f47) {
        this.f47 = f47;
    }

    public int getF48() {
        return f48;
    }

    public void setF48(int f48) {
        this.f48 = f48;
    }

    public int getF49() {
        return f49;
    }

    public void setF49(int f49) {
        this.f49 = f49;
    }

    public int getF50() {
        return f50;
    }

    public void setF50(int f50) {
        this.f50 = f50;
    }

    public int getF51() {
        return f51;
    }

    public void setF51(int f51) {
        this.f51 = f51;
    }

    public int getF52() {
        return f52;
    }

    public void setF52(int f52) {
        this.f52 = f52;
    }

    public int getF53() {
        return f53;
    }

    public void setF53(int f53) {
        this.f53 = f53;
    }

    public int getF54() {
        return f54;
    }

    public void setF54(int f54) {
        this.f54 = f54;
    }

    public int getF55() {
        return f55;
    }

    public void setF55(int f55) {
        this.f55 = f55;
    }

    public int getF56() {
        return f56;
    }

    public void setF56(int f56) {
        this.f56 = f56;
    }

    public int getF57() {
        return f57;
    }

    public void setF57(int f57) {
        this.f57 = f57;
    }

    public int getF58() {
        return f58;
    }

    public void setF58(int f58) {
        this.f58 = f58;
    }

    public int getF59() {
        return f59;
    }

    public void setF59(int f59) {
        this.f59 = f59;
    }

    public int getF60() {
        return f60;
    }

    public void setF60(int f60) {
        this.f60 = f60;
    }

    public int getF61() {
        return f61;
    }

    public void setF61(int f61) {
        this.f61 = f61;
    }

    public int getF62() {
        return f62;
    }

    public void setF62(int f62) {
        this.f62 = f62;
    }

    public int getF63() {
        return f63;
    }

    public void setF63(int f63) {
        this.f63 = f63;
    }

    public int getF64() {
        return f64;
    }

    public void setF64(int f64) {
        this.f64 = f64;
    }

    public int getF65() {
        return f65;
    }

    public void setF65(int f65) {
        this.f65 = f65;
    }

    public int getF66() {
        return f66;
    }

    public void setF66(int f66) {
        this.f66 = f66;
    }

    public int getF67() {
        return f67;
    }

    public void setF67(int f67) {
        this.f67 = f67;
    }

    public int getF68() {
        return f68;
    }

    public void setF68(int f68) {
        this.f68 = f68;
    }

    public int getF69() {
        return f69;
    }

    public void setF69(int f69) {
        this.f69 = f69;
    }

    public int getF70() {
        return f70;
    }

    public void setF70(int f70) {
        this.f70 = f70;
    }

    public int getF71() {
        return f71;
    }

    public void setF71(int f71) {
        this.f71 = f71;
    }

    public int getF72() {
        return f72;
    }

    public void setF72(int f72) {
        this.f72 = f72;
    }

    public int getF73() {
        return f73;
    }

    public void setF73(int f73) {
        this.f73 = f73;
    }

    public int getF74() {
        return f74;
    }

    public void setF74(int f74) {
        this.f74 = f74;
    }

    public int getF75() {
        return f75;
    }

    public void setF75(int f75) {
        this.f75 = f75;
    }

    public int getF76() {
        return f76;
    }

    public void setF76(int f76) {
        this.f76 = f76;
    }

    public int getF77() {
        return f77;
    }

    public void setF77(int f77) {
        this.f77 = f77;
    }

    public int getF78() {
        return f78;
    }

    public void setF78(int f78) {
        this.f78 = f78;
    }

    public int getF79() {
        return f79;
    }

    public void setF79(int f79) {
        this.f79 = f79;
    }

    public int getF80() {
        return f80;
    }

    public void setF80(int f80) {
        this.f80 = f80;
    }

    public int getF81() {
        return f81;
    }

    public void setF81(int f81) {
        this.f81 = f81;
    }

    public int getF82() {
        return f82;
    }

    public void setF82(int f82) {
        this.f82 = f82;
    }

    public int getF83() {
        return f83;
    }

    public void setF83(int f83) {
        this.f83 = f83;
    }

    public int getF84() {
        return f84;
    }

    public void setF84(int f84) {
        this.f84 = f84;
    }

    public int getF85() {
        return f85;
    }

    public void setF85(int f85) {
        this.f85 = f85;
    }

    public int getF86() {
        return f86;
    }

    public void setF86(int f86) {
        this.f86 = f86;
    }

    public int getF87() {
        return f87;
    }

    public void setF87(int f87) {
        this.f87 = f87;
    }

    public int getF88() {
        return f88;
    }

    public void setF88(int f88) {
        this.f88 = f88;
    }

    public int getF89() {
        return f89;
    }

    public void setF89(int f89) {
        this.f89 = f89;
    }

    public int getF90() {
        return f90;
    }

    public void setF90(int f90) {
        this.f90 = f90;
    }

    public int getF91() {
        return f91;
    }

    public void setF91(int f91) {
        this.f91 = f91;
    }

    public int getF92() {
        return f92;
    }

    public void setF92(int f92) {
        this.f92 = f92;
    }

    public int getF93() {
        return f93;
    }

    public void setF93(int f93) {
        this.f93 = f93;
    }

    public int getF94() {
        return f94;
    }

    public void setF94(int f94) {
        this.f94 = f94;
    }

    public int getF95() {
        return f95;
    }

    public void setF95(int f95) {
        this.f95 = f95;
    }

    public int getF96() {
        return f96;
    }

    public void setF96(int f96) {
        this.f96 = f96;
    }

    public int getF97() {
        return f97;
    }

    public void setF97(int f97) {
        this.f97 = f97;
    }

    public int getF98() {
        return f98;
    }

    public void setF98(int f98) {
        this.f98 = f98;
    }

    public int getF99() {
        return f99;
    }

    public void setF99(int f99) {
        this.f99 = f99;
    }

    public int getF100() {
        return f100;
    }

    public void setF100(int f100) {
        this.f100 = f100;
    }

    private int f100;







    public CaseInfo(){

    }

    public int getCase() {
        return Case;
    }

    public void setCase(int aCase) {
        Case = aCase;
    }

    public int getCaseMoney() {
        return CaseMoney;
    }

    public void setCaseMoney(int caseMoney) {
        CaseMoney = caseMoney;
    }

    public CaseInfo(final int Case, final int caseMoney){
        this.Case = Case;
        this.CaseMoney = caseMoney;
    }
}
