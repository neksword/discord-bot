package UserRankSystem.Economy.Case;

import Audit.LogCommand;
import BotConfig.BotConfig;
import UserRankSystem.Economy.DBNewUser;
import UserRankSystem.MessageRankInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import commands.NoModeration.CheckingChannels.UsedChannel;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class AddCase extends Command {
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    private final MongoCollection<Document> collection;
    public AddCase(MongoCollection<Document> mongoCollection) {
        this.guildOnly = true;
        this.name = "addcase";
        this.ownerCommand = true;
        this.collection = mongoCollection;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (event.isOwner()) {
            final UsedChannel usedChannel = new UsedChannel();
            if (usedChannel.Return(event.getMember(), event.getTextChannel(), event.getMessage())) {
                String[] command = event.getMessage().getContentRaw().split("\\s");
                List<Member> members = event.getMessage().getMentionedMembers();
                try {
                    if (command.length >= 4) {
                        int CaseNumber = Integer.parseInt(command[3]);
                        int Case = Integer.parseInt(command[2]);
                        long id1 = Long.parseLong(event.getMember().getId());
                        Document document1 = collection.find(new Document("id",id1)).first();
                        if (document1 == null){
                            DBNewUser dbNewUser = new DBNewUser();
                            dbNewUser.NewUser(collection,event.getMember());
                        }
                        long id = Long.parseLong(members.get(0).getId());
                        Document document = collection.find(new Document("id",id)).first();
                        if (document == null){
                            DBNewUser dbNewUser = new DBNewUser();
                            dbNewUser.NewMentionUser(collection,members);
                        }
                            if (Case == 1) {
                                collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case1", Integer.parseInt(String.valueOf(document.get("case1"))) + CaseNumber)));
                                event.getMessage().reply("**Кейс выдан участнику -** " + members.get(0).getAsMention()).queue();
                            } else if (Case == 4) {
                                collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case4", Integer.parseInt(String.valueOf(document.get("case4"))) + CaseNumber)));
                                event.getMessage().reply("**Кейс выдан участнику -** " + members.get(0).getAsMention()).queue();
                            } else if (Case == 2) {
                                collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case2", Integer.parseInt(String.valueOf(document.get("case2"))) + CaseNumber)));
                                event.getMessage().reply("**Кейс выдан участнику -** " + members.get(0).getAsMention()).queue();
                            } else if (Case == 3) {
                                collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("case3", Integer.parseInt(String.valueOf(document.get("case3"))) + CaseNumber)));
                                event.getMessage().reply("**Кейс выдан участнику -** " + members.get(0).getAsMention()).queue();
                            } else {
                                event.replyError("**Нет такого кейса!**");
                            }
                    } else {
                        event.getMessage().reply("Использование команды: " + cfg.prefix() + "addcase @пользователь (кейс) (количество кейсов)").queue();
                    }
                } catch (NumberFormatException nfe) {
                    event.getMessage().reply("Использование команды: " + cfg.prefix() + "addcase @пользователь (кейс) (количество кейсов)").queue();
                }

            } else {
                event.getMessage().reply("**Вы не можете использовать эту команду!**").queue();
            }
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "AddCase";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getTextChannel()).build()).queue();
        }
    }
}
