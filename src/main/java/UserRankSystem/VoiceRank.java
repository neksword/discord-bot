package UserRankSystem;

import BotConfig.BotConfig;
import UserRankSystem.Economy.DBNewUser;
import UserRankSystem.Economy.LevelRole;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;
import org.bson.Document;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class VoiceRank extends ListenerAdapter {
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    private final Map<String, VoiceRankInfo> voiceCache = new ConcurrentHashMap<>();
    private final MongoCollection<Document> collection;
    public VoiceRank(MongoCollection<Document> mongoCollection){
        this.collection = mongoCollection;
    }

    public void onGuildVoiceJoin(GuildVoiceJoinEvent event) {
        if (event.getMember().getUser().isBot()){
            return;
        }
        Date date = new Date();
        VoiceRankInfo voiceRankInfo = new VoiceRankInfo(date);
        voiceCache.put(event.getMember().getId(), voiceRankInfo);
    }

    public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {
        if (event.getMember().getUser().isBot()){
            return;
        }
        VoiceRankInfo voiceRankInfo = voiceCache.get(event.getMember().getId());
        long time1 = voiceRankInfo.getTime();
        long time2 = new Date().getTime();
        int minutes = (int) ((time2 - time1) / 60000);
        voiceRankInfo.setTime(minutes);
        long id = Long.parseLong(event.getMember().getId());
        Document document = collection.find(new Document("id", id)).first();
        if (document == null) {
            DBNewUser dbNewUser = new DBNewUser();
            dbNewUser.NewUser(collection, event.getMember());
        }
        collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("newRank", Integer.parseInt(String.valueOf(document.get("newRank"))) - minutes)));
        if ((Integer.parseInt(String.valueOf(document.get("message"))) + Integer.parseInt(String.valueOf(document.get("minvoice")))) >= Integer.parseInt(String.valueOf(document.get("newRank")))) {
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("lvl", Integer.parseInt(String.valueOf(document.get("lvl"))) + 1)));
            collection.updateOne(Filters.eq("id", id), new Document("$set", new Document("newRank",(int) (Integer.parseInt(String.valueOf(document.get("message"))) + Integer.parseInt(String.valueOf(document.get("minvoice"))) + ((Integer.parseInt(String.valueOf(document.get("message"))) + Integer.parseInt(String.valueOf(document.get("minvoice")))) * 2)))));
            final LevelRole levelRole = new LevelRole();
            levelRole.LevelRole(Integer.parseInt(String.valueOf(document.get("lvl"))), event.getMember(), event.getGuild());
        }
    }
}
