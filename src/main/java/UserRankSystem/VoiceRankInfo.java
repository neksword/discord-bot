package UserRankSystem;

import java.util.Date;

public class VoiceRankInfo {
    private long time;

    public VoiceRankInfo(){

    }

    public void setTime(long time) {
        this.time = time;
    }

    public VoiceRankInfo(final Date date){
        this.time = date.getTime();
    }
    public long getTime(){return time;}
}
