package VkConnect;

import BotConfig.BotConfig;
import api.longpoll.bots.LongPollBot;
import api.longpoll.bots.model.events.boards.BoardPostEvent;
import api.longpoll.bots.model.events.messages.MessageNewEvent;
import api.longpoll.bots.model.objects.basic.WallPost;
import net.dv8tion.jda.api.JDA;
import org.aeonbits.owner.ConfigFactory;

public class VkBoard extends LongPollBot {
    BotConfig cfg = ConfigFactory.create(BotConfig.class);
    private JDA jda;
    public VkBoard(JDA jda){
        this.jda = jda;
    }
    @Override
    public void onMessageNew(MessageNewEvent messageNewEvent) {
        System.out.println(messageNewEvent.getMessage().getText());
        jda.getGuildById("815912457058517003").getTextChannelById("815912457561571370").sendMessage(messageNewEvent.getMessage().getText()).queue();
    }

    @Override
    public void onWallPostNew(WallPost wallPost) {
        jda.getGuildById("815912457058517003").getTextChannelById("815912457561571370").sendMessage(wallPost.getText()).queue();
    }

    @Override
    public String getAccessToken() {
        return cfg.vktoken();
    }

    @Override
    public int getGroupId() {
        return 204756636;
    }
}
