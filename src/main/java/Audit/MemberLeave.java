package Audit;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.time.format.DateTimeFormatter;
import java.util.Date;

public class MemberLeave extends ListenerAdapter {
    public void onGuildMemberRemove(GuildMemberRemoveEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        Member member = event.getMember();
        User user = event.getUser();
        TextChannel textChannel = event.getGuild().getTextChannelById(cfg.botaudit());
        textChannel.sendMessage( "<@" + user.getId() + ">"  + " вышел!").queue();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setAuthor(user.getName(),user.getAvatarUrl(),user.getEffectiveAvatarUrl());
        embedBuilder.setThumbnail(user.getAvatarUrl());
        embedBuilder.addField("Информация:","Вышел: " + "<@" + user.getId() + ">" + "\n" + "Дата создания аккаунта: " + user.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "\n" + "ID: " + user.getId(),false);
        Date date = new Date();
        embedBuilder.setFooter(date.toString() + " | " + event.getGuild().getName());
        textChannel.sendTyping().queue();
        textChannel.sendMessage(embedBuilder.build()).queue();
        embedBuilder.clear();

    }
}
