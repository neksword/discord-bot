package Audit;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.util.Date;
import java.util.List;

public class ChangeNickname extends ListenerAdapter {

    public void onGuildMemberUpdateNickname(GuildMemberUpdateNicknameEvent event) {
    User user = event.getUser();
        //System.out.println(event.getMember().getEffectiveName());
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        Member member = event.getMember();
        TextChannel textChannel = event.getGuild().getTextChannelById(cfg.botaudit());
        System.out.println(event.getOldNickname());
        EmbedBuilder info = new EmbedBuilder();
        info.setAuthor(member.getUser().getName(),member.getUser().getAvatarUrl(),member.getUser().getEffectiveAvatarUrl());
        info.setThumbnail(member.getUser().getAvatarUrl());
        info.addField("Никнейм изменён! ","<@" + member.getUser().getId() + ">" + " сменил ник!",false);
        info.addField("Старый ник: ",event.getOldNickname(),true);
        if (event.getNewNickname() == null){
            info.addField("Новый ник: ",event.getMember().getUser().getName(),false);
        }
        else {
            info.addField("Новый ник: ", event.getNewNickname(), true);
        }
        Date date = new Date();
        info.setFooter(date.toString() + " | " + event.getGuild().getName());
        info.setColor(Color.getHSBColor(1,12,3));

        textChannel.sendTyping().queue();
        textChannel.sendMessage(info.build()).queue();
        info.clear();

    }
}
