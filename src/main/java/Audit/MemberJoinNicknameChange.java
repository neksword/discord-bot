package Audit;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class MemberJoinNicknameChange extends ListenerAdapter {
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        //BotConfig cfg = ConfigFactory.create(BotConfig.class);
        Member target = event.getMember();
        User user = event.getUser();
        Guild guild = event.getGuild();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        channel.sendMessage("Присоеденился: <@" + target.getId() + ">").queue();
        EmbedBuilder join = new EmbedBuilder();
        join.setColor(Color.cyan);
        join.setAuthor(user.getName(), user.getAvatarUrl(), user.getEffectiveAvatarUrl());
        join.addField("Информация:", "\n" + "Присоеденился: <@" + target.getId() + ">" + "\n" + "Дата создания аккаунта: " + target.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "\n" + "ID: " + user.getId(), false);
        join.setThumbnail(user.getEffectiveAvatarUrl());
        Date date = new Date();
        join.setFooter(date.toString() + " | " + event.getGuild().getName());
        //target.modifyNickname(user.getName() + " | Одиночка").queue();

        channel.sendTyping().queue();
        channel.sendMessage(join.build()).queue();
        join.clear();
    }
}
