package Audit;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Invite;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.invite.GuildInviteCreateEvent;
import net.dv8tion.jda.api.events.guild.invite.GuildInviteDeleteEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.time.format.DateTimeFormatter;

public class InviteLink extends ListenerAdapter {

    public void onGuildInviteCreate(GuildInviteCreateEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        Invite invite = event.getInvite();
        User inviter = event.getInvite().getInviter();
        EmbedBuilder builder = new EmbedBuilder();
        builder.setAuthor(inviter.getAsTag(),inviter.getAvatarUrl(),inviter.getEffectiveAvatarUrl());
        if (invite.getMaxUses() == 0){
            builder.addField("Информация о ссылке: ","Создатель: " + "<@" + inviter.getId() + ">" +  "\n" + "Дата создания: " + invite.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "\n" + "Ссылка: " + invite.getUrl() + "\n" + "Максимум использований: " + "*Не ограничено*" + "\n" + "Канал: " + invite.getChannel().getName(),false);
        }
        else {
            builder.addField("Информация о ссылке: ","Создатель: " + "<@" + inviter.getId() + ">" +  "\n" + "Дата создания: " + invite.getTimeCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "\n" + "Ссылка: " + invite.getUrl() + "\n" + "Максимум использований: " + invite.getMaxUses() + "\n" + "Канал: " + invite.getChannel().getName(),false);
        }
        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }
    public void onGuildInviteDelete(GuildInviteDeleteEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        EmbedBuilder builder = new EmbedBuilder();
        builder.addField("Удалена ссылка: ","Ссылка: " + event.getUrl(),false);

        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }
}
