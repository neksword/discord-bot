package Audit;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleAddEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.util.Date;

public class MemberRoleUpdate extends ListenerAdapter {

    public void onGuildMemberRoleAdd(GuildMemberRoleAddEvent event) {
        Member member = event.getMember();
        Role role = event.getRoles().get(0);
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        EmbedBuilder builder = new EmbedBuilder();
        builder.addField("Добавлены роли: ","Участнику: " + "<@" + member.getId() + ">" + " выдана роль: " + role.getAsMention(),false );
        Date date = new Date();
        builder.setFooter(date.toString() + " | " + event.getGuild().getName());
        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }

    public void onGuildMemberRoleRemove(GuildMemberRoleRemoveEvent event) {
        Member member = event.getMember();
        Role role = event.getRoles().get(0);
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());
        EmbedBuilder builder = new EmbedBuilder();
        builder.addField("Удалены роли: ","У участника: " + "<@" + member.getId() + ">" + " убрана роль: " + role.getAsMention(),false );
        Date date = new Date();
        builder.setFooter(date.toString() + " | " + event.getGuild().getName());

        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }
}
