package Audit;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.audit.ActionType;
import net.dv8tion.jda.api.audit.AuditLogChange;
import net.dv8tion.jda.api.audit.AuditLogEntry;
import net.dv8tion.jda.api.audit.AuditLogOption;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.channel.text.TextChannelCreateEvent;
import net.dv8tion.jda.api.events.channel.text.TextChannelDeleteEvent;
import net.dv8tion.jda.api.events.channel.text.update.TextChannelUpdateNameEvent;
import net.dv8tion.jda.api.events.channel.voice.VoiceChannelCreateEvent;
import net.dv8tion.jda.api.events.channel.voice.VoiceChannelDeleteEvent;
import net.dv8tion.jda.api.events.channel.voice.update.VoiceChannelUpdateNameEvent;
import net.dv8tion.jda.api.events.role.GenericRoleEvent;
import net.dv8tion.jda.api.events.role.update.RoleUpdateNameEvent;
import net.dv8tion.jda.api.events.role.update.RoleUpdatePermissionsEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import javax.xml.crypto.Data;
import java.util.Date;

public class ChannelRoleUpdate extends ListenerAdapter{

    public void onRoleUpdateName(RoleUpdateNameEvent event) {
        Date date = new Date();
        Role role = event.getRole();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder builder = new EmbedBuilder();
        builder.setFooter(date.toString() + " | " + event.getGuild().getName());
        builder.addField("Изменение роли: ","Роль: " + role.getAsMention() + "\n" + "Новое имя: " + "`" + event.getNewName() + "`" + "\n" + "Старое имя: " + "`" + event.getOldName() + "`",false);

        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }

    public void onTextChannelUpdateName(TextChannelUpdateNameEvent event) {
        Date date = new Date();
        TextChannel textChannel = event.getChannel();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder builder = new EmbedBuilder();
        builder.setFooter(date.toString() + " | " + event.getGuild().getName());
        builder.addField("Изменение канала: ","Канал: " + textChannel.getAsMention() + "\n" + "Новое имя: " + "`" + event.getNewName() + "`" + "\n" + "Старое имя: " + "`" + event.getOldName() + "`",false);

        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }

    public void onVoiceChannelUpdateName(VoiceChannelUpdateNameEvent event) {
        Date date = new Date();
        VoiceChannel voiceChannel = event.getChannel();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder builder = new EmbedBuilder();
        builder.setFooter(date.toString() + " | " + event.getGuild().getName());
        builder.addField("Изменение голосового канала: ","Канал: " + voiceChannel.getName() + "\n" + "Новое имя: " + "`" + event.getNewName() + "`" + "\n" + "Старое имя: " + "`" + event.getOldName() + "`",false);

        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }

    public void onTextChannelCreate(TextChannelCreateEvent event) {
        Date date = new Date();
        TextChannel textChannel = event.getChannel();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder builder = new EmbedBuilder();
        builder.setFooter(date.toString() + " | " + event.getGuild().getName());
        builder.addField("Создан канал: ","Имя: " + "`" + textChannel.getName() + "`" + "\n" + "Тег: " + textChannel.getAsMention() + "\n" + "ID: " + "`" + textChannel.getId() + "`",false);

        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }

    public void onTextChannelDelete(TextChannelDeleteEvent event) {
        Date date = new Date();
        TextChannel textChannel = event.getChannel();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder builder = new EmbedBuilder();
        builder.setFooter(date.toString() + " | " + event.getGuild().getName());
        builder.addField("Удалён канал: ","Имя: " + "`" + textChannel.getName() + "`" + "\n" + "ID: " + "`" + textChannel.getId() + "`" ,false);

        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }

    public void onVoiceChannelCreate(VoiceChannelCreateEvent event) {
        Date date = new Date();
        VoiceChannel voiceChannel = event.getChannel();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder builder = new EmbedBuilder();
        builder.addField("Создан голосовой канал: ","Имя: " + "`" + voiceChannel.getName() + "`" + "\n" + "ID: " + "`" + voiceChannel.getId() + "`",false);
        builder.setFooter(date.toString() + " | " + event.getGuild().getName());
        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();

    }

    public void onVoiceChannelDelete(VoiceChannelDeleteEvent event) {
        Date date = new Date();
        VoiceChannel voiceChannel = event.getChannel();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder builder = new EmbedBuilder();
        builder.setFooter(date.toString() + " | " + event.getGuild().getName());
        builder.addField("Удалён голосовой канал: ","Имя: " + "`" + voiceChannel.getName() + "`" + "\n" + "ID: " + "`" + voiceChannel.getId() + "`" ,false);

        channel.sendTyping().queue();
        channel.sendMessage(builder.build()).queue();
        builder.clear();
    }
}
