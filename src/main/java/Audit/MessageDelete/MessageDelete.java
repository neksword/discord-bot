package Audit.MessageDelete;

import Audit.MemberInviteInfo.InviteData;
import Audit.MessageDelete.MessageData;
import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.guild.GuildReadyEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MessageDelete extends ListenerAdapter {
    private final Map<String, MessageData> messageCache = new ConcurrentHashMap<>();


    public void onGuildMessageReceived(final GuildMessageReceivedEvent event) {
        final String message = event.getMessageId();
        final MessageData messageData = new MessageData(event.getMessage());
        messageCache.put(message, messageData);
    }

    public void onGuildMessageDelete(GuildMessageDeleteEvent event) {
        final String message = event.getMessageId();
        System.out.println(messageCache.toString());
        System.out.println(messageCache.containsKey(event.getMessageId()));
        System.out.println(event.getMessageId());
        System.out.println(messageCache.get(event.getMessageId()).getMessageContent());

        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        final String messageCont = event.getMessageId();
        final MessageData messageCashe = messageCache.get(messageCont);
        EmbedBuilder builder = new EmbedBuilder();
        builder.setTitle("Удалено сообщение!");
        builder.setDescription("**Контент: **" + "\n" + messageCashe.getMessageContent());
        if (!(messageCache.get(event.getMessageId()).getImageURL().size() == 0)){
         builder.setImage(messageCache.get(event.getMessageId()).getImageURL().get(0).getUrl());
        }
        builder.addField("Информация: ","**Автор: **" + event.getGuild().getMemberById(messageCashe.getMemberid()).getAsMention() + "\n" + "**Время: **" + new Date().toString() + "\n" + "**Канал: **" + event.getChannel().getAsMention(), false);
        TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
        logchannel.sendMessage(builder.build()).queue();
        builder.clear();
        messageCache.remove(message);
    }
}

