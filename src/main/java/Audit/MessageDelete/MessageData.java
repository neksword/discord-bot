package Audit.MessageDelete;

import net.dv8tion.jda.api.entities.Message;

import java.util.List;

public class MessageData {
    private String channelid;
    private String memberid;
    private String messageContent;
    private String messageID;
    private List<Message.Attachment> ImageURL;
    public MessageData(final Message message) {
        this.channelid = message.getChannel().getId();
        this.memberid = message.getAuthor().getId();
        this.messageContent = message.getContentDisplay();
        this.messageID = message.getId();
        this.ImageURL = message.getAttachments();
    }
    public String getChannelid()
    {
        return channelid;
    }
    public String getMemberid()
    {
        return memberid;
    }
    public  String getMessageContent()
    {
        return messageContent;
    }
    public String getMessageID()
    {
        return messageID;
    }
    public List<Message.Attachment> getImageURL()
    {
        return ImageURL;
    }
}
