package Audit;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import org.aeonbits.owner.ConfigFactory;

import java.util.Date;

public class LogCommand {
    public EmbedBuilder log(final Guild guild, final String commandName, final User user, final TextChannel textChannel){
        Date date = new Date();

        EmbedBuilder builder = new EmbedBuilder();
        builder.setTitle("Была использована команда: ");
        builder.addField("Информация: ", "Название команды: " + "`" + commandName + "`" + "\n" + "Использована в: " + "`" + textChannel.getName() + "`" + "( " + textChannel.getAsMention() + " )" + "\n" + "Время: " + "`" + date.toString() + "`", false);
        builder.setThumbnail(user.getAvatarUrl());
        builder.setColor(56);
        builder.addField("Пользователь: ", "Имя: " + "`" + user.getName() + "`" + "( " + user.getAsMention() + " )" + "\n" + "ID: " + "`" + user.getId() + "`", false);
        builder.setFooter(guild.getName());
        return builder;
    }

}
