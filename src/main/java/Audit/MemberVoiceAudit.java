package Audit;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.audit.AuditLogEntry;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.util.Date;

public class MemberVoiceAudit extends ListenerAdapter {

    public void onGuildVoiceJoin(GuildVoiceJoinEvent event) {
        Date date = new Date();
        Member member = event.getMember();
        VoiceChannel channel = event.getChannelJoined();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel textchannel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setFooter(date.toString() + " | "+ event.getGuild().getName());
        embedBuilder.addField("Зашёл в голосовой канал: ","Участник: " + "<@" + member.getId() + ">" + "\n" + "Канал: " + "`" + channel.getName() + "`",false);
        embedBuilder.setThumbnail(member.getUser().getAvatarUrl());

        textchannel.sendTyping().queue();
        textchannel.sendMessage(embedBuilder.build()).queue();
        embedBuilder.clear();
    }

    public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {
        Date date = new Date();
        Member member = event.getMember();
        VoiceChannel channel = event.getChannelJoined();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel textchannel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setFooter(date.toString() + " | "+ event.getGuild().getName());
        embedBuilder.addField("Вышел из голосового канала: ","Участник: " + "<@" + member.getId() + ">",false);
        embedBuilder.setThumbnail(member.getUser().getAvatarUrl());

        textchannel.sendTyping().queue();
        textchannel.sendMessage(embedBuilder.build()).queue();
        embedBuilder.clear();
    }

    public void onGuildVoiceMove(GuildVoiceMoveEvent event) {
        Date date = new Date();
        Member member = event.getMember();
        VoiceChannel channel = event.getChannelJoined();
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        TextChannel textchannel = event.getGuild().getTextChannelById(cfg.botaudit());

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setFooter(date.toString() + " | "+ event.getGuild().getName());
        embedBuilder.addField("Перешёл в другой голосовой канал : ","Участник: " + "<@" + member.getId() + ">" + "\n" + "Канал: " + "`" + channel.getName() + "`",false);
        embedBuilder.setThumbnail(member.getUser().getAvatarUrl());

        textchannel.sendTyping().queue();
        textchannel.sendMessage(embedBuilder.build()).queue();
        embedBuilder.clear();

    }
}
