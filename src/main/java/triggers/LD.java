package triggers;

import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

import java.util.Date;
import java.util.TimeZone;

public class LD extends ListenerAdapter {
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        BotConfig cfg = ConfigFactory.create(BotConfig.class);
        String msg = event.getMessage().getContentRaw();
        if (msg.endsWith("L&D")) {
            Message message = event.getMessage();
            message.addReaction("\uD83D\uDC4D").queue();
            message.addReaction("\uD83D\uDC4E").queue();

            Date date = new Date();

            TextChannel channel = event.getGuild().getTextChannelById(cfg.botaudit());

            EmbedBuilder builder = new EmbedBuilder();
            builder.setTitle("Была использована команда :");
            builder.addField("Информация: ","Название команды: `L&D` " + "\n" + "Использована в: " + "`" + event.getChannel().getName() + "`" + "( " + event.getChannel().getAsMention() + " )" + "\n" + "Время: " + "`" + date.toString() + "`" ,false);
            builder.setThumbnail(event.getAuthor().getAvatarUrl());
            builder.setColor(56);
            builder.addField("Пользователь: ","Имя: " + "`" + event.getAuthor().getName() + "`" + "( " + event.getMember().getAsMention() + " )" + "\n" + "ID: " + "`" + event.getAuthor().getId() + "`",false);
            builder.setFooter(event.getGuild().getName());

            channel.sendMessage(builder.build()).queue();
            builder.clear();
        }
    }
}

