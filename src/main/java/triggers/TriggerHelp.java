package triggers;

import Audit.LogCommand;
import BotConfig.BotConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.aeonbits.owner.ConfigFactory;

public class TriggerHelp extends ListenerAdapter {
     BotConfig cfg = ConfigFactory.create(BotConfig.class);
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] command = event.getMessage().getContentRaw().split("\\s");
        if (command[0].equalsIgnoreCase("911")){
            Member member = event.getMember();
            TextChannel channel = event.getChannel();
            TextChannel admin = event.getGuild().getTextChannelById("807894051050749982");
            EmbedBuilder builder = new EmbedBuilder();
            builder.addField("Вызвал:",member.getAsMention(),true);
            builder.addField("Канал:",channel.getAsMention(),true);
            StringBuilder stringBuilder = new StringBuilder();
            if (command.length == 1){
                builder.addField("Причина:","Не указана.",true);
            }
            else {
                for (int i = 1; i < command.length; i++) {
                    stringBuilder.append(command[i]).append(" ");
                }
                builder.addField("Причина:", stringBuilder.toString(), true);
            }
            MessageBuilder messageBuilder = new MessageBuilder();
            messageBuilder.setEmbed(builder.build());
            Role role = event.getGuild().getRoleById("807838754000994325");
            messageBuilder.setContent(role.getAsMention());
            admin.sendMessage(messageBuilder.build()).queue();
            TextChannel logchannel = event.getGuild().getTextChannelById(cfg.botaudit());
            LogCommand logCommand = new LogCommand();
            String commandname = "911";
            logchannel.sendMessage(logCommand.log(event.getGuild(), commandname, event.getMember().getUser(), event.getChannel()).build()).queue();
            event.getMessage().reply("Администрация уже спешит к вам на помощь!").queue();
        }
    }
}
